package dev.perryplaysmc.core;


import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

@SuppressWarnings("all")
public class SuperAPI {

    @Getter@Setter
    static Core core;

    public static Config getSettings() {
        return getCore().getSettings();
    }

    public static Config getMessages() {
        return getCore().getMessages();
    }


    public static Set<User> getOnlineUsers() {
        return getCore().getOnlineUsers();
    }

    public static Set<OfflineUser> getOfflineUsers() {
        return getCore().getOfflineUsers();
    }

    public static User getUser(Player player) {
        Optional<User> p = getOnlineUsers().stream().filter(u->
                u.getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString())).findFirst();
        return p==null||!p.isPresent()?null:p.get();
    }

    public static User getUser(UUID player) {
        Optional<User> p = getOnlineUsers().stream().filter(u->u.getUniqueId().toString().equalsIgnoreCase(player.toString())).findFirst();
        return p==null||!p.isPresent()?null:p.get();
    }

    public static User getUser(String player) {
        Optional<User> p = getOnlineUsers().stream().filter(u->u.getName().equalsIgnoreCase(player)).findFirst();
        return p==null||!p.isPresent()?null:p.get();
    }

    public static OfflineUser getOfflineUser(OfflinePlayer player) {
        Optional<OfflineUser> p = getOfflineUsers().stream().filter(u->
                u.getUniqueId().toString().equalsIgnoreCase(player.getUniqueId().toString())).findFirst();
        return p==null||!p.isPresent()?null:p.get();

    }

    public static OfflineUser getOfflineUser(UUID player) {
        Optional<OfflineUser> p = getOfflineUsers().stream().filter(u->
                u.getUniqueId().toString().equalsIgnoreCase(player.toString())).findFirst();
        return p==null||!p.isPresent()?null:p.get();
    }

    public static OfflineUser getOfflineUser(String player) {
        Optional<OfflineUser> p = getOfflineUsers().stream().filter(u->u.getName().equalsIgnoreCase(player)).findFirst();
        return p==null||!p.isPresent()?null:p.get();
    }

    public static CommandSource getSource(String name) {
        if(getUser(name)!=null)return getUser(name);
        if(name.equalsIgnoreCase("server") || name.equalsIgnoreCase("console"))
            return getCore().getConsole();
        return null;
    }

    public static CommandSource getSource(CommandSender sender) {
        String name = sender.getName();
        if(getUser(name)!=null)return getUser(name);
        if(name.equalsIgnoreCase("server") || name.equalsIgnoreCase("console"))
            return getCore().getConsole();
        return null;
    }

    public static CommandSource getConsole() {
        return getCore().getConsole();
    }

    public static <T> T[] addToArray(T[] array, T toAdd) {
        List<T> list = Arrays.asList(array);
        list.add(toAdd);
        return convertToArray(list);
    }

    public static <T> T[] convertToArray(List<T> list) {
        return (T[]) list.toArray();
    }

}
