package dev.perryplaysmc.core;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.commands.CommandExecutor;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.sources.inheritors.BaseCommandSource;
import dev.perryplaysmc.sources.inheritors.BaseOfflineUser;
import dev.perryplaysmc.sources.inheritors.BaseUser;
import dev.perryplaysmc.utils.ClassUtils;
import dev.perryplaysmc.utils.EconomyImpl;
import dev.perryplaysmc.utils.TimerUtility;
import dev.perryplaysmc.utils.nmsUtils.NMSMain;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.Getter;
import net.milkbowl.vault.Vault;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.SimplePluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import sun.plugin.security.PluginClassLoader;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.core
 * Class: Core
 * <p>
 * Path: dev.perryplaysmc.core.Core
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public abstract class Core extends JavaPlugin implements Listener {

    @Getter static Core instance;

    @Getter private CommandSource console;

    @Getter private Set<User> onlineUsers;

    @Getter private Set<OfflineUser> offlineUsers;

    @Getter private Config settings, messages;

    @Getter private SimpleCommandMap simpleCommandMap;

    private List<String> listeners;

    private SimplePluginManager spm;

    private void setupSimpleCommandMap() {
        spm = (SimplePluginManager) this.getServer().getPluginManager();
        Field f = null;
        try {
            f = SimplePluginManager.class.getDeclaredField("commandMap");
        } catch (Exception e) {
            e.printStackTrace();
        }
        f.setAccessible(true);
        try {
            simpleCommandMap = (SimpleCommandMap) f.get(spm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected abstract void onInitialize();


    boolean devMode = true;

    private void initialize() {
        instance = this;

        //Generate Configs
        settings = new Config("settings");
        messages = new Config("messages");

        //Load Handlers
        if(listeners==null)
            listeners = new ArrayList<>();
        setupSimpleCommandMap();
        NMSMain.load();

        //Load Players
        if(console == null) console = new BaseCommandSource(Bukkit.getConsoleSender());
        SuperAPI.setCore(this);
        generateUsers();

        registerEvents(this, new Handle());
        SuperAPI.setCore(this);

        Log.info("" +
                        "&b<&l&m&b-------------------------&r&b>",
                "",
                StringUtils.centerText(StringUtils.ColorUtil.
                        removeColor("&b<&l&m&b-------------------------&r&b>").length() + 4, "&e&l"+getClass().getSimpleName()),
                "",
                "&b<&l&m&b-------------------------&r&b>"
        );
        onInitialize();
    }

    @EventHandler void x(PluginEnableEvent e) {
        if(e.getPlugin().getName().equalsIgnoreCase("Vault")) {
            new TimerUtility(() -> {
                if(Bukkit.getPluginManager().getPlugin("Vault") != null && Bukkit.getPluginManager().getPlugin("Vault").isEnabled())
                    if(Bukkit.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class) == null)
                        Bukkit.getServer().getServicesManager()
                                .register(net.milkbowl.vault.economy.Economy.class,
                                        new EconomyImpl(this), Vault.getPlugin(Vault.class), ServicePriority.Normal);
            }, 5, 0);
        }
    }

    @Override public void onEnable() {
        initialize();
    }


    public void registerEvents(Listener... listeners) {
        Arrays.stream(listeners).forEach(l -> {
            if(!this.listeners.contains(l.getClass().getSimpleName())) {
                Bukkit.getPluginManager().registerEvents(l, this);
                this.listeners.add(l.getClass().getSimpleName());
            }
        });
    }

    public void registerEvents(String pkg) {
        for(Class<?> clazz : ClassUtils.getAllClassesOf(getFile(), Listener.class, pkg)) {
            if(clazz.isInstance(Listener.class)) {
                try {
                    registerEvents((Listener) clazz.newInstance());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void generateUsers() {
        onlineUsers = new HashSet<>();
        for(Player p : Bukkit.getOnlinePlayers())
            onlineUsers.add(new BaseUser(p));
        offlineUsers = new HashSet<>();
        for(OfflinePlayer p : Bukkit.getOfflinePlayers())
            if(!p.isOnline())
                offlineUsers.add(new BaseOfflineUser(p));
    }

    public Player loadAsPlayer(OfflinePlayer offline) {
        final String key = offline.getUniqueId().toString();
        if (offline.isOnline()) {
            final Player loaded = offline.getPlayer();
            return loaded;
        }
        if (Bukkit.isPrimaryThread()) {
            return NMSMain.getPlayerDataLoader().loadPlayer(offline);
        }
        final Future<Player> future = Bukkit.getScheduler().callSyncMethod(this, () ->
                NMSMain.getPlayerDataLoader().loadPlayer(offline));
        int ticks = 0;
        while (!future.isDone() && !future.isCancelled() && ticks < 10) {
            ++ticks;
            try {
                Thread.sleep(50L);
                continue;
            }
            catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
        if (!future.isDone() || future.isCancelled()) {
            return null;
        }
        Player loaded;
        try {
            loaded = future.get();
        }
        catch (InterruptedException | ExecutionException e2) {
            e2.printStackTrace();
            return null;
        }
        return loaded;
    }

    public void registerCommand(Command cmd) {
        String prefix = getSettings().getString("Data.Chat.Prefixes.Command");
        CommandExecutor exe = new CommandExecutor(cmd.getName(), cmd);
        HashMap<String, List<String>> aliases = new HashMap<>();
        for(String s : cmd.getAliases()) {
            if(s.contains(":")) {
                List<String> f = aliases.containsKey(s.split(":")[0]) ? aliases.get(s.split(":")[0]) : new ArrayList<>();
                if(!f.contains(s.split(":")[1]))
                    f.add(s.split(":")[1]);
                if(s.split(":")[0].equalsIgnoreCase(cmd.getName()))continue;
                aliases.put(s.split(":")[0], f);
            }
        }
        aliases.forEach((key, value) -> {
            Command cmd1 = new Command(key, value.toArray(new String[value.size()])) {
                @Override
                public void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
                    cmd.sender = s;
                    cmd.args = args;
                    cmd.setLabel(label);
                    cmd.run(s, label, args);
                }
            };
            CommandExecutor newCmd = new CommandExecutor(key, cmd1);
            newCmd.setAliases(value);
            getSimpleCommandMap().register(newCmd.getName(), prefix, newCmd);
        });
       getSimpleCommandMap().register(exe.getName(), prefix, exe);
    }

    public void registerCommands(String pkg) {
        for(Class<Command> l : ClassUtils.getAllClassesOf(getFile(), Command.class, pkg)) {
            try {
                Command i = l.newInstance();
                registerCommand(i);
                if(i instanceof Listener)
                    registerEvents((Listener) i);
            } catch (InstantiationException | IllegalAccessException e) {
            }
        }
    }


    class Handle implements Listener {

        @EventHandler
        void onJoin(PlayerJoinEvent e) {
            getOnlineUsers().add(new BaseUser(e.getPlayer()));
            getOfflineUsers().remove(SuperAPI.getOfflineUser(e.getPlayer()));
            SuperAPI.setCore(Core.this);
            SuperAPI.getUser(e.getPlayer()).
                    getConfig().
                    set("Data.joinTime", System.currentTimeMillis());
        }

        @EventHandler
        void onLeave(PlayerQuitEvent e) {
            getOfflineUsers().add(new BaseOfflineUser(e.getPlayer()));
            getOnlineUsers().remove(SuperAPI.getUser(e.getPlayer()));
            SuperAPI.setCore(Core.this);
            SuperAPI.getOfflineUser(e.getPlayer()).
                    getConfig().
                    set("Data.quitTime", System.currentTimeMillis());
        }
    }
    public static <T extends Core> T getCore(@NotNull Class<T> clazz) {
        Validate.notNull(clazz, "Null class cannot have a plugin");
        if (!Core.class.isAssignableFrom(clazz)) {
            throw new IllegalArgumentException(clazz + " does not extend " + Core.class);
        } else {
            return clazz.cast(Core.class);
        }
    }
}
