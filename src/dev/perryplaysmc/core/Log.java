package dev.perryplaysmc.core;

import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.util.Arrays;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.core
 * Class: Logger
 * <p>
 * Path: dev.perryplaysmc.core.Logger
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Log {

    public static void complete(String... text) {
        Arrays.stream(text).forEach(t -> {
            Bukkit.getConsoleSender().sendMessage(StringUtils.ColorUtil.translateChatColor(LogType.SUCCEED.getName() + t));});
    }

    public static void info(String... text) {
        Arrays.stream(text).forEach(t -> {
            Bukkit.getConsoleSender().sendMessage(StringUtils.ColorUtil.translateChatColor(LogType.INFO.getName() + t));});
    }

    public static void warn(String... text) {
        Arrays.stream(text).forEach(t -> {
            Bukkit.getConsoleSender().sendMessage(StringUtils.ColorUtil.translateChatColor(LogType.WARNING.getName() + t));});
    }


    public static void warning(String... text) {
        Arrays.stream(text).forEach(t -> {
            Bukkit.getConsoleSender().sendMessage(StringUtils.ColorUtil.translateChatColor(LogType.WARNING.getName() + t));});
    }

    public static void error(String... text) {
        Arrays.stream(text).forEach(t -> {
            Bukkit.getConsoleSender().sendMessage(StringUtils.ColorUtil.translateChatColor(LogType.ERROR.getName() + t));});
    }


    enum LogType {
        INFO("&e&lINFO&7: &6"),
        WARNING("&d&lWARNING&7: &6"),
        ERROR("&4&lERROR&7: &6"),
        SUCCEED("&a&lCOMPLETE&7: &6");

        @Getter
        String name;
        LogType(String name) {
            this.name = StringUtils.ColorUtil.translateChatColor(name);
        }
    }

}
