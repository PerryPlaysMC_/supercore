package dev.perryplaysmc.configuration;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.sources.data.PlayerInventoryImpl;
import dev.perryplaysmc.utils.Utils;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.configuration
 * Class: Config
 * <p>
 * Path: dev.perryplaysmc.configuration.Config
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Config implements ConfigSection {

    private File file, directory;
    public static final File defaultDirectory = new File("plugins/SuperCore");
    private YamlConfiguration cfg;
    private Core core;

    public Config(String name) {
        this("plugins/SuperCore", name);
    }


    public Config(String directory, String name) {
        this(new File(directory), name);
    }


    public Config(File directory, String name) {
        this(Core.getInstance(), directory, name);
    }


    public Config(Core core, String directory, String name) {
        this(Core.getInstance(), new File(directory), name);
    }

    public Config(Core core, File directory, String name) {
        if(core==null)core=Core.getInstance();
        if(directory==null)directory = new File("plugins", "SuperCore");
        if(!directory.exists()||!directory.isDirectory())directory.mkdirs();
        this.core = core;
        this.directory = directory;
        this.file = new File(directory, name.endsWith(".yml") ? name : name+".yml");
        if(!file.exists())
            try {
                InputStream is = core.getResource(file.getName());
                if(is!=null)
                    ConfigManager.printToFile(is, file);
                else file.createNewFile();
            }catch (Exception e) {

            }
        reload();
        ConfigManager.addConfig(this);
    }

    @Override
    public File getFile() {
        return file;
    }

    @Override
    public File getDirectory() {
        return directory;
    }

    @Override
    public String getCurrentPath() {
        return cfg.getCurrentPath();
    }

    @Override
    public String getRootPath() {
        return cfg.getRoot().getCurrentPath();
    }

    @Override
    public ConfigSection set(String path, Object value) {
        if(value instanceof Location)
            cfg.set(path, Utils.convertLocation((Location)value));
        else if(value instanceof ItemStack)
            saveItem(getSection(path), (ItemStack) value);
        else if(value instanceof Inventory) {
            Inventory inv = (Inventory) value;
            if(inv instanceof PlayerInventory) {
                set(path + ".isPlayer", true);
                for(int i = 0; i < ((PlayerInventory) inv).getStorageContents().length; i++) {
                    ItemStack item = ((PlayerInventory) inv).getStorageContents()[i];
                    if(item == null) continue;
                    else saveItem(getSection(path + ".storage." + i), item);
                }
                for(int i = 0; i < ((PlayerInventory) inv).getArmorContents().length; i++) {
                    ItemStack item = ((PlayerInventory) inv).getArmorContents()[i];
                    if(item == null) continue;
                    else saveItem(getSection(path + ".armor." + i), item);
                }
                for(int i = 0; i < ((PlayerInventory) inv).getExtraContents().length; i++) {
                    ItemStack item = ((PlayerInventory) inv).getExtraContents()[i];
                    if(item == null) continue;
                    else saveItem(getSection(path + ".extra." + i), item);
                }
            }else{
                set(path + ".isPlayer", false);
                set(path + ".size", inv.getSize() + (inv.getSize() % 9 > 0 ? (9 - (inv.getSize() % 9)) : 0));
                for(int i = 0; i < inv.getSize(); i++) {
                    ItemStack item = inv.getItem(i);
                    if(item == null) continue;
                    else saveItem(getSection(path + ".contents." + i), item);
                }
            }
        }else
            cfg.set(path, value);
        save();
        return this;
    }

    @Override
    public Inventory getInventory(String path) {
        if(getBoolean(path + ".isPlayer")) {
            ItemStack[] storage = new ItemStack[36], armor = new ItemStack[4], contents = new ItemStack[41], extra = new ItemStack[1];
            PlayerInventoryImpl inv = new PlayerInventoryImpl(null, contents, storage, armor, extra);
            for(String s : getSection(path+".storage").getKeys(false)) {
                int i = Integer.parseInt(s);
                if(getString(path+".storage."+i).equalsIgnoreCase("null")) {
                    inv.setItem(i, null);
                    continue;
                }
                ItemStack item = getItem(getSection(path + ".storage."+i));
                inv.setItem(i, item);
                storage[i] = item;
            }
            for(String s : getSection(path+".armor").getKeys(false)) {
                int i = Integer.parseInt(s);
                if(getString(path+".armor."+i).equalsIgnoreCase("null")) {
                    inv.setItem(i, null);
                    continue;
                }
                ItemStack item = getItem(getSection(path + ".armor."+i));
                armor[i] = item;
            }
            for(String s : getSection(path+".extra").getKeys(false)) {
                int i = Integer.parseInt(s);
                if(getString(path+".extra."+i).equalsIgnoreCase("null")) {
                    inv.setItem(i, null);
                    continue;
                }
                ItemStack item = getItem(getSection(path + ".extra."+i));
                extra[i] = item;
            }
            inv.setArmorContents(armor);
            inv.setExtraContents(extra);
            return inv;
        }
        if(getSection(path + ".contents") == null) return null;
        Inventory inv = Bukkit.createInventory(null, getInt(path + ".size"));
        for(String s : getSection(path+".contents").getKeys(false)) {
            int i = Integer.parseInt(s);
            if(getString(path+".contents."+i).equalsIgnoreCase("null")) {
                inv.setItem(i, null);
                continue;
            }
            ItemStack item = getItem(getSection(path + ".contents."+i));
            inv.setItem(i, item);
        }

        return inv;
    }

    @Override
    public ConfigSection setIfNotSet(String path, Object value) {
        if(!isSet(path))set(path, value);
        return this;
    }

    @Override
    public Object get(String path) {
        if(getInventory(path) != null) return getInventory(path);
        return cfg.get(path);
    }

    @Override
    public String getString(String path) {
        return cfg.getString(path, "");
    }

    @Override
    public Integer getInt(String path) {
        return cfg.getInt(path, 0);
    }

    @Override
    public Double getDouble(String path) {
        return cfg.getDouble(path, 0);
    }

    @Override
    public Boolean getBoolean(String path) {
        return cfg.getBoolean(path, false);
    }

    @Override
    public Float getFloat(String path) {
        return (Float)cfg.get(path, 0f);
    }

    @Override
    public Byte getByte(String path) {
        return (Byte)cfg.get(path, 0);
    }

    @Override
    public Long getLong(String path) {
        return cfg.getLong(path, 0);
    }

    @Override
    public Location getLocation(String path) {
        return Utils.parseLocation(getString(path));
    }

    @Override
    public ItemStack getItemStack(String path) {
        return getItem(getSection(path));
    }

    @Override
    public List<String> getStringList(String path) {
        return cfg.getStringList(path);
    }

    @Override
    public List<?> getList(String path) {
        return cfg.getList(path);
    }

    @Override
    public List<Location> getLocationList(String path) {
        return Utils.parseLocations(getStringList(path));
    }

    @Override
    public Object[] getArray(String path) {
        return Utils.convert(getList(path));
    }

    @Override
    public String[] getStringArray(String path) {
        return StringUtils.convertToArray(getStringList(path));
    }

    @Override
    public Boolean isSet(String path) {
        return cfg.isSet(path);
    }

    @Override
    public Boolean contains(String path) {
        return cfg.contains(path);
    }

    @Override
    public Set<String> getKeys(boolean deep) {
        return cfg.getKeys(deep);
    }

    @Override
    public ConfigSection getSection(String path) {
        return new ImplConfigSection(this, path);
    }

    @Override
    public ConfigSection createSection(String path) {
        return new ImplConfigSection(this, path);
    }

    @Override
    public ConfigSection save() {
        try {
            cfg.save(file);
        } catch (IOException e) {

        }
        return this;
    }

    @Override
    public ConfigSection reload() {
        cfg = YamlConfiguration.loadConfiguration(file);
        return this;
    }

    @Override
    public YamlConfiguration getConfig() {
        return cfg;
    }

    private void saveItem(ConfigSection section, ItemStack item) {
        if (item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if (item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)item.getItemMeta();
            if (im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if (item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }

    private ItemStack getItem(ConfigSection section) {
        if(section == null || !isSet(section.getCurrentPath())) {
            return new ItemStack(Material.AIR);
        }
        if(Material.valueOf(section.getString("type")).name().contains("AIR")) {
            cfg.set(section.getCurrentPath(), null);
            return new ItemStack(Material.AIR);
        }
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if (i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)i.getItemMeta();
            if (section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if (section.getStringList("enchants") != null) {
                for (String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if (section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short)section.getInt("data").shortValue());
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if (section.getStringList("enchants") != null) {
            for (String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if (section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short)section.getInt("data").shortValue());
        i.setItemMeta(im);
        return i;
    }

    public void delete() {
        file.delete();
    }
}
