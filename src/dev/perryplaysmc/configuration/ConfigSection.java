package dev.perryplaysmc.configuration;

import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.configuration
 * Class: ConfigSection
 * <p>
 * Path: dev.perryplaysmc.configuration.ConfigSection
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public interface ConfigSection {

    /**
     * Gets the config file
     * @return
     */
    File getFile();

    /**
     * Gets the Files Folder
     * @return
     */
    File getDirectory();

    /**
     * Gets the current config path
     * Example: returns "this.is.a.test";
     * @return String
     */
    String getCurrentPath();

    /**
     * Returns the root path
     * @return String
     */
    String getRootPath();

    /**
     * Sets a value to the config with a provided path
     * Example: set("this.is.a.test", "This is the value");
     * @param path
     * @param value
     * @return ConfigSection
     */
    ConfigSection set(String path, Object value);

    /**
     * Sets a value to the config ONLY IF IT IS NOT SET
     * @param path
     * @param value
     * @return
     */
    ConfigSection setIfNotSet(String path, Object value);

    /**
     * Gets an Object from the provided path
     * @param path
     * @return Object
     */
    Object get(String path);

    /**
     * Gets a String from the provided path
     * @param path
     * @return String
     */
    String getString(String path);

    /**
     * Gets an integer from the provided path
     * @param path
     * @return Integer(1)
     */
    Integer getInt(String path);

    /**
     * Gets a Double from the provided path
     * @param path
     * @return Double(1.5)
     */
    Double getDouble(String path);

    /**
     * Gets a Boolean from the provided path
     * @param path
     * @return Boolean(true/false)
     */
    Boolean getBoolean(String path);

    /**
     * Gets a Float from the provided path
     * @param path
     * @return Float(1>0.1)
     */
    Float getFloat(String path);

    /**
     * Gets a Byte from the provided path
     * @param path
     * @return Byte
     */
    Byte getByte(String path);

    /**
     * Gets a Long from the provided path
     * @param path
     * @return Long
     */
    Long getLong(String path);

    /**
     * Gets a Location from the provided path
     * @param path
     * @return Location
     */
    Location getLocation(String path);

    /**
     * Gets an ItemStack from the provided path
     * @param path
     * @return ItemStack
     */
    ItemStack getItemStack(String path);

    /**
     * Gets an Inventory from the provided path
     * @param path
     * @return Inventory
     */
    Inventory getInventory(String path);

    /**
     * Gets a list of String from the provided path
     * @param path
     * @return List of Strings
     */
    List<String> getStringList(String path);

    /**
     * Gets a list of Objects from the provided path
     * @param path
     * @return List of Objects
     */
    List<?> getList(String path);

    /**
     * Gets a list of Locations from the porvided path
     * @param path
     * @return
     */
    List<Location> getLocationList(String path);
    
    /**
     * Gets a Object Array from the provided path
     * @param path
     * @return Array of Objects
     */
    Object[] getArray(String path);

    /**
     * Gets a String Array from the provided path
     * @param path
     * @return Array of Strings
     */
    String[] getStringArray(String path);

    /**
     * Tells you if the provided path is set or not
     * @param path
     * @return Boolean(true/false)
     */
    Boolean isSet(String path);

    /**
     * Tells you if the provided path is in the config
     * @param path
     * @return Boolean(true/false)
     */
    Boolean contains(String path);

    /**
     * Gets all values from the current path
     * @param deep Should it pull values aswell
     * @return Set of Values
     */
    Set<String> getKeys(boolean deep);

    /**
     * Get a Configuration Section from provided path
     * @param path
     * @return new ConfigSection
     */
    ConfigSection getSection(String path);

    /**
     * Creates a new Config section
     * @param path
     * @return new ConfigSection
     */
    ConfigSection createSection(String path);

    /**
     * Saves the config file
     * @return this
     */
    ConfigSection save();

    /**
     * Reloads the configuration file
     * @return this
     */
    ConfigSection reload();

    /**
     * Get the current config
     * @return Config
     */
    YamlConfiguration getConfig();
}
