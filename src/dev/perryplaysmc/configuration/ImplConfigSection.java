package dev.perryplaysmc.configuration;

import dev.perryplaysmc.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.configuration
 * Class: ImplConfigSection
 * <p>
 * Path: dev.perryplaysmc.configuration.ImplConfigSection
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ImplConfigSection implements ConfigSection {

    Config config;
    ConfigurationSection cfg;

    public ImplConfigSection(Config config, String path) {
        this.config = config;
        this.cfg = config.getConfig().getConfigurationSection(path) == null
                ? config.getConfig().createSection(path)
                : config.getConfig().getConfigurationSection(path);
    }


    @Override
    public File getFile() {
        return config.getFile();
    }

    @Override
    public File getDirectory() {
        return config.getDirectory();
    }

    @Override
    public String getCurrentPath() {
        return cfg.getCurrentPath();
    }

    @Override
    public String getRootPath() {
        return cfg.getRoot().getCurrentPath();
    }

    @Override
    public ConfigSection set(String path, Object value) {
        if(value instanceof Location)
            cfg.set(path, Utils.convertLocation((Location)value));
        else if(value instanceof ItemStack)
            saveItem(getSection(path), (ItemStack) value);
        else if(value instanceof Inventory) {
            Inventory inv = (Inventory) value;
            for(int i = 0; i < inv.getContents().length; i++) {
                ItemStack item = inv.getContents()[i];
                if(item == null) set(path + ".contents." + i, "null");
                else saveItem(getSection(path + ".contents." + i), item);
            }
            if(inv instanceof PlayerInventory) {
                for(int i = 0; i < ((PlayerInventory) inv).getArmorContents().length; i++) {
                    ItemStack item = ((PlayerInventory) inv).getArmorContents()[i];
                    if(item == null) set(path + ".armor." + i, "null");
                    else saveItem(getSection(path + ".armor." + i), item);
                }
                for(int i = 0; i < ((PlayerInventory) inv).getExtraContents().length; i++) {
                    ItemStack item = ((PlayerInventory) inv).getExtraContents()[i];
                    if(item == null) set(path + ".extra." + i, "null");
                    else saveItem(getSection(path + ".extra." + i), item);
                }
            }
        }else
            cfg.set(path, value);
        save();
        return this;
    }

    @Override
    public Inventory getInventory(String path) {
        if(getSection(path + ".contents") == null) return null;
        Inventory inv = Bukkit.createInventory(null, getSection(path + ".contents").getKeys(false).size());
        for(int i = 0; i < inv.getSize(); i++) {
            if(getString(path+".contents."+i).equalsIgnoreCase("null")) {
                inv.setItem(i, null);
                continue;
            }
            ItemStack item = getItem(getSection(path + ".contents."+i));
            inv.setItem(i, item);
        }
        return inv;
    }

    @Override
    public ConfigSection setIfNotSet(String path, Object value) {
        if(!isSet(path))set(path, value);
        return this;
    }

    @Override
    public Object get(String path) {
        if(getInventory(path) != null) return getInventory(path);
        return cfg.get(path);
    }

    @Override
    public String getString(String path) {
        return cfg.getString(path);
    }

    @Override
    public Integer getInt(String path) {
        return cfg.getInt(path);
    }

    @Override
    public Double getDouble(String path) {
        return cfg.getDouble(path);
    }

    @Override
    public Boolean getBoolean(String path) {
        return cfg.getBoolean(path);
    }

    @Override
    public Float getFloat(String path) {
        return (Float)cfg.get(path);
    }

    @Override
    public Byte getByte(String path) {
        return (Byte)cfg.get(path);
    }

    @Override
    public Long getLong(String path) {
        return cfg.getLong(path);
    }

    @Override
    public Location getLocation(String path) {
        return Utils.parseLocation(getString(path));
    }

    @Override
    public ItemStack getItemStack(String path) {
        return getItem(getSection(path));
    }

    @Override
    public List<String> getStringList(String path) {
        return cfg.getStringList(path);
    }

    @Override
    public List<?> getList(String path) {
        return cfg.getList(path);
    }

    @Override
    public List<Location> getLocationList(String path) {
        return Utils.parseLocations(getStringList(path));
    }

    @Override
    public Object[] getArray(String path) {
        return Utils.convert(getList(path));
    }

    @Override
    public String[] getStringArray(String path) {
        return Utils.<String>convert(getStringList(path));
    }

    @Override
    public Boolean isSet(String path) {
        return cfg.isSet(path);
    }

    @Override
    public Boolean contains(String path) {
        return cfg.contains(path);
    }

    @Override
    public Set<String> getKeys(boolean deep) {
        return cfg.getKeys(deep);
    }

    @Override
    public ConfigSection getSection(String path) {
        return new ImplConfigSection(config, path);
    }

    @Override
    public ConfigSection createSection(String path) {
        return new ImplConfigSection(config, path);
    }

    @Override
    public ConfigSection save() {
        config.save();
        return this;
    }

    @Override
    public ConfigSection reload() {
        config.reload();
        return this;
    }

    @Override
    public YamlConfiguration getConfig() {
        return config.getConfig();
    }



    private void saveItem(ConfigSection section, ItemStack item) {
        if (item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if (item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)item.getItemMeta();
            if (im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if (item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if (im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for (Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if (enchants.size() > 0)
                section.set("enchants", enchants);
            if (im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }

    private ItemStack getItem(ConfigSection section) {
        if(section == null || !isSet(section.getCurrentPath())) {
            return new ItemStack(Material.AIR);
        }
        if(Material.valueOf(section.getString("type")).name().contains("AIR")) {
            cfg.set(section.getCurrentPath(), null);
            return new ItemStack(Material.AIR);
        }
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if (i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta)i.getItemMeta();
            if (section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if (section.getStringList("enchants") != null) {
                for (String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if (section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short)section.getInt("data").shortValue());
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if (section.getStringList("enchants") != null) {
            for (String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if (section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short)section.getInt("data").shortValue());
        i.setItemMeta(im);
        return i;
    }

}
