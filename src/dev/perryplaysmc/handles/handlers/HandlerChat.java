package dev.perryplaysmc.handles.handlers;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.handles.Handler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.handles.handlers
 * Class: HandlerChat
 * <p>
 * Path: dev.perryplaysmc.handles.handlers.HandlerChat
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class HandlerChat extends Handler {

    Core core = Core.getInstance();

    @Override
    public void onInitialize() {
        System.out.println("Loading chat handler");
        core.registerEvents(this);
    }

    @Override
    public void onChat(AsyncPlayerChatEvent e) {

    }
}
