package dev.perryplaysmc.handles;

import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.handles
 * Class: Handler
 * <p>
 * Path: dev.perryplaysmc.handles.Handler
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public abstract class Handler implements Listener {

    public abstract void onInitialize();


    public void onChat(AsyncPlayerChatEvent e) {

    }

    public void onBlockBreak(BlockBreakEvent e) {

    }

    public void onBlockPlace(BlockPlaceEvent e) {

    }

    public void onCommand(PlayerCommandPreprocessEvent e) {

    }


}
