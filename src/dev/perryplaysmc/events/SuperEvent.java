package dev.perryplaysmc.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.events
 * Class: SuperEvent
 * <p>
 * Path: dev.perryplaysmc.events.SuperEvent
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public abstract class SuperEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public SuperEvent() {
        super(false);
    }

    public SuperEvent(boolean isAsync) {
        super(isAsync);
    }

    @NotNull
    public HandlerList getHandlers() {
        return handlers;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

}
