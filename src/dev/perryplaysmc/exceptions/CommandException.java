package dev.perryplaysmc.exceptions;

import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.Getter;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.exceptions
 * Class: CommandException
 * <p>
 * Path: dev.perryplaysmc.exceptions.CommandException
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandException extends Exception {

    private String message;
    @Getter
    private boolean ignore = false;

    public CommandException() {
        super("");
        ignore = true;
    }
    public CommandException(String message) {
        super(message);
        this.message=message;
        ignore = false;
    }
    public CommandException(String path, Object... objects) {
        super(StringUtils.Formatter.formatDefault(path = (path.startsWith("Messages.") ? path : "Messages." + path), objects));
        this.message=StringUtils.Formatter.formatDefault(path, objects);
        ignore = false;
    }

    @Override
    public String getMessage() {
        return StringUtils.translate(message);
    }

}
