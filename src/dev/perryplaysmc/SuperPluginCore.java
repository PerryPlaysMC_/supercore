package dev.perryplaysmc;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.utils.inventory.menus.GuiHandler;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import dev.perryplaysmc.utils.text.strings.placeholders.holders.LocationPlaceholder;
import dev.perryplaysmc.utils.text.strings.placeholders.holders.UserPlaceholder;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class SuperPluginCore extends Core implements Listener {

    List<String> sleepers = new ArrayList<>();
    @Override protected void onInitialize() {
        registerEvents(new GuiHandler(), this);
        new UserPlaceholder();
        new LocationPlaceholder();
    }
    @EventHandler
    void sleep(final PlayerBedEnterEvent e) {
        if(e.isCancelled()) return;
        sleepers.add(e.getPlayer().getName());
        new BukkitRunnable()
        {
            public void run() {
                if(!sleepers.contains(e.getPlayer().getName())) {
                    return;
                }
                org.bukkit.World w = e.getPlayer().getWorld();
                w.setStorm(false);
                w.setThundering(false);
                e.getPlayer().getWorld().setWeatherDuration(0);
                e.getPlayer().getWorld().setTime(8L);
                Bukkit.broadcastMessage(StringUtils.translate(
                        "[pc]" + e.getPlayer().getName() + "[c] Went to sleep, &o&7Sweet dreams"));
                sleepers.remove(e.getPlayer().getName());
            }

        }.runTaskLater(this, 100L);
    }

    @EventHandler
    void leaveBed(PlayerBedLeaveEvent e) {
        sleepers.remove(e.getPlayer().getName());
    }
}
