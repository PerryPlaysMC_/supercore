package dev.perryplaysmc.enchantment;

public enum Levels {

    LEVELS_25_30, LEVELS_20_25, LEVELS_15_20, LEVELS_10_15, LEVELS_6_10, LEVELS_0_6

}
