package dev.perryplaysmc.enchantment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.utils.nmsUtils.classes.versionUtil.Version;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class EnchantHandler {
    private static List<CustomEnchant> enchants;


    public static List<CustomEnchant> getEnchantments() {
        if (enchants == null) {
            enchants = new ArrayList<>();
        }
        return enchants;
    }

    public static List<Enchantment> getAllEnchantments() {
        List<Enchantment> enchants = new ArrayList<>();
        enchants.addAll(getEnchantments());
        enchants.addAll(Arrays.asList(Enchantment.values()));
        return enchants;
    }

    public static Object getType(String name) {
        if(Version.isCurrentHigher(Version.v1_12)) {
            return new NamespacedKey(Core.getInstance(), StringUtils.ColorUtil.removeColor(name.toLowerCase()));
        }
        return 71+(getEnchantments().size()+1);
    }
    public static CustomEnchant getEnchantment(String name) {
        for (CustomEnchant ench : getEnchantments()) {
            if (ench.getName().equalsIgnoreCase(name) || StringUtils.ColorUtil.removeColor(ench.getName()).equalsIgnoreCase(StringUtils.ColorUtil.removeColor(name))) {
                return ench;
            }
        }
        return null;
    }

    public static boolean hasCustomEnchant(ItemStack item) {
        boolean ret = false;
        for (CustomEnchant ench : getEnchantments()) {
            if (item.containsEnchantment(ench)) {
                ret = true;
            }
        }
        return ret;
    }

    private static void loadEnchant(CustomEnchant ench) {
        try {
            try {
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.set(null, true);
                f.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Enchantment.registerEnchantment(ench);
            } catch (IllegalArgumentException ignored) {
            }

            try {
                Enchantment.stopAcceptingRegistrations();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addEnchantments(CustomEnchant... enchantments) {
        for (CustomEnchant e : enchantments) {
            if (!getEnchantments().contains(e)) {
                getEnchantments().add(e);
                loadEnchant(e);
            }
        }
    }

    public static void resetEnchantments() {
        for (CustomEnchant ench : getEnchantments()) {
            try {
                Field byKeyField = Enchantment.class.getDeclaredField("byKey");
                Field byNameField = Enchantment.class.getDeclaredField("byName");

                byKeyField.setAccessible(true);
                byNameField.setAccessible(true);

                HashMap<Integer, Enchantment> byKey = (HashMap)byKeyField.get(null);
                HashMap<Integer, Enchantment> byName = (HashMap)byNameField.get(null);

                if (byKey.containsKey(ench.getKey())) {
                    byKey.remove(ench.getKey());
                }

                if (byName.containsKey(ench.getName())) {
                    byName.remove(ench.getName());
                }
            }
            catch (Exception localException) {}
        }
    }
}
