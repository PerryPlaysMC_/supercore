package dev.perryplaysmc.utils;

import dev.perryplaysmc.utils.text.strings.StringUtils;

import java.util.Arrays;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: Parser
 * <p>
 * Path: dev.perryplaysmc.utils.Parser
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Parser {

   private static String parse = "/";

    public static String parseData(Parsed parsed) {
        String data = parsed.parsedClass.getName();
        for(Object obj : parsed.toParse) {
            data+=parse + obj.getClass().getName() + ":" + StringUtils.findValue(obj);
        }
        return data;
    }

    public static Parsed parse(String parsed) {
        try {
            String[] text = Arrays.copyOfRange(parsed.split(parse), 1, parsed.split(parse).length);
            Object[] obj = new Object[text.length];
            for(int i = 0; i < text.length; i++) {
                String clazz = text[i].split(":")[0];
                String value = text[i].split(":")[1];
                obj[i] = StringUtils.tryCast(Class.forName(clazz), value);
            }
            Parsed p = new Parsed(Class.forName(parsed.split(parse)[0]), obj);
            return p;
        } catch (Exception e) {
            e.printStackTrace();
            return new Parsed(String.class, new Object[]{"Invalid parserString: " + parsed});
        }
    }

}
