package dev.perryplaysmc.utils;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.core.Log;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/28/19-2023
 * Package: me.perryplaysmc.base.permissions
 * Path: me.perryplaysmc.base.permissions.EconomyImpl
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class EconomyImpl implements Economy {
    private final String name = Core.getInstance().getName();
    public EconomyImpl(Plugin plugin) {
        // Load Plugin in case it was loaded before
        Plugin perms = plugin.getServer().getPluginManager().getPlugin(name);
        if (perms != null) {
            if (perms.isEnabled()) {
                try {
                    if (Double.valueOf(perms.getDescription().getVersion()) < 1.16) {
                        Log.info(String.format("[%s][Economy] %s below 1.16 is not compatible with Vault! Falling back to SuperPerms only mode. PLEASE UPDATE!",
                                plugin.getDescription().getName(), name));
                    }
                } catch (NumberFormatException e) {
                    // Do nothing
                }
                Log.info(String.format("[%s][Economy] %s hooked.", plugin.getDescription().getName(), name));
            }
        }
    }
    
    @Override
    public boolean isEnabled() {
        return true;
    }
    
    @Override
    public String getName() {
        return Core.getInstance().getName();
    }
    
    @Override
    public boolean hasBankSupport() {
        return false;
    }
    
    @Override
    public int fractionalDigits() {
        return 2;
    }
    
    @Override
    public String format(double v) {
        return NumberUtil.format(v);
    }
    
    @Override
    public String currencyNamePlural() {
        return "$";
    }
    
    @Override
    public String currencyNameSingular() {
        return "$";
    }
    
    @Override
    public boolean hasAccount(String s) {
        return true;
    }
    
    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer) {
        return true;
    }
    
    @Override
    public boolean hasAccount(String s, String s1) {
        return true;
    }
    
    @Override
    public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
        return true;
    }
    
    @Override
    public double getBalance(String s) {
        return SuperAPI.getUser(s).getBalance();
    }
    
    @Override
    public double getBalance(OfflinePlayer offlinePlayer) {
        return getBalance(offlinePlayer.getName());
    }
    
    @Override
    public double getBalance(String s, String s1) {
        return getBalance(s);
    }
    
    @Override
    public double getBalance(OfflinePlayer offlinePlayer, String s) {
        return getBalance(offlinePlayer);
    }
    
    @Override
    public boolean has(String s, double v) {
        return SuperAPI.getUser(s).hasEnough(v);
    }
    
    @Override
    public boolean has(OfflinePlayer offlinePlayer, double v) {
        return has(offlinePlayer.getName(), v);
    }
    
    @Override
    public boolean has(String s, String s1, double v) {
        return has(s, v);
    }
    
    @Override
    public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
        return has(offlinePlayer, v);
    }
    
    @Override
    public EconomyResponse withdrawPlayer(String s, double v) {
        User acc = SuperAPI.getUser(s);
        if(acc.hasEnough(v)) {
            acc.withdraw(v);
            return new EconomyResponse(v, acc.getBalance(), EconomyResponse.ResponseType.SUCCESS, "Error while taking money from " + s);
        }
        return new EconomyResponse(v, acc.getBalance(), EconomyResponse.ResponseType.FAILURE, "Error while taking money from " + s);
    }
    
    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double v) {
        return withdrawPlayer(offlinePlayer.getName(), v);
    }
    
    @Override
    public EconomyResponse withdrawPlayer(String s, String s1, double v) {
        return withdrawPlayer(s, v);
    }
    
    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        return withdrawPlayer(offlinePlayer.getName(), v);
    }
    
    @Override
    public EconomyResponse depositPlayer(String s, double v) {
        User acc = SuperAPI.getUser(s);
        if(!acc.exceedsMax(false, v)) {
            acc.deposit(v);
            return new EconomyResponse(v, acc.getBalance(), EconomyResponse.ResponseType.SUCCESS, "Error while giving money to " + s);
        }
        return new EconomyResponse(v, acc.getBalance(), EconomyResponse.ResponseType.FAILURE, "Error while giving money to " + s);
    }
    
    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double v) {
        return depositPlayer(offlinePlayer.getName(), v);
    }
    
    @Override
    public EconomyResponse depositPlayer(String s, String s1, double v) {
        return depositPlayer(s, v);
    }
    
    @Override
    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        return depositPlayer(offlinePlayer.getName(), v);
    }
    
    @Override
    public EconomyResponse createBank(String s, String s1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse deleteBank(String s) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse bankBalance(String s) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse bankHas(String s, double v) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse bankWithdraw(String s, double v) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse bankDeposit(String s, double v) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse isBankOwner(String s, String s1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse isBankMember(String s, String s1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Error this is not supported");
    }
    
    @Override
    public List<String> getBanks() {
        return new ArrayList<>();
    }
    
    @Override
    public boolean createPlayerAccount(String s) {
        return true;
    }
    
    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
        return createPlayerAccount(offlinePlayer.getName());
    }
    
    @Override
    public boolean createPlayerAccount(String s, String s1) {
        return createPlayerAccount(s);
    }
    
    @Override
    public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
        return createPlayerAccount(offlinePlayer.getName());
    }
}
