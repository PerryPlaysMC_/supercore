package dev.perryplaysmc.utils;

import dev.perryplaysmc.utils.text.strings.StringUtils;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: Parsed
 * <p>
 * Path: dev.perryplaysmc.utils.Parsed
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Parsed {

    Object[] toParse;
    Class<?> parsedClass;

    public Parsed(Class<?> parsedClass, Object[] toParse) {
        this.parsedClass = parsedClass;
        this.toParse = toParse;
    }

    public Class<?> getParsedClass() {
        return parsedClass;
    }

    public Object[] getToParse() {return toParse;}

    @Override
    public String toString() {
        return "Parsed{" +
                "toParse=" + StringUtils.toString(toParse) +
                ", parsedClass=" + parsedClass +
                '}';
    }
}
