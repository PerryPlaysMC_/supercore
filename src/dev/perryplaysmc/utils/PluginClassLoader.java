package dev.perryplaysmc.utils;


import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSigner;
import java.security.CodeSource;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import com.sun.istack.internal.NotNull;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.plugin.InvalidPluginException;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginLoader;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: PluginClassLoader
 * <p>
 * Path: dev.perryplaysmc.utils.PluginClassLoader
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

final class PluginClassLoader extends URLClassLoader {
    private final JavaPluginLoader loader;
    private final Map<String, Class<?>> classes = new ConcurrentHashMap<>();
    private final PluginDescriptionFile description;
    private final File dataFolder;
    private final File file;
    private final JarFile jar;
    private final Manifest manifest;
    private final URL url;
    final JavaPlugin plugin;
    private JavaPlugin pluginInit;
    private IllegalStateException pluginState;
    
    static {
        ClassLoader.registerAsParallelCapable();
    }
    
    PluginClassLoader(@NotNull JavaPluginLoader loader, @Nullable ClassLoader parent, @NotNull PluginDescriptionFile description, @NotNull File dataFolder, @NotNull File file)
            throws IOException, InvalidPluginException {
        super(new URL[]{file.toURI().toURL()}, parent);
        Validate.notNull(loader, "Loader cannot be null");
        this.loader = loader;
        this.description = description;
        this.dataFolder = dataFolder;
        this.file = file;
        this.jar = new JarFile(file);
        this.manifest = this.jar.getManifest();
        this.url = file.toURI().toURL();
        
        try {
            Class jarClass;
            try {
                JarFile f = new JarFile(file);
                System.out.println(findClass(description.getMain()).getName());
                jarClass = findClass(description.getMain());//Class.forName(description.getMain(), true, this);
            } catch (ClassNotFoundException var10) {
                throw new InvalidPluginException("Cannot find main class `" + description.getMain() + "'", var10);
            }
            
            Class pluginClass;
            try {
                pluginClass = jarClass.asSubclass(JavaPlugin.class);
            } catch (ClassCastException var9) {
                throw new InvalidPluginException("main class `" + description.getMain() + "' does not extend JavaPlugin", var9);
            }
            
            this.plugin = (JavaPlugin)pluginClass.newInstance();
        } catch (IllegalAccessException var11) {
            throw new InvalidPluginException("No public constructor", var11);
        } catch (InstantiationException var12) {
            throw new InvalidPluginException("Abnormal plugin type", var12);
        }
    }
    
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        return this.findClass(name, true);
    }
    
    public Class<?> findClass(@NotNull String name, boolean checkGlobal) throws ClassNotFoundException {
        if (!name.startsWith("org.bukkit.") && !name.startsWith("net.minecraft.")) {
            Class<?> result = this.classes.get(name);
            if (result == null) {
                if (checkGlobal) {
                    try{
                        Method m = loader.getClass().getMethod("getClassByName", String.class);
                        m.setAccessible(true);
                        result = (Class<?>) m.invoke(loader, name);
                    }catch (Exception e) {
                        System.out.println("Error can't access getClassByName method");
                    }
                }
                
                if (result == null) {
                    String path = name.replace('.', '/').concat(".class");
                    JarEntry entry = this.jar.getJarEntry(path);
                    if (entry != null) {
                        byte[] classBytes;
                        String pkgName;
                        try {
                            Throwable var7 = null;
                            pkgName = null;
                            
                            try {
                                InputStream is = this.jar.getInputStream(entry);
                                
                                try {
                                    classBytes = ByteStreams.toByteArray(is);
                                } finally {
                                    if (is != null) {
                                        is.close();
                                    }
                                    
                                }
                            } catch (Throwable var19) {
                                if (var7 == null) {
                                    var7 = var19;
                                } else if (var7 != var19) {
                                    var7.addSuppressed(var19);
                                }
                                
                                throw var7;
                            }
                        } catch (Throwable var20) {
                            throw new ClassNotFoundException(name, var20);
                        }
                        
                        classBytes = Bukkit.getServer().getUnsafe().processClass(this.description, path, classBytes);
                        int dot = name.lastIndexOf(46);
                        if (dot != -1) {
                            pkgName = name.substring(0, dot);
                            if (this.getPackage(pkgName) == null) {
                                try {
                                    if (this.manifest != null) {
                                        this.definePackage(pkgName, this.manifest, this.url);
                                    } else {
                                        this.definePackage(pkgName, null, null, null, null, null, null, null);
                                    }
                                } catch (IllegalArgumentException var21) {
                                    if (this.getPackage(pkgName) == null) {
                                        throw new IllegalStateException("Cannot find package " + pkgName);
                                    }
                                }
                            }
                        }
                        
                        CodeSigner[] signers = entry.getCodeSigners();
                        CodeSource source = new CodeSource(this.url, signers);
                        result = this.defineClass(name, classBytes, 0, classBytes.length, source);
                    }
                    
                    if (result == null) {
                        result = super.findClass(name);
                    }
                    
                    if (result != null) {
                        try {
                            Method m = loader.getClass().getMethod("setClass", String.class, Class.class);
                            m.setAccessible(true);
                            m.invoke(loader, name, result);
                        }catch (Exception e) {
                            System.out.println("Invalid method setClass");
                        }
                    }
                }
                
                this.classes.put(name, result);
            }
            
            return result;
        } else {
            throw new ClassNotFoundException(name);
        }
    }
    
    public void close() throws IOException {
        try {
            super.close();
        } finally {
            this.jar.close();
        }
        
    }
    
    @NotNull
    Set<String> getClasses() {
        return this.classes.keySet();
    }
    
    synchronized void initialize(@NotNull JavaPlugin javaPlugin) {
        Validate.notNull(javaPlugin, "Initializing plugin cannot be null");
        Validate.isTrue(javaPlugin.getClass().getClassLoader() == this, "Cannot initialize plugin outside of this class loader");
        if (this.plugin == null && this.pluginInit == null) {
            this.pluginState = new IllegalStateException("Initial initialization");
            this.pluginInit = javaPlugin;
            try {
                Method m = javaPlugin.getClass().getMethod("init", PluginLoader.class, Server.class, PluginDescriptionFile.class, File.class, File.class, ClassLoader.class);
                m.setAccessible(true);
                m.invoke(javaPlugin,this.loader, Bukkit.getServer(), this.description, this.dataFolder, this.file, this);
            }catch (Exception e) {
                System.out.println("Error");
            }
        } else {
            throw new IllegalArgumentException("Plugin already initialized!", this.pluginState);
        }
    }
}
