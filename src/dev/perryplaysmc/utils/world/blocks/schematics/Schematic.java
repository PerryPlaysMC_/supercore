package dev.perryplaysmc.utils.world.blocks.schematics;

import dev.perryplaysmc.utils.inventory.MaterialHelper;
import dev.perryplaysmc.utils.nmsUtils.classes.versionUtil.Version;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.material.MaterialData;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 12/10/19-2023
 * Package: me.perryplaysmc.base.world.schematics
 * Path: me.perryplaysmc.base.world.schematics.Schematic
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Schematic {

    private int[][][] blocks;
    private int[][][] data;
    private int rotation;
    private String name;

    public Schematic(String name, int[][][] blocks, int[][][] data) {
        this.blocks = blocks;
        this.data = data;
        rotation = 0;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void save(String name) {
        SchematicUtil.save(name, this);
    }

    /**
     * @return the blocks
     */
    public int[][][] getBlocks() {
        return blocks;
    }

    /**
     * @return the data
     */
    public int[][][] getData() {
        return data;
    }

    public Schematic rotate(int rotation) {
        this.rotation = (this.rotation + rotation) % 4;
        return this;
    }

    public void paste(Location loc) {
        World world = loc.getWorld();
        int[][][] blocks = getBlocks();
        int[][][] blockData = getData();
        System.out.println("Rotation: " + rotation);
        if(rotation == 0) {
            for (int x = 0; x < blocks.length; x++) {
                for (int y = 0; y < blocks[x].length; y++) {
                    for (int z = 0; z < blocks[x][y].length; z++) {

                        Block block = new Location(world, x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock();
                        //block.setType(blocks[index], blockData[index], true);
                        if(Version.isCurrentLower(Version.v1_13)) {
                            block.setType(MaterialHelper.byId(blocks[x][y][z]));
                            block.getState().getData().setData((byte) blockData[x][y][z]);
                            block.getState().update(true);
                        } else {
                            block.setType(Bukkit.getUnsafe().fromLegacy(new MaterialData(MaterialHelper.byId(blocks[x][y][z]), (byte) blockData[x][y][z])));
                            block.getState().setRawData((byte) blockData[x][y][z]);
                            block.getState().update(true, false);
                        }
                    }
                }
            }
        } else if(rotation == 1) {
            for (int z = 0; z < blocks.length; z++) {
                for (int y = 0; y < blocks[z].length; y++) {
                    for (int x = 0; x < blocks[z][y].length; x++) {

                        Block block = new Location(world, x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock();
                        //block.setType(blocks[index], blockData[index], true);
                        if(Version.isCurrentLower(Version.v1_13)) {
                            block.setType(MaterialHelper.byId(blocks[x][y][z]));
                            block.getState().getData().setData((byte) blockData[x][y][z]);
                            block.getState().update(true);
                        } else {
                            block.setType(Bukkit.getUnsafe().fromLegacy(new MaterialData(MaterialHelper.byId(blocks[z][y][x]), (byte) blockData[z][y][x])));
                        }
                    }
                }
            }
        } else if(rotation == 2) {
            int lz = 0;
            int lx = 0;
            for (int z = blocks.length - 1; z > -1; z--) {
                lz = (blocks.length-1) - z;
                for (int y = 0; y < blocks[z].length; y++) {
                    for (int x = blocks[z][y].length - 1; x > -1; x--) {
                        lx = (blocks[z][y].length-1) - x;
                        Block block = new Location(world, x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock();
                        //block.setType(blocks[index], blockData[index], true);
                        if(Version.isCurrentLower(Version.v1_13)) {
                            block.setType(MaterialHelper.byId(blocks[lz][y][lx]));
                            block.getState().getData().setData((byte) blockData[lz][y][lx]);
                            block.getState().update(true);
                        } else {
                            block.setType(Bukkit.getUnsafe().fromLegacy(new MaterialData(MaterialHelper.byId(blocks[lz][y][lx]), (byte) blockData[lz][y][lx])));
                        }
                    }
                }
            }
        } else if(rotation == 3) {
            int lz = 0;
            int lx = 0;
            for (int x = blocks.length - 1; x > -1; x--) {
                lx = (blocks.length-1) - x;
                for (int y = 0; y < blocks[x].length; y++) {
                    for (int z = blocks[x][y].length - 1; z > -1; z--) {
                        lz = (blocks[x][y].length-1) - z;
                        Block block = new Location(world, x + loc.getX(), y + loc.getY(), z + loc.getZ()).getBlock();
                        //block.setType(blocks[index], blockData[index], true);
                        if(Version.isCurrentLower(Version.v1_13)) {
                            block.setType(MaterialHelper.byId(blocks[lx][y][lz]));
                            block.getState().getData().setData((byte) blockData[lx][y][lz]);
                            block.getState().update(true);
                        } else {
                            block.setType(Bukkit.getUnsafe().fromLegacy(new MaterialData(MaterialHelper.byId(blocks[lx][y][lz]), (byte) blockData[lx][y][lz])));
                        }
                    }
                }
            }
        }
    }
}
