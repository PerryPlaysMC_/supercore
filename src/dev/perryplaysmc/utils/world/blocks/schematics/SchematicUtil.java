package dev.perryplaysmc.utils.world.blocks.schematics;

import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.utils.inventory.MaterialHelper;
import org.bukkit.Location;
import org.bukkit.block.Block;

import java.io.*;
import java.util.Arrays;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 12/12/19-2023
 * Package: me.perryplaysmc.base.world.schematics
 * Path: me.perryplaysmc.base.world.schematics.SchematicUtil
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class SchematicUtil {
    /**
     * Get all blocks between two points and return a 3d array
     */
    
    public static Schematic generateSchematic(Location block, Location block2){
        int minX, minZ, minY;
        int maxX, maxZ, maxY;
        

        minX = block.getBlockX() < block2.getBlockX() ? block.getBlockX() : block2.getBlockX();
        minZ = block.getBlockZ() < block2.getBlockZ() ? block.getBlockZ() : block2.getBlockZ();
        minY = block.getBlockY() < block2.getBlockY() ? block.getBlockY() : block2.getBlockY();
        
        maxX = block.getBlockX() > block2.getBlockX() ? block.getBlockX() : block2.getBlockX();
        maxZ = block.getBlockZ() > block2.getBlockZ() ? block.getBlockZ() : block2.getBlockZ();
        maxY = block.getBlockY() > block2.getBlockY() ? block.getBlockY() : block2.getBlockY();
        
        int[][][] blocks = new int[maxX-minX+1][maxY-minY+1][maxZ-minZ+1];
        int[][][] blockData = new int[maxX-minX+1][maxY-minY+1][maxZ-minZ+1];
        
        for(int x = minX; x <= maxX; x++){
            for(int y = minY; y <= maxY; y++){
                for(int z = minZ; z <= maxZ; z++){
                    Block b = block.getWorld().getBlockAt(x, y, z);
                    blocks[x-minX][y-minY][z-minZ] = MaterialHelper.getId(b.getType());
                    blockData[x-minX][y-minY][z-minZ] = b.getData();
                }
            }
        }
//    public static Schematic(byte[] blocks, byte[] data, short width, short lenght, short height)
        
        return new Schematic("", blocks, blockData);
        
    }
    
    public static Schematic load(String name){
        
        File f = new File(Config.defaultDirectory,"schematics/" + name + ".schem");
        if(!f.exists()) {
            return null;
        }
        int[][][][] loaded = new int[0][0][0][0];
        
        try {
            FileInputStream streamIn = new FileInputStream(f);
            ObjectInputStream objectinputstream = new ObjectInputStream(streamIn);
            loaded = (int[][][][])objectinputstream.readObject();
            
            objectinputstream.close();
            
        } catch (Exception e) {
            
            e.printStackTrace();
        }
        
        return new Schematic(name, loaded[0], loaded[1]);
    }
    
    /**
     * Save a structure with a desired name
     */
    
    public static void save(String name, Schematic schem){
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        
        File f = new File(Config.defaultDirectory, "schematics/" + name + ".schem");
        File dir = new File(Config.defaultDirectory, "schematics");
        
        try {
            dir.mkdirs();
            f.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        
        try{
            fout = new FileOutputStream(f);
            oos = new ObjectOutputStream(fout);
            oos.writeObject(new int[][][][]{schem.getBlocks(), schem.getData()});
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if(oos  != null){
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /**
     * Some methods I used to test
     *
     */
    
    public static void printArray(Schematic a){
        for(int i = 0; i < a.getBlocks().length; i++){
            System.out.println(toString(a.getBlocks()[i]));
        }
    }
    
    public static String toString(int[][] a){
        String s = "";
        for (int row=0; row < a.length ; ++row) {
            s += Arrays.toString(a[row]) + "\n";
        }
        return s;
    }
    
    
    
}