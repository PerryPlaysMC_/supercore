package dev.perryplaysmc.utils.world.blocks;

import dev.perryplaysmc.SuperPluginCore;
import dev.perryplaysmc.utils.Utils;
import dev.perryplaysmc.utils.inventory.NBTTag;
import dev.perryplaysmc.utils.nmsUtils.NMSMain;
import dev.perryplaysmc.utils.nmsUtils.classes.versionUtil.Version;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.lang.reflect.Method;
import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: Utils
 * <p>
 * Path: dev.perryplaysmc.utils.Utils
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class BlockHelper {
    static class LcationHelper {
        public static boolean isSimilar(Location loc1, Location loc2) {
            if(loc1 == null || loc2 == null){
                System.out.println("Locatons null: " + (loc1==null) + " " + (loc2==null));
                return false;
            }
            return loc1.getWorld().getName().equalsIgnoreCase(loc2.getWorld().getName()) &&
                    loc1.getBlockX() == loc2.getBlockX() &&
                    loc1.getBlockY() == loc2.getBlockY()&&
                    loc1.getBlockZ() == loc2.getBlockZ();
        }

        public static void launch(Entity entityToLaunch, Location current, Location nextLocation) {
            Vector vec1 = current.toVector();
            Vector vec2 = nextLocation.toVector();
            Vector pre_velocity = vec2.subtract(vec1);
            Vector velocity = pre_velocity.multiply(0.3);
            entityToLaunch.setVelocity(velocity);
        }

        List<Item> getNearbyItems(Location loc, int rad) {
            List<Item> ret = new ArrayList<>();
            for(Entity e : loc.getWorld().getEntities()) {
                if(e instanceof Item) {
                    if(e.getLocation().distance(loc) <= rad) ret.add((Item) e);
                }
            }
            return ret;
        }

    }

    public static class BreakBlock {

        public static boolean isSilk(ItemStack item) {
            if(!item.hasItemMeta()) {
                return false;
            }
            if((item.getItemMeta().getLore() != null) && (item.getItemMeta().hasEnchant(Enchantment.SILK_TOUCH))) {
                return true;
            }
            return false;
        }

        public static void breakBlockNaturally(ItemStack item, Block toBreak, double xDiff, double yDiff, double zDiff) {
            Material mat = toBreak.getType();
            ItemStack ret = toBreak.getState().getData().toItemStack();
            if(mat == Material.AIR) return;
            try {
                Class cbbc = Class.forName("org.bukkit.craftbukkit." + Version.getCurrentVersionExact().name() + ".CraftBlock");
                Object cbw = cbbc.cast(toBreak);
                Method m = cbbc.getMethod("itemCausesDrops", ItemStack.class);
                m.setAccessible(true);
                if(!((boolean) m.invoke(cbw, item))) {
                    m.setAccessible(false);
                    return;
                }
                m.setAccessible(false);
            } catch (Exception e) {

            }
            if(isSilk(item)) {
                toBreak.setType(Material.AIR);
                if(ret.getType() == Material.AIR) return;
                if(mat.name().endsWith("_ORE")) {
                    toBreak.getWorld().dropItemNaturally(toBreak.getLocation().add(0.5 + xDiff, 0.5 + yDiff, 0.5 + zDiff), new ItemStack(mat, 1));
                    return;
                }
                toBreak.getWorld().dropItemNaturally(toBreak.getLocation().add(0.5 + xDiff, 0.5 + yDiff, 0.5 + zDiff), ret);
                return;
            }
            List<ItemStack> items = new ArrayList<>(toBreak.getDrops(item));
            for(int index = 0; index < items.size(); index++) {
                ItemStack itemStack = items.get(index);
                if(index == 0 && mat.name().endsWith("_ORE")) {
                    int amt = ItemUtil.getNumDrops(ItemUtil.getFortuneLevel(item), 0);
                    itemStack.setAmount(itemStack.getAmount() + amt);
                }
                toBreak.getWorld().dropItemNaturally(toBreak.getLocation().add(0.5 + xDiff, 0.5 + yDiff, 0.5 + zDiff), itemStack);
            }
            int j = 0;
            try {
                Class nmsWorld = Class.forName("net.minecraft.server." + Version.getCurrentVersionExact().name() + ".World");
                Class cbwc = Class.forName("org.bukkit.craftbukkit." + Version.getCurrentVersionExact().name() + ".CraftWorld");
                Object cbw = cbwc.cast(toBreak.getWorld());
                Object nmsW = cbwc.getMethod("getHandle").invoke(cbw);
                Random random = (Random) nmsWorld.getField("random").get(nmsW);
                if(toBreak.getType() == Material.COAL_ORE) {
                    j = nextInt(random, 0, 2);
                } else if(toBreak.getType() == Material.DIAMOND_ORE) {
                    j = nextInt(random, 3, 7);
                } else if(toBreak.getType() == Material.EMERALD_ORE) {
                    j = nextInt(random, 3, 7);
                } else if(toBreak.getType() == Material.LAPIS_ORE) {
                    j = nextInt(random, 2, 5);
                }
            } catch (Exception e) {

            }
            if(j > 0) {
                spawnExpOrb(toBreak.getLocation(), j);
            }
            toBreak.setType(Material.AIR);
        }

        public static void breakBlockNaturally(ItemStack item, Block toBreak) {
            breakBlockNaturally(item, toBreak, 0, 0, 0);
        }

        public static void spawnExpOrb(Location l, int value) {
            ExperienceOrb orb = (ExperienceOrb) l.getWorld().spawnEntity(l.add(0.05, 0, 0.5), EntityType.EXPERIENCE_ORB);
            orb.setExperience(value);
        }

        public static List<Block> generateGroundUp(List<Block> blocks) {
            List<Block> newBlocks = new ArrayList<>();
            int maxX = Integer.MIN_VALUE, minX = Integer.MAX_VALUE;
            int maxY = Integer.MIN_VALUE, minY = Integer.MAX_VALUE;
            int maxZ = Integer.MIN_VALUE, minZ = Integer.MAX_VALUE;
            for(Block b : blocks) {
                Location loc = b.getLocation();
                int x = loc.getBlockX(), y = loc.getBlockY(), z = loc.getBlockZ();
                if(x > maxX) maxX = x;
                if(x < minX) minX = x;

                if(y > maxY) maxY = y;
                if(y < minY) minY = y;

                if(z > maxZ) maxZ = z;
                if(z < minZ) minZ = z;
            }
            for(int y = minY; y < (maxY+1); y++) {
                for(int x = minX; x < (maxX+1); x++) {
                    for(int z = minZ; z < (maxZ+1); z++) {
                        Block b = getBlock(blocks, x, y, z);
                        newBlocks.add(b);
                    }
                }
            }
            return newBlocks;
        }
    }

    public static List<dev.perryplaysmc.utils.world.blocks.Block> convertList(List<Block> blocks) {
        List<dev.perryplaysmc.utils.world.blocks.Block> ret = new ArrayList<>();
        blocks.forEach(block -> ret.add(
                new dev.perryplaysmc.utils.world.blocks.Block(block)
        ));
        return ret;
    }

    public static RegenBlocks regenBlocks(List<Block> blocks, int blockPerInterval, int delay, int interval) {
       return new RegenBlocks(blocks, blockPerInterval, delay, interval);
    }

    private static class RegenBlocks extends BukkitRunnable {
        List<BlockState> states;
        int blockPerInterval;
        public RegenBlocks(List<Block> blocks1, int blockPerInterval, int delay, int interval) {
            states = new ArrayList<>();
            this.blockPerInterval = blockPerInterval;
            List<Block> blocks = Utils.BlockHelper.generateGroundUp(blocks1);
            blocks.stream().filter(b->b!=null).forEach(b -> states.add(b.getState()));
            states = Utils.reverseList(states);
            runTaskTimer(SuperPluginCore.getInstance(), delay, interval);
        }

        @Override
        public void run() {
            if(states.isEmpty()) {
                cancel();
                return;
            }
            for(int i = 0; i < blockPerInterval; i++) {
                int index = states.size()-1;
                states.get(index).update(true, false);
                states.remove(index);
                if(states.isEmpty()) {
                    cancel();
                    return;
                }
            }
        }
    }

    static Block getBlock(List<Block> blocks, int x, int y, int z) {
        for(Block b : blocks) {
            if(b.getX() == x && b.getY() == y && b.getZ() == z) return b;
        }
        return null;
    }

    public Set<Block> getNearbyBlocks(Block block, int rad) {
        Set<Block> b = new HashSet<>();
        for(int x = -rad; x < rad; x++) {
            for(int y = -rad; y < rad; y++) {
                for(int z = -rad; z < rad; z++) {
                    Location loc = block.getRelative(x,y,z).getLocation();
                    b.add(loc.getBlock());
                }
            }
        }
        return b;
    }

    public Set<Block> getNearbyBlocks(Location block, int rad) {
        Set<Block> b = new HashSet<>();
        for(int x = -rad; x < rad; x++) {
            for(int y = -rad; y < rad; y++) {
                for(int z = -rad; z < rad; z++) {
                    Location loc = block.getBlock().getRelative(x,y,z).getLocation();
                    b.add(loc.getBlock());
                }
            }
        }
        return b;
    }


    public static void generateSphere(Location location, HashMap<Integer, MaterialData> materials, int radius, boolean hollow) {
        List<Location> blocks = new ArrayList<>();
        int bx = location.getBlockX();
        int by = location.getBlockY();
        int bz = location.getBlockZ();

        for(int y = by - radius; y <= by + radius; y++) {
            for(int x = bx - radius; x <= bx + radius; x++) {
                for(int z = bz - radius; z <= bz + radius; z++) {
                    double distance = ((bx-x) * (bx-x) + ((bz-z) * (bz-z)) + ((by-y) * (by-y)));
                    if(distance < radius * radius && !(hollow && distance < ((radius - 1) * (radius - 1)))) {
                        Location l = new Location(location.getWorld(), x, y, z);
                        blocks.add(l);
                    }
                }
            }
        }
        HashMap<Integer, List<Location>> locs = new HashMap<>();
        List<Location> newBlocks = new ArrayList<>();
        int i = 0;
        int l = new Random().nextInt(blocks.size());
        while(newBlocks.size() < blocks.size()) {
            while(l<0)
                l = new Random().nextInt(blocks.size());
            Location loc = blocks.get(l);
            while(newBlocks.contains(loc)) {
                l = new Random().nextInt(blocks.size());
                loc = blocks.get(l);
            }
            double percent = NumberUtil.getPercent(i, blocks.size());
            Random r = new Random();
            int index = r.nextInt(materials.keySet().size());
            List<Location> loca = new ArrayList<>();
            if(locs.containsKey(index)) {
                loca = locs.get(index);
            }
            loca.add(loc);
            double perc = NumberUtil.getPercent(loca.size(), blocks.size());
            if(perc > new ArrayList<>(materials.keySet()).get(index)) {
                l--;
                continue;
            }
            locs.put(index, loca);
            MaterialData data = materials.get(new ArrayList<>(materials.keySet()).get(index));
            Block block = loc.getBlock();
            BlockState state = block.getState();
            state.setData(data);
            state.update(true);
            newBlocks.add(loc);
            l = new Random().nextInt(blocks.size());
            i++;
        }
    }



    public Set<Block> getConnectedBlocks(int max, Block start, List<Material> allowedMaterials) {
        return getConnectedBlocks(max, start, allowedMaterials, new HashSet<>());
    }

    private Set<Block> getConnectedBlocks(int max, Block start, List<Material> allowedMaterials, Set<Block> blocks) {
        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {
                for (int z = -1; z < 2; z++) {
                    if ((blocks.size() >= max) && (max != -1)) {
                        return blocks;
                    }
                    Block block = start.getLocation().clone().add(x, y, z).getBlock();
                    if ((!blocks.contains(block)) && (allowedMaterials.size() == 0 ? true : allowedMaterials.contains(block.getType())) && (block.getType() == start.getType())) {
                        blocks.add(block);
                        if ((blocks.size() >= max) && (max != -1)) {
                            return blocks;
                        }
                        try {
                            String a = getConnectedBlocks(max, block, allowedMaterials, blocks).size() + "/" + blocks.size();
                            blocks.addAll(getConnectedBlocks(max, block, allowedMaterials, blocks));
                        }
                        catch (Exception localException) {}
                    }
                }
            }
        }
        return blocks;
    }
    List<Block> ignore = new ArrayList<>();

    public List<Block> getIgnore() {
        return ignore;
    }



    static int nextInt(Random var0, int var1, int var2) {
        return var1 >= var2 ? var1 : var0.nextInt(var2 - var1 + 1) + var1;
    }

    private static class ItemUtil {

        public static int getFortuneLevel(ItemStack item) {
            if((item.getItemMeta().hasEnchant(Enchantment.LOOT_BONUS_BLOCKS))) {
                return item.getItemMeta().getEnchantLevel(Enchantment.LOOT_BONUS_BLOCKS);
            }
            return 0;
        }

        public static int getNumDrops(int i, int def) {
            if(i <= 0) {
                return 1;
            }
            int j = 2;
            int k = 35 - i * 5;
            if(i > 100) {
                j = (int) (j + Math.round(i - 40.0D));
            }
            if(i > 50) {
                k = 1;
            } else if(i > 25) {
                k = 2;
            } else if(i > 15) {
                k = 3;
            } else if(i > 8) {
                k = 4;
            }
            if(k < 5) {
                i = 100;
            }
            int l = k;
            int i1 = new Random().nextInt(100) + 1;
            while(l <= k * (i + 1)) {
                if(i1 <= l) {
                    return j;
                }
                l += k;
                j++;
            }
            return def;
        }

        public static int getNumDrops(int i) {
            return getNumDrops(i, 1);
        }

        public static boolean a(ItemStack i, int l, Random r) {
            if(((i.getType().name().contains("CHEST")
                    || i.getType().name().contains("LEGGINS")
                    || i.getType().name().contains("HELMET")
                    || i.getType().name().contains("BOOTS"))) && (r.nextFloat() < 0.6F)) {
                return false;
            }
            return r.nextInt(l + 1) > 0;
        }

        public static boolean e(ItemStack item) {
            if(item.getType().getMaxDurability() <= 0) {
                return false;
            } else{
                try {
                    HashMap<String, NBTTag> data = NMSMain.getItemDataUtilHelper().getData(item);
                    return data.size() < 1 || (data.containsKey("Unbreakable") && !Boolean.valueOf(data.get("Unbreakable").getValue().toString()));
                } catch (Exception e) {
                    return false;
                }
            }
        }

        public static ItemStack damage(ItemStack item, int i, Random random) {

            ItemMeta im = item.getItemMeta();
            if(im == null) im = Bukkit.getItemFactory().getItemMeta(item.getType());
            if(!e(item)) {
                return item;
            }
            if(i > 0) {
                int j = im.getEnchantLevel(Enchantment.DURABILITY);
                int k = 0;
                for(int l = 0; (j > 0) && (l < i); l++) {
                    if(a(item, j, random)) {
                        k++;
                    }
                }
                i -= k;
                if(i <= 0) {
                    return item;
                }
            }
            item.setDurability((short) (item.getDurability() + i));
            return item;
        }

    }
}
