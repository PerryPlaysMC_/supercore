package dev.perryplaysmc.utils.world.blocks;

import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.material.MaterialData;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.world
 * Class: Block
 * <p>
 * Path: dev.perryplaysmc.utils.world.blocks.Block
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Block {

    @Getter
    private org.bukkit.block.Block block;


    public Block(org.bukkit.block.Block block) {
        this.block = block;
    }

    public BlockState getState() {
        return block.getState();
    }

    public Block setType(Material material) {
        if(material == null) material = Material.AIR;
        if(!material.isBlock()) {
            System.out.println("\n\n\n\nError: " + material.name() + " is not a block\n\n\n\n");
            return this;
        }
        block.setType(material);
        getState().update(true);
        return this;
    }

    public Block getRelative(BlockFace face) {
        return new Block(block.getRelative(face));
    }

    public Block getRelative(BlockFace face, int dist) {
        return new Block(block.getRelative(face, dist));
    }

    public Location getLocation() {
        return block.getLocation();
    }

    public Material getType() {
        return block.getType();
    }

    public Block setData(int data) {
        getState().setRawData((byte)data);
        getState().update(true);
        return this;
    }

    public Block setData(MaterialData data) {
        if(data.getItemType() != getType()) {
            System.out.println("\n\n\n\nError: MaterialData Type " + data.getItemType().name() +
                    " is not the same as " + getType().name() + "\n\n\n\n");
            return this;
        }
        getState().setData(data);
        getState().update(true);
        return this;
    }

    public int getX() {
        return getLocation().getBlockX();
    }

    public int getY() {
        return getLocation().getBlockY();
    }

    public int getZ() {
        return getLocation().getBlockZ();
    }

}
