package dev.perryplaysmc.utils;

import com.sun.istack.internal.NotNull;
import dev.perryplaysmc.core.Log;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.plugin.*;
import dev.perryplaysmc.utils.PluginClassLoader;
import org.bukkit.plugin.java.JavaPluginLoader;
import org.yaml.snakeyaml.error.YAMLException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: ClassUtils
 * <p>
 * Path: dev.perryplaysmc.utils.ClassUtils
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/
@SuppressWarnings("all")
public class ClassUtils {
    
    public static Set<Class<?>> getClasses(File jarFile, String packageName) {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        List<String> names = new ArrayList<>();
        try {
            JarFile file = new JarFile(jarFile);
            for (Enumeration<JarEntry> entry = file.entries(); entry.hasMoreElements();) {
                JarEntry jarEntry = entry.nextElement();
                String name = jarEntry.getName().replace("/", ".");
                if(name.startsWith(packageName)
                   && !name.contains("$")
                   && name.endsWith(".class") && !names.contains(name.split(packageName + ".")[1].replace(".class", ""))) {
                    names.add(name.split(packageName + ".")[1].replace(".class", ""));
                }
            }
            Collections.sort(names);
            for(String name : names) {
                classes.add(Class.forName(packageName + "." + name));
            }
            file.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return classes;
    }
    public static <T> Set<Class<T>> getAllClassesOf(File jarFile, Class<T> t, String packageName) {
        Set<Class<T>> classes = new HashSet<Class<T>>();
        List<String> names = new ArrayList<>();
        try {
            JarFile file = new JarFile(jarFile);
            for (Enumeration<JarEntry> entry = file.entries(); entry.hasMoreElements();) {
                JarEntry jarEntry = entry.nextElement();
                String name = jarEntry.getName().replace("/", ".");
                if(name.startsWith(packageName)
                   && !name.contains("$")
                   && name.endsWith(".class") &&
                   (packageName.length() > 0 ? !names.contains(name.split(packageName + ".")[1].replace(".class", "")) : true)) {
                    String newName = (packageName.length() > 0 ?
                            name.split(packageName + ".")[1].replace(".class","")
                            : name.replace(".class",""));
                    if(newName.startsWith("dev.perryplaysmc.utils.nmsUtils")
                       || newName.startsWith("net.minecraft.server")) continue;
                    try {
                        Class s = Class.forName((packageName.length() > 0 ? packageName + "." : "") + newName);
                        List<String> nameClasses = new ArrayList<>();
                        for (Class<?> c : s.getInterfaces()) {
                            if(c != null)
                                nameClasses.add(c.getSimpleName());
                        }
                        if(s.getSuperclass() != null)
                            nameClasses.add(s.getSuperclass().getSimpleName());
                        if(s != null && nameClasses.contains(t.getSimpleName())) {
                            names.add((packageName.length() > 0 ? name.split(packageName + ".")[1] : name).replace(".class", ""));
                        }
                    }catch(Exception e) {
                        continue;
                    }
                }
            }
            Collections.sort(names);
            for(String name : names) {
                try {
                    Class<T> t1 = (Class<T>) Class.forName((packageName.length() > 0 ?packageName + "." : "") + name);
                    if(!classes.contains(t1))
                        classes.add(t1);
                }catch (Exception e) {
                    System.out.println("Error " + ((packageName.length() > 0 ? packageName + "." : "") + name)
                                       + "\nIs not an instanceof " + t.getSimpleName());
                }
            }
            file.close();
        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("Error");
        }
        return classes;
    }
    
    @NotNull
    public static Plugin loadPlugin(@NotNull File file) throws InvalidPluginException {
        JavaPluginLoader l = new JavaPluginLoader(Bukkit.getServer());
        Validate.notNull(file, "File cannot be null");
        if (!file.exists()) {
            throw new InvalidPluginException(new FileNotFoundException(file.getPath() + " does not exist"));
        } else {
            PluginDescriptionFile description;
            try {
                description = getPluginDescription(file);
            } catch (InvalidDescriptionException var11) {
                throw new InvalidPluginException(var11);
            }
            
            File parentFile = file.getParentFile();
            File dataFolder = new File(parentFile, description.getName());
            File oldDataFolder = new File(parentFile, description.getRawName());
            if (!dataFolder.equals(oldDataFolder)) {
                if (dataFolder.isDirectory() && oldDataFolder.isDirectory()) {
                    Log.warning(String.format("While loading %s (%s) found old-data folder: `%s' next to the new one `%s'", description.getFullName(), file, oldDataFolder, dataFolder));
                } else if (oldDataFolder.isDirectory() && !dataFolder.exists()) {
                    if (!oldDataFolder.renameTo(dataFolder)) {
                        throw new InvalidPluginException("Unable to rename old data folder: `" + oldDataFolder + "' to: `" + dataFolder + "'");
                    }
                    
                    Log.info(String.format("While loading %s (%s) renamed data folder: `%s' to `%s'", description.getFullName(), file, oldDataFolder, dataFolder));
                }
            }
            
            if (dataFolder.exists() && !dataFolder.isDirectory()) {
                throw new InvalidPluginException(String.format("Projected datafolder: `%s' for %s (%s) exists and is not a directory", dataFolder, description.getFullName(), file));
            } else {
                Iterator var7 = description.getDepend().iterator();
                
                while(var7.hasNext()) {
                    String pluginName = (String)var7.next();
                    Plugin current = Bukkit.getPluginManager().getPlugin(pluginName);
                    if (current == null) {
                        throw new UnknownDependencyException(pluginName);
                    }
                }
                
                Bukkit.getServer().getUnsafe().checkSupported(description);
                PluginClassLoader loader;
                try {
                    loader = new PluginClassLoader(l, inst.getClass().getClassLoader(), description, dataFolder, file);
                } catch (InvalidPluginException var9) {
                    throw var9;
                } catch (Throwable var10) {
                    throw new InvalidPluginException(var10);
                }
                Bukkit.getPluginManager().enablePlugin(loader.plugin);
                return loader.plugin;
            }
        }
    }
    static ClassUtils inst;
    static {
        inst = new ClassUtils();
    }
    
    @NotNull
    public static PluginDescriptionFile getPluginDescription(@NotNull File file) throws InvalidDescriptionException {
        Validate.notNull(file, "File cannot be null");
        JarFile jar = null;
        InputStream stream = null;
        
        PluginDescriptionFile var6;
        try {
            jar = new JarFile(file);
            JarEntry entry = jar.getJarEntry("plugin.yml");
            if (entry == null) {
                entry = jar.getJarEntry("core-plugin.yml");
                if (entry == null)
                    throw new InvalidDescriptionException(new FileNotFoundException("Jar does not contain plugin.yml or core-plugin.yml"));
            }
            
            stream = jar.getInputStream(entry);
            var6 = new PluginDescriptionFile(stream);
        } catch (IOException var17) {
            throw new InvalidDescriptionException(var17);
        } catch (YAMLException var18) {
            throw new InvalidDescriptionException(var18);
        } finally {
            if (jar != null) {
                try {
                    jar.close();
                } catch (IOException var16) {
                    ;
                }
            }
            
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException var15) {
                    ;
                }
            }
            
        }
        
        return var6;
    }
    
}
