package dev.perryplaysmc.utils.text;

import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.nmsUtils.NMSMain;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import net.md_5.bungee.api.chat.*;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.function.Consumer;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/20/19-2023
 * Package: me.perryplaysmc.base.chat
 * Path: me.perryplaysmc.base.chat.FancyMessage
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class FancyMessage {
    private List<TextComponent> componentList;
    private List<TextComponent> lastThenSet;
    private UUID setId;
    private Map<UUID, Consumer<Player>> commandsMap = new HashMap<>();
    private net.md_5.bungee.api.ChatColor currentColor = net.md_5.bungee.api.ChatColor.WHITE;
    
    public FancyMessage() {
        this("");
    }
    
    public FancyMessage(String text) {
        componentList = new ArrayList<>();
        then(text);
    }
    
    public FancyMessage then(String text) {
        return then(text, null);
    }
    
    public FancyMessage then(String text, User u) {
        return thenRaw(u==null? StringUtils.translate(text):StringUtils.translate(text, "", u));
    }
    
    public FancyMessage thenRaw(String text) {
        lastThenSet = new ArrayList<>();
        if (text.contains("§") || text.contains("http")) {
            if (text.contains("http")) {
                int index = text.indexOf("http");
                while (true) {
                    int nextIndex = text.indexOf(" ", index + 1);
                    then(text.substring(0, index));
                    
                    String link = text.substring(index, nextIndex == -1 ? text.length() : nextIndex);
                    TextComponent linkPart = new TextComponent(link);
                    linkPart.setColor(currentColor);
                    linkPart.setItalic(true);
                    linkPart.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, link));
                    componentList.add(linkPart);
                    lastThenSet.add(linkPart);
                    
                    text = text.substring(nextIndex == -1 ? text.length() : nextIndex);
                    index = text.indexOf("http");
                    if (index == -1) {
                        if (!text.equals("")) {
                            then(text);
                        }
                        return this;
                    }
                }
            }
            
            if (text.contains("§")) {
                int index = text.indexOf("§");
                
                //Index of first color may be much later, so we need to add everything up until that
                //next color with whatever the last set color was.
                TextComponent beforeFirstColor = new TextComponent(text.substring(0, index));
                if (currentColor != null)
                    beforeFirstColor.setColor(currentColor);
                lastThenSet.add(beforeFirstColor);
                
                //Then we can move on to doing more color parsing
                TextComponent next = new TextComponent("");
                
                do {
                    int nextIndex = text.indexOf("§", index + 1);
                    
                    ChatColor color = ChatColor.getByChar(text.substring(index + 1, index + 2));
                    String nextPart = text.substring(index + 2, nextIndex == -1 ? text.length() : nextIndex);
                    if (!nextPart.equalsIgnoreCase(""))
                        next.setText(nextPart);
                    
                    net.md_5.bungee.api.ChatColor colorChar = net.md_5.bungee.api.ChatColor.getByChar(color.getChar());
                    switch (colorChar) {
                        case BOLD:
                            next.setBold(true);
                            break;
                        case STRIKETHROUGH:
                            next.setStrikethrough(true);
                            break;
                        case UNDERLINE:
                            next.setUnderlined(true);
                            break;
                        case ITALIC:
                            next.setItalic(true);
                            break;
                        case RESET:
                            break;
                        default:
                            next.setColor(colorChar);
                            break;
                    }
                    if (!nextPart.equalsIgnoreCase("")) {
                        lastThenSet.add(next);
                        next = new TextComponent("");
                    }
                    text = text.substring(index + 2);
                    index = text.indexOf("§");
                } while (index != -1);
            }
            
            componentList.addAll(lastThenSet);
        } else {
            TextComponent textComponent = new TextComponent(text);
            if (currentColor != null) {
                textComponent.setColor(currentColor);
            }
            componentList.add(textComponent);
            lastThenSet.add(textComponent);
        }
        return this;
    }
    
    public FancyMessage color(ChatColor color) {
        net.md_5.bungee.api.ChatColor bColor = net.md_5.bungee.api.ChatColor.getByChar(color.getChar());
        latest().setColor(bColor);
        currentColor = bColor;
        return this;
    }
    
    public FancyMessage style(ChatColor style) {
        switch (style) {
            case BOLD:
                latest().setBold(true);
                break;
            case STRIKETHROUGH:
                latest().setStrikethrough(true);
                break;
            case UNDERLINE:
                latest().setUnderlined(true);
                break;
            case ITALIC:
                latest().setItalic(true);
                break;
        }
        return this;
    }
    
    public FancyMessage itemTooltip(ItemStack itemStack) {
        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_ITEM,
                new BaseComponent[]{new TextComponent(convertItemStackToJson(itemStack))});
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setHoverEvent(hoverEvent);
        }
//        latest().setHoverEvent(hoverEvent);
        return this;
    }
    
    /**
     * Converts an {@link ItemStack} to a Json string
     * for sending with {@link BaseComponent}'s.
     *
     * @param itemStack the item to convert
     * @return the Json string representation of the item
     */
    private String convertItemStackToJson(ItemStack itemStack) {
        return NMSMain.getItemStackJSONConverter().convertItemStack(itemStack);
    }
    
    public FancyMessage tooltip(List<String> tooltip) {
        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder(String.join("\n", tooltip)).create());
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setHoverEvent(hoverEvent);
        }
        return this;
    }
    
    public FancyMessage hover(String... tooltip) {
        HoverEvent hoverEvent = new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder(String.join("\n", tooltip)).create());
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setHoverEvent(hoverEvent);
        }
        return this;
    }
    
    public FancyMessage link(String url) {
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url));
        }
        return this;
    }
    
    public FancyMessage suggest(String command) {
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command));
        }
        return this;
    }
    
    public FancyMessage command(String command) {
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
        }
        return this;
    }
    
    public FancyMessage command(Consumer<Player> command) {
        if (setId == null) {
            setId = UUID.randomUUID();
        }
        
        UUID commandId = UUID.randomUUID();
        commandsMap.put(commandId, command);
        
        for (TextComponent component : componentList) {
            if (lastThenSet.contains(component))
                component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/confirm " + setId + " " + commandId));
        }
        return this;
    }
    
    private TextComponent latest() {
        if (componentList.isEmpty()) {
            return new TextComponent("");
        }
        return componentList.get(componentList.size() - 1);
    }
    
    public String toOldMessageFormat() {
        return build().toLegacyText();
    }
    
    public void send(Player p) {
        if (p == null) return;
        
        p.spigot().sendMessage(build());
    }
    
    public void send(CommandSender sender) {
        sender.spigot().sendMessage(build());
    }
    
    private TextComponent build() {
        TextComponent finalComponent = new TextComponent();
        for (TextComponent component : componentList) {
            finalComponent.addExtra(component);
        }
        return finalComponent;
    }
}
