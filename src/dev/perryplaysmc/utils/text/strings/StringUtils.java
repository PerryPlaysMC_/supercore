package dev.perryplaysmc.utils.text.strings;

import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.CommandSource;
import org.bukkit.ChatColor;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: StringUtils
 * <p>
 * Path: dev.perryplaysmc.utils.text.strings.StringUtils
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class StringUtils {

    private static String[] fancyAlphabetLowerCase = new String[] {
            "Ⓐ", "ⓐ",
            "Ⓑ", "ⓑ",
            "Ⓒ", "ⓒ",
            "Ⓓ", "ⓓ",
            "Ⓔ", "ⓔ",
            "Ⓕ", "ⓕ",
            "Ⓖ", "ⓖ",
            "Ⓗ", "ⓗ",
            "Ⓘ", "ⓘ",
            "Ⓙ", "ⓙ",
            "Ⓚ", "ⓚ",
            "Ⓛ", "ⓛ",
            "Ⓜ", "ⓜ",
            "Ⓝ", "ⓝ",
            "Ⓞ", "ⓞ",
            "Ⓟ", "ⓟ",
            "Ⓠ", "ⓠ",
            "Ⓡ", "ⓡ",
            "Ⓢ", "ⓢ",
            "Ⓣ", "ⓣ",
            "Ⓤ", "ⓤ",
            "Ⓥ", "ⓥ",
            "Ⓦ", "ⓦ",
            "Ⓧ", "ⓧ",
            "Ⓨ", "ⓨ",
            "Ⓩ", "ⓩ"

    };

    static String[] newFancyText = new String[]{
            "⓪",
            "①",
            "②",
            "③",
            "④",
            "⑤",
            "⑥",
            "⑦",
            "⑧",
            "⑨"
    };

    public static String toFancy(String text) {
        String ret = "";
        for(char c : text.toCharArray()) {
            A:switch (c) {
                case 'A':
                    ret += fancyAlphabetLowerCase[0];
                    break A;
                case 'a':
                    ret += fancyAlphabetLowerCase[1];
                    break A;
                case 'B':
                    ret += fancyAlphabetLowerCase[2];
                    break A;
                case 'b':
                    ret += fancyAlphabetLowerCase[3];
                    break A;
                case 'C':
                    ret += fancyAlphabetLowerCase[4];
                    break A;
                case 'c':
                    ret += fancyAlphabetLowerCase[5];
                    break A;
                case 'D':
                    ret += fancyAlphabetLowerCase[6];
                    break A;
                case 'd':
                    ret += fancyAlphabetLowerCase[7];
                    break A;
                case 'E':
                    ret += fancyAlphabetLowerCase[8];
                    break A;
                case 'e':
                    ret += fancyAlphabetLowerCase[9];
                    break A;
                case 'F':
                    ret += fancyAlphabetLowerCase[10];
                    break A;
                case 'f':
                    ret += fancyAlphabetLowerCase[11];
                    break A;
                case 'G':
                    ret += fancyAlphabetLowerCase[12];
                    break A;
                case 'g':
                    ret += fancyAlphabetLowerCase[13];
                    break A;
                case 'H':
                    ret += fancyAlphabetLowerCase[14];
                    break A;
                case 'h':
                    ret += fancyAlphabetLowerCase[15];
                    break A;
                case 'I':
                    ret += fancyAlphabetLowerCase[16];
                    break A;
                case 'i':
                    ret += fancyAlphabetLowerCase[17];
                    break A;
                case 'J':
                    ret += fancyAlphabetLowerCase[18];
                    break A;
                case 'j':
                    ret += fancyAlphabetLowerCase[19];
                    break A;
                case 'K':
                    ret += fancyAlphabetLowerCase[20];
                    break A;
                case 'k':
                    ret += fancyAlphabetLowerCase[21];
                    break A;
                case 'L':
                    ret += fancyAlphabetLowerCase[22];
                    break A;
                case 'l':
                    ret += fancyAlphabetLowerCase[23];
                    break A;
                case 'M':
                    ret += fancyAlphabetLowerCase[24];
                    break A;
                case 'm':
                    ret += fancyAlphabetLowerCase[25];
                    break A;
                case 'N':
                    ret += fancyAlphabetLowerCase[26];
                    break A;
                case 'n':
                    ret += fancyAlphabetLowerCase[27];
                    break A;
                case 'O':
                    ret += fancyAlphabetLowerCase[28];
                    break A;
                case 'o':
                    ret += fancyAlphabetLowerCase[29];
                    break A;
                case 'P':
                    ret += fancyAlphabetLowerCase[30];
                    break A;
                case 'p':
                    ret += fancyAlphabetLowerCase[31];
                    break A;
                case 'Q':
                    ret += fancyAlphabetLowerCase[32];
                    break A;
                case 'q':
                    ret += fancyAlphabetLowerCase[33];
                    break A;
                case 'R':
                    ret += fancyAlphabetLowerCase[34];
                    break A;
                case 'r':
                    ret += fancyAlphabetLowerCase[35];
                    break A;
                case 'S':
                    ret += fancyAlphabetLowerCase[36];
                    break A;
                case 's':
                    ret += fancyAlphabetLowerCase[37];
                    break A;
                case 'T':
                    ret += fancyAlphabetLowerCase[38];
                    break A;
                case 't':
                    ret += fancyAlphabetLowerCase[39];
                    break A;
                case 'U':
                    ret += fancyAlphabetLowerCase[40];
                    break A;
                case 'u':
                    ret += fancyAlphabetLowerCase[41];
                    break A;
                case 'V':
                    ret += fancyAlphabetLowerCase[42];
                    break A;
                case 'v':
                    ret += fancyAlphabetLowerCase[43];
                    break A;
                case 'W':
                    ret += fancyAlphabetLowerCase[44];
                    break A;
                case 'w':
                    ret += fancyAlphabetLowerCase[45];
                    break A;
                case 'X':
                    ret += fancyAlphabetLowerCase[46];
                    break A;
                case 'x':
                    ret += fancyAlphabetLowerCase[47];
                    break A;
                case 'Y':
                    ret += fancyAlphabetLowerCase[48];
                    break A;
                case 'y':
                    ret += fancyAlphabetLowerCase[49];
                    break A;
                case 'Z':
                    ret += fancyAlphabetLowerCase[50];
                    break A;
                case 'z':
                    ret += fancyAlphabetLowerCase[51];
                    break A;
                case '0':
                    ret += newFancyText[0];
                    break A;
                case '1':
                    ret += newFancyText[1];
                    break A;
                case '2':
                    ret += newFancyText[2];
                    break A;
                case '3':
                    ret += newFancyText[3];
                    break A;
                case '4':
                    ret += newFancyText[4];
                    break A;
                case '5':
                    ret += newFancyText[5];
                    break A;
                case '6':
                    ret += newFancyText[6];
                    break A;
                case '7':
                    ret += newFancyText[7];
                    break A;
                case '8':
                    ret += newFancyText[8];
                    break A;
                case '9':
                    ret += newFancyText[9];
                    break A;
                case '+':
                    ret += '⊕';
                    break A;
                case '-':
                    ret += '⊖';
                    break A;
                case '*':
                    ret += '⊗';
                    break A;
                case '/':
                    ret += '⊘';
                    break A;
                case '<':
                    ret += '⧀';
                    break A;
                case '>':
                    ret += '⧁';
                    break A;
                case '=':
                    ret += '⊜';
                    break A;

                default:
                    ret+=c;
                    break A;
            }
        }
        return ret;
    }



    private static Config getSettings() {
        return SuperAPI.getSettings();
    }

    public static String[] translate(String[] text) {
        for(int i = 0; i < text.length; i++) {
            text[i] = translate(text[i]);
        }
        return text;
    }

    public static String translate(String toTrans) {
        String message = toTrans;
        String msg = message;
        String prefix = "", errorPrefix = "", anticheatPrefix = "",
                errorPrefixFile = getSettings().getString("Data.Chat.Prefixes.Error"),
                prefixFile = getSettings().getString("Data.Chat.Prefixes.Main"),
                anticheatPrefixFile = getSettings().getString("Data.Chat.Prefixes.AntiCheat");
        if(toTrans != null && getSettings() != null) {
            if(getSettings().getString("Data.Chat.Prefixes.Main") != ""
                    && getSettings().getString("Data.Chat.Prefixes.Main") != null)
                prefix = (!toTrans.contains("[p] ") ?
                        (prefixFile.endsWith(" ") ? prefixFile.substring(0, prefixFile.length()+-1) : prefixFile) + " " :
                        prefixFile);

            if(getSettings().getString("Data.Chat.Prefixes.Error") != ""
                    && getSettings().getString("Data.Chat.Prefixes.Error") != null)
                errorPrefix = (!toTrans.contains("[e] ") ?
                        (errorPrefixFile.endsWith(" ") ? errorPrefixFile.substring(0, errorPrefixFile.length()+-1) : errorPrefixFile) + " " :
                        errorPrefixFile);
            if(getSettings().getString("Data.Chat.Prefixes.AntiCheat") != ""
                    && getSettings().getString("Data.Chat.Prefixes.AntiCheat") != null)
                anticheatPrefix = (!toTrans.contains("[ac] ") ?
                        (anticheatPrefixFile.endsWith(" ") ?
                                anticheatPrefixFile.substring(0, anticheatPrefixFile.length()+-1) : anticheatPrefixFile) + " " :
                        anticheatPrefixFile);
            msg = message
                    .replace("[p]".toLowerCase() , prefix)
                    .replace("[e]".toLowerCase() , errorPrefix)
                    .replace("[ac]".toLowerCase(), anticheatPrefix)
                    .replace("[pc]".toLowerCase(), getSettings().getString("Data.Chat.Prefixes.Data.Important"))
                    .replace("[c]".toLowerCase() , getSettings().getString("Data.Chat.Prefixes.Data.Default"))
                    .replace("[a]".toLowerCase() , getSettings().getString("Data.Chat.Prefixes.Data.Argument"))
                    .replace("[r]".toLowerCase() , getSettings().getString("Data.Chat.Prefixes.Data.Required-Argument"))
                    .replace("[o]".toLowerCase( ), getSettings().getString("Data.Chat.Prefixes.Data.Optional-Argument"))
                    .replace("[n]", "\n")
                    .replace("\\n", "\n");
        }

        return ColorUtil.translateColors('&', msg);
    }

    public static String translate(String text, String original, CommandSource user) {
        if(!user.isPlayer())
            return ColorUtil.translateChatColor(text);
        for(ChatColor c : ChatColor.values())
            if(user.hasPermission("chatcolors." + c.getChar(), "chatcolors.all"))
                if(c.getChar() == 'r')
                    text = text.replace("&r", "§" + original.replace("§", ""));
                else
                    text = text.replace("&" + c.getChar(), "§" + c.getChar());
        return text;
    }


    public static boolean hasChars(String str) {
        for(char c : str.toCharArray()) {
            if(c!=' ' && c!='\n' && !Character.getName(c).equalsIgnoreCase("LINE FEED (LF)"))
                return true;
        }
        return str == null || str.isEmpty() || str.length() == 0;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty() || str.length() == 0 || !hasChars(str);
    }

    public static boolean isEmptyArray(String[] str) {
        boolean x = true;
        for(String f : str) {
            if(!isEmpty(f)) {
                x = false;
            }
        }
        return str == null || x || str.length == 0;
    }

    public static String[] addToArray(String[] array, String toAdd) {
        List<String> list = Arrays.asList(array) == null ? new ArrayList<>() : Arrays.asList(array);
        list.add(toAdd);
        return convertToArray(list);
    }

    public static String[] convertToArray(List<String> list) {
        return list.toArray(new String[list.size()]);
    }

    public static boolean checkForDomain(String str) {
        String domainPattern = "[a-zA-Z0-9](([a-zA-Z0-9\\-]{0,61}[A-Za-z0-9])?\\.)+(com|net|org|co|gg|io|pro|host|tk)";
        Pattern r = Pattern.compile(domainPattern);
        Matcher m = r.matcher(str);
        Pattern invPat = Pattern.compile("(?:https?://)?discord(?:app\\.com/invite|\\.gg)/([a-z0-9-]+)", 2);
        Matcher m2 = invPat.matcher(str);
        return m.find() || m2.find();
    }

    public static String getNameFromEnum(Enum e) {
        String f = e.name();
        String a = f.charAt(0) + f.substring(1).toLowerCase().split("_")[0];
        if(f.contains("_")) {
            for(int z = 1; z < f.split("_").length; z++) {
                String p = f.split("_")[z];
                a+=" " + (p.charAt(0)+"").toUpperCase() + p.substring(1).toLowerCase();
            }
        }
        return a;
    }

    public static String getNameFromCaps(String e) {
        String f = e.replace(" ", "_");
        String a = f.charAt(0) + f.substring(1).toLowerCase().split("_")[0];
        if(f.contains("_")) {
            for(int z = 1; z < f.split("_").length; z++) {
                String p = f.split("_")[z];
                a+=" " + (p.charAt(0)+"").toUpperCase() + p.substring(1).toLowerCase();
            }
        }
        return a;
    }

    public static String findValue(Object obj) {
        if(obj instanceof String) return obj.toString();
        Class<?> o = obj.getClass();
        Method m = null;
        try {
            m = o.getMethod("getRealName");
        } catch (NoSuchMethodException e) {
            try {
                m = o.getMethod("getName");
            } catch (NoSuchMethodException e1) {
                try {
                    m = o.getMethod("name");
                } catch (NoSuchMethodException e2) {
                    try {
                        m = o.getMethod("getContents");
                    } catch (NoSuchMethodException e3) {
                        try {
                            m = o.getMethod("getInput");
                        } catch (NoSuchMethodException e4) {
                            try {
                                m = o.getMethod("toString");
                            } catch (NoSuchMethodException e5) {
                            }
                        }
                    }
                }
            }
        }
        try {
            return getNameFromCaps(String.valueOf(m.invoke(obj)));
        } catch (Exception e) {
            return obj.toString();
        }
    }

    public static Object tryCast(Class<?> attemptCast, Object attemptingCast) {
        Class<?> o = attemptCast;
        Method m = null;
        try {
            m = o.getMethod("valueOf", attemptingCast.getClass());
            return m.invoke(attemptCast, attemptingCast);
        } catch (Exception e) {
        }
        try {
            return o.cast(attemptingCast);
        }catch (Exception e) {
            return attemptingCast.toString();
        }
    }

    public static String toString(Object[] array) {
        String ret = "";
        for(Object r : array) {
            ret += findValue(r) + ", ";
        }
        return "[" + ret.trim().substring(0, ret.lastIndexOf(',') > -1 ? ret.lastIndexOf(',') : 0) + "]";
    }

    public static String join(String[] args, String joiner, int start) {
        String ret = "";
        for(int i = start; i < args.length; i++) {
            ret+=args[i]+(i < (args.length+-1) ? joiner : "");
        }
        return ret;
    }

    public static String join(Object[] args, String joiner, int start) {
        String ret = "";
        for(int i = start; i < args.length; i++) {
            ret+=findValue(args[i])+(i < (args.length+-1) ? joiner : "");
        }
        return ret;
    }


    public static String repeat(String toRepeat, int amount) {
        String ret = "";
        for(int i = 0; i < amount; i++) {
            ret+=toRepeat;
        }
        return ret;
    }

    public static String centerText(int maxWidth, String text) {
        int spaces = (int) Math.round((maxWidth - 1.4 * ColorUtil.removeColor(text).length()) / 2);

        return repeat(" ", spaces) + text;
    }


    public static List<String> getString(String findIn, String split, Character first, Character last) {
        List<String> ret = new ArrayList<>();
        boolean start = (first == null || !StringUtils.isEmpty(first+"")), end = false;
        String text = "";
        for(char c : findIn.toCharArray()) {
            if(last!=null&&c == last) end = true;
            if(start && !end) text+=c;
            if(first==null||c == first) start = true;

        }
        for(String s : text.split(split))
            ret.add(s);
        return ret;
    }


    public static String centerText(int maxWidth, String text, String endWith) {
        int spaces = (int) Math.round((maxWidth - 1.4 * ColorUtil.removeColor(text).length()) / 2);
        String repeat = repeat(" ", spaces);
        int x = repeat.length()+-text.length();
        int end = x < 0 ? 0 : x;
        return repeat + text + repeat(" ", maxWidth-spaces-text.toCharArray().length-1) + endWith;
    }

    public static String centerDefaultText(String text) {
        return centerText(80, text);
    }

    public static String[] toLowerCase(String... aliases) {
        String[] ret = new String[aliases.length];
        for(int i = 0; i < aliases.length; i++) {
            ret[i] = aliases[i].toLowerCase();
        }
        return ret;
    }

    public static String[] replace(String[] replaceIn, String target, String replacement) {
        for(int i = 0; i < replaceIn.length; i++) {
            replaceIn[i] = replaceIn[i].replace(target, replacement);
        }
        return replaceIn;
    }

    public static List<String> replace(List<String> replaceIn, String target, String replacement) {
        List<String> ret = new ArrayList<>();
        for(String s : replaceIn) {
            ret.add(s.replace(target, replacement));
        }
        return ret;
    }

    public static class Formatter {
        public static String format(Config config, String path, Object... objects) {
            String ret = config.getString(path);
            for (int i = 0; i < objects.length; i++) {
                try {
                    ret = ret.replace("{" + i + "}", findValue(objects[i]));
                }catch (NullPointerException e){continue;}
            }
            return translate(ret);
        }
        public static String formatDefault(String path, Object... objects) {
            return format(SuperAPI.getMessages(), path, objects);
        }

        public static String[] format(String[] str, Object... objects) {
            for(int j = 0; j < str.length; j++) {
                String s = str[j];
                for (int i = 0; i < objects.length; i++) {
                    try {
                        s = s.replace("{" + i + "}", findValue(objects[i]));
                    }catch (NullPointerException e){continue;}
                }
                str[j] = s;
            }
            return str;
        }
    }

    public static class ColorUtil {

        public static ChatColor locateChatColor(String l) {
            int id = l.length()+-1;
            ChatColor c = null;
            try {
                while(c == null) {
                    if(id < 0) {
                        c = null;
                        break;
                    }
                    c = ChatColor.getByChar(l.charAt(id));
                    id -= 1;
                    String x = c.name();
                }
            }catch (Exception t){
            }
            return c;
        }

        public static boolean isColor(String last) {
            return !isBold(last) && !isItalic(last) && !isStrikThrough(last) && !isUnderline(last);
        }

        private static boolean isBold(String last) {
            String x = (last);
            if(x.isEmpty() || x == "") return false;
            return x.charAt(x.length() + -1) == 'l';
        }
        private static boolean isItalic(String last) {
            String x = (last);
            if(x.isEmpty() || x == "") return false;
            return x.charAt(x.length() + -1) == 'o';
        }
        private static boolean isUnderline(String last) {
            String x = (last);
            if(x.isEmpty() || x == "") return false;
            return x.charAt(x.length() + -1) == 'n';
        }
        private static boolean isStrikThrough(String last) {
            String x = (last);
            if(x.isEmpty() || x == "") return false;
            return x.charAt(x.length() + -1) == 'm';
        }

        public static String convertColorCodes(char newChar, String str) {
            String r = str;
            if (str.contains("§") || str.contains("&"))
                for (ChatColor c : ChatColor.values()) {
                    if (str.contains("§" + c.getChar()))
                        r = r.replace("§" + c.getChar(), "" + newChar + c.getChar());
                }
            return r;
        }

        public static String removeColor(String str) {
            String r = StringUtils.translate(str);
            for(ChatColor c : ChatColor.values()) {
                if(r.contains("§"+c.getChar()))
                    r = r.replace("§" + c.getChar(), "");
                else if(r.contains("&"+c.getChar()))
                    r = r.replace("&" + c.getChar(), "");
            }
            return r;
        }

        public static String[] removeColor(String[] str) {
            String[] text = StringUtils.translate(str);
            for(int i = 0; i < text.length; i++) {
                text[i] = removeColor(text[i]);
            }
            return text;
        }
        public static String translateColors(char altColor, String text) {
            if(text == null) return "Error text is null";
            for(ChatColor c : ChatColor.values()) {
                if(text.contains((altColor+"")+c.getChar()))
                    text = text.replace((altColor+"") + c.getChar(), "§" + c.getChar());
            }
            return text;
        }

        public static String[] translateColors(char altColor, String[] text) {
            for(int i = 0; i < text.length; i++) {
                text[i] = translateColors(altColor, text[i]);
            }
            return text;
        }

        public static String translateChatColor(String text) {
            return translateColors('&', text);
        }
    }

}
