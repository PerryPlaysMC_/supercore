package dev.perryplaysmc.utils.text.strings;

import java.text.DecimalFormat;
import java.time.*;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("all")
public class TimeUtil {
    private static Pattern timePattern = Pattern.compile("(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?(?:([0-9]+)\\s*" +
            "mo[a-z]*[,\\s]*)?(?:([0-9]+)\\s*" +
            "w[a-z]*[,\\s]*)?(?:([0-9]+)\\s*" +
            "d[a-z]*[,\\s]*)?(?:([0-9]+)\\s*" +
            "h[a-z]*[,\\s]*)?(?:([0-9]+)\\s*" +
            "m[a-z]*[,\\s]*)?(?:([0-9]+)\\s*(?:s[a-z]*)?)?", 2);
    
    
    private static final int maxYears = Integer.MAX_VALUE;
    
    public static String removeTimePattern(String input) {
        Matcher m = timePattern.matcher(input);
        boolean found = false;
        while (m.find()) {
            if ((m.group() != null) && (!m.group().isEmpty()))
            {
                for (int i = 0; i < m.groupCount(); i++) {
                    if ((m.group(i) != null) && (!m.group(i).isEmpty())) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    if ((m.group(1) != null) && (!m.group(1).isEmpty())) {
                        input = input.replace(m.group(1) + "y,", "").replace(m.group(1) + "y", "");
                    }
                    if ((m.group(2) != null) && (!m.group(2).isEmpty())) {
                        input = input.replace(m.group(2) + "mo,", "").replace(m.group(2) + "mo", "");
                    }
                    if ((m.group(3) != null) && (!m.group(3).isEmpty())) {
                        input = input.replace(m.group(3) + "w,", "").replace(m.group(3) + "w", "");
                    }
                    if ((m.group(4) != null) && (!m.group(4).isEmpty())) {
                        input = input.replace(m.group(4) + "d,", "").replace(m.group(4) + "d", "");
                    }
                    if ((m.group(5) != null) && (!m.group(5).isEmpty())) {
                        input = input.replace(m.group(5) + "h,", "").replace(m.group(5) + "h", "");
                    }
                    if ((m.group(6) != null) && (!m.group(6).isEmpty())) {
                        input = input.replace(m.group(6) + "m,", "").replace(m.group(6) + "m", "");
                    }
                    if ((m.group(7) != null) && (!m.group(7).isEmpty())) {
                        input = input.replace(m.group(7) + "s,", "").replace(m.group(7) + "s", "");
                    }
                }
            }
        }
        return input;
    }
    
    public static long parseDateDiff(String time, boolean future) {
        Matcher m = timePattern.matcher(time);
        int years = 0;
        int months = 0;
        int weeks = 0;
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        boolean found = false;
        while (m.find()) {
            if ((m.group() != null) && (!m.group().isEmpty()))
            {
                for (int i = 0; i < m.groupCount(); i++) {
                    if ((m.group(i) != null) && (!m.group(i).isEmpty())) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    if ((m.group(1) != null) && (!m.group(1).isEmpty())) {
                        years = Integer.parseInt(m.group(1));
                    }
                    if ((m.group(2) != null) && (!m.group(2).isEmpty())) {
                        months = Integer.parseInt(m.group(2));
                    }
                    if ((m.group(3) != null) && (!m.group(3).isEmpty())) {
                        weeks = Integer.parseInt(m.group(3));
                    }
                    if ((m.group(4) != null) && (!m.group(4).isEmpty())) {
                        days = Integer.parseInt(m.group(4));
                    }
                    if ((m.group(5) != null) && (!m.group(5).isEmpty())) {
                        hours = Integer.parseInt(m.group(5));
                    }
                    if ((m.group(6) != null) && (!m.group(6).isEmpty())) {
                        minutes = Integer.parseInt(m.group(6));
                    }
                    if ((m.group(7) != null) && (!m.group(7).isEmpty())) {
                        seconds = Integer.parseInt(m.group(7));
                    }
                }
            }
        }
        if (!found) {
            try {
                throw new Exception("Illegal date!");
            }catch (Exception e) {
            
            }
        }
        Calendar c = new GregorianCalendar();
        Calendar now = new GregorianCalendar();
        ZoneId z = ZoneId.of("America/Montreal");
        ZonedDateTime zdt = ZonedDateTime.now(z);
        
        Date d = new Date(zdt.getYear(), zdt.getMonthValue(), zdt.getDayOfMonth(), zdt.getHour(), zdt.getMinute(), zdt.getSecond());
        now.setTime(d);
        now.set(d.getYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds());
        c = now;
        if (years > 0) {
            if (years > Integer.MAX_VALUE) {
                years = Integer.MAX_VALUE;
            }
            c.add(1, years * (future ? 1 : -1));
        }
        if (months > 0) {
            c.add(2, months * (future ? 1 : -1));
        }
        if (weeks > 0) {
            c.add(3, weeks * (future ? 1 : -1));
        }
        if (days > 0) {
            c.add(5, days * (future ? 1 : -1));
        }
        if (hours > 0) {
            c.add(11, hours * (future ? 1 : -1));
        }
        if (minutes > 0) {
            c.add(12, minutes * (future ? 1 : -1));
        }
        if (seconds > 0) {
            c.add(13, seconds * (future ? 1 : -1));
        }
        Calendar max = new GregorianCalendar();
        max.add(1, 10);
        if (c.after(max)) {
            return max.getTimeInMillis();
        }
        return c.getTimeInMillis();
    }
    
    public static Date parseDateDiffDate(String time, boolean future) {
        return new Date(parseDateDiff(time, future));
    }
    
    static int dateDiff(int type, Calendar fromDate, Calendar toDate, boolean future) {
        int year = 1;
        
        int fromYear = fromDate.get(year);
        int toYear = toDate.get(year);
        if (Math.abs(fromYear - toYear) > 100000) {
            toDate.set(year, fromYear + (future ? 100000 : -100000));
        }
        
        int diff = 0;
        long savedDate = fromDate.getTimeInMillis();
        while (((future) && (!fromDate.after(toDate))) || ((!future) && (!fromDate.before(toDate)))) {
            savedDate = fromDate.getTimeInMillis();
            fromDate.add(type, future ? 1 : -1);
            diff++;
        }
        diff--;
        fromDate.setTimeInMillis(savedDate);
        return diff;
    }
    
    public static Date getFutureDate(long date) {
        int seconds = (int) (date / 1000L);
        int minutes = (int) (seconds / 60L);
        int hours = (int) (minutes / 60L);
        int days = (int) (hours / 24L);
        int weeks = (int) (days / 7L);
        int months = (int) (days / 30L);
        int years = (int) (months / 12L);
    
        date -= seconds * 1000L;
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        days -= weeks * 7L;
        days -= months * 30L;
        System.out.println(years + "/" +  months + "/" + weeks + "/" + days + "/" + hours + "/" + minutes + "/" + seconds);
        Calendar d = Calendar.getInstance();
        d.setTimeInMillis(date);
        return d.getTime();
    }

    public static String formatDateDiff(long date) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(date);
        Calendar now = new GregorianCalendar();
        ZoneId z = ZoneId.of("America/Montreal");
        ZonedDateTime d = ZonedDateTime.now(z);
        now.set(d.getYear(), d.getMonth().getValue(), d.getDayOfMonth(), d.getHour(), d.getMinute(), d.getSecond());
        return formatDateDiff(now, c);
    }
    public static String formatDateDiffReverse(long date) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(date);
        Calendar now = new GregorianCalendar();
        /*ZoneId z = ZoneId.of("America/Montreal");
        ZonedDateTime d = ZonedDateTime.now(z);
        now.set(d.getYear(), d.getMonth().getValue(), d.getDayOfMonth(), d.getHour(), d.getMinute(), d.getSecond());*/
        return formatDateDiff(c, now);
    }
    
    public static String formatDateDiffSign(long date) {
        StringBuilder sb = new StringBuilder();
        long seconds = date / 1000L;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
    
        date -= seconds * 1000L;
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        if (days == 0 && hours == 0 && minutes == 0 && seconds == 0) {
            return "Now";
        }
        String m = minutes > 0 && minutes < 10 ? "0" + minutes : minutes+"";
        String h = hours > 0 && hours < 10 ? "0" + hours : hours+"";
        String d = days > 0 && days < 10 ? "0" + days : days+"";
        if(days==0)d="00";
        if(hours==0)h="00";
        if(minutes==0)m="00";
        sb.append(d + ":" +  h + ":" + m);
        String ret = sb.toString();
        return ret;
    }
    public static String formatSignDate(long date) {
        StringBuilder sb = new StringBuilder();
        long seconds = date / 1000L;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
        
        date -= seconds * 1000L;
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        if (days == 0 && hours == 0 && minutes == 0 && seconds == 0) {
            return "Now";
        }
        String s = seconds > 0 && seconds < 10 ? "0" + seconds : seconds+"";
        String m = minutes > 0 && minutes < 10 ? "0" + minutes : minutes+"";
        String h = hours > 0 && hours < 10 ? "0" + hours : hours+"";
        String d = days > 0 && days < 10 ? "0" + days : days+"";
        if(days==0)d="00";
        if(hours==0)h="00";
        if(minutes==0)m="00";
        if(seconds==0)s="00";
        sb.append(d + ":" +  h + ":" + m + ":" + s);
        String ret = sb.toString();
        return ret;
    }
    
    public static long getDateDiff(Date date1, Date date) {
        return date.getTime() - date1.getTime();
    }
    
    public static long convert(Date date) {
        return date.getTime();
    }
    
    public static Date getDate(long date) {
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(date);
        Calendar now = new GregorianCalendar();
        ZoneId z = ZoneId.of("America/Montreal");
        ZonedDateTime d = ZonedDateTime.now(z);
        now.set(d.getYear(), d.getMonth().getValue(), d.getDayOfMonth(), d.getHour(), d.getMinute(), d.getSecond());
        return getDate(now, c);
    }
    
    public static Date getDate(Calendar fromDate, Calendar toDate) {
        boolean future = false;
        if (toDate.equals(fromDate)) {
            return new Date();
        }
        if (toDate.after(fromDate)) {
            future = true;
        }
        StringBuilder sb = new StringBuilder();
        long diff = toDate.getTime().getTime() - fromDate.getTime().getTime();
        long seconds = diff / 1000L % 60L;
        long minutes = diff / 60000L % 60L;
        long hours = diff / 3600000L % 24L;
        long days = diff / 86400000L;
        long weeks = diff / 604800000L;
        long months = diff / -1702967296L;
        long years = diff / 1471228928L;
        Calendar now = new GregorianCalendar();
        ZoneId z = ZoneId.of("America/Montreal");
        ZonedDateTime d = ZonedDateTime.now(z);
        int month = (int)((d.getMonthValue()+months) > 12 ? (d.getMonthValue()+months)-12 : (d.getMonthValue()+months));
        Month m = Month.of(month);
        int day = (int)(d.getDayOfMonth()+days);
        int minute = (int) (minutes + d.getMinute());
        int second = (int) (seconds + d.getSecond());
        int hour = (int) hours+d.getHour();
        while(second >= 60) {
            second-=60;
            minute+=1;
        }
        while(minute >= 60) {
            minute-=60;
            hour+=1;
        }
        while(hour >= 24) {
            hour-=24;
            day+=1;
        }
        while(day > m.maxLength()) {
            day-=m.maxLength();
            month = month+1 > 12 ? 1 : month+1;
            m = Month.of(month);
        }
        Date date = new Date((int)(d.getYear()+years), month, day);
        date.setHours(hour);
        date.setMinutes(minute);
        date.setSeconds(second);
        
        return date;
    }
    
    public static String formatDateDiff(Calendar fromDate, Calendar toDate) {
        boolean future = false;
        if (toDate.equals(fromDate)) {
            return "Now";
        }
        if (toDate.after(fromDate)) {
            future = true;
        }
        StringBuilder sb = new StringBuilder();
        long diff = toDate.getTime().getTime() - fromDate.getTime().getTime();
        long seconds = diff / 1000L;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
        long weeks = days / 7L;
        long months = days / 30L;
        long years = months / 12L;
    
        diff -= seconds * 1000L;
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        days -= weeks * 7L;
        days -= months * 30L;
        
        if (years > 0) {
            sb.append(years + " " + (years > 1L ? "Years" : "Year"));
            if ((months > 0) || (weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(" ");
            }
        }
        if (months > 0) {
            sb.append(months + " " + (months > 1L ? "Months" : "Month"));
            if ((weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(" ");
            }
        }
        if (weeks > 0) {
            sb.append(weeks + " " + (weeks > 1L ? "Weeks" : "Week"));
            if ((days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(" ");
            }
        }
        if (days > 0) {
            sb.append(days + " " + (days > 1L ? "Days" : "Day"));
            if ((hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(" ");
            }
        }
        if (hours > 0) {
            sb.append(hours + " " + (hours > 1L ? "Hours" : "Hour"));
            if ((minutes > 0) || (seconds > 0)) {
                sb.append(" ");
            }
        }
        if (minutes > 0) {
            sb.append(minutes + " " + (minutes > 1L ? "Minutes" : "Minute"));
            if (seconds > 0) {
                sb.append(" ");
            }
        }
        if (seconds > 0) {
            sb.append(seconds + " " + (seconds > 1L ? "Seconds" : "Second"));
        }
        if (sb.length() == 0) {
            return "Now";
        }
        String ret = sb.toString();
        return ret;
    }
    
    public static String formatDate(ZonedDateTime d, boolean withMillis) {
        String month = d.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
        String AM_PM = d.getHour() >= 12 ? "PM" : "AM";
        String hour = ((d.getHour() > 12 ? d.getHour() + -12 : d.getHour()) + "").replace("-", "") + AM_PM;
        String ret = "[pc]" + d.getYear() + "[c]-[pc]" + month + "[c]-[pc]" +
                     d.getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH) +
                     "[c]-[pc]" + hour + "[c]:[pc]" + (d.getSecond() < 10 ? "0" + d.getSecond() : d.getSecond()) + "[c]:[pc]" + d.getSecond() + (withMillis ? "[c]:[pc]" +
                                                                                                                                                              (d.getNano() + "").substring(0, (d.getNano() + "").length() > 1 ? 1 : (d.getNano() + "").length() > 2 ? 2 : 0) : "");
        return StringUtils.translate(ret);
    }
   
    public static String formatDateWithoutTime(ZonedDateTime d) {
        String month = d.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH);
        String ret = "[pc]" + d.getYear() + "[c]-[pc]" + month + "[c]-[pc]" + d.getDayOfWeek().getDisplayName(TextStyle.SHORT_STANDALONE, Locale.ENGLISH);
        return StringUtils.translate(ret);
    }
   
    public static String formatTime(ZonedDateTime d, boolean withMillis) {
        ZoneId zz = ZoneId.of(ZoneId.SHORT_IDS.get("CST")),
                est = ZoneId.of(ZoneId.SHORT_IDS.get("EST"));
        int offset = 0;
        
        if(zz.getId() == d.getZone().getId() || est.getId() ==  d.getZone().getId()) {
            if(zz.getRules().isDaylightSavings(d.toInstant())) {
                offset = Math.toIntExact(zz.getRules().getDaylightSavings(d.toInstant()).getSeconds() / 60 / 60);
            }
        }
        String AM_PM = d.getHour() >= 12 ? "PM" : "AM";
        int hr = offset+d.getHour();
        String hour = (((hr > 12 ? hr + -12 : hr)) + "").replace("-", "") + AM_PM;
        String ret = hour + "[c]:[pc]" + d.getMinute() + "[c]:[pc]" + (d.getSecond() < 10 ? "0" + d.getSecond() : d.getSecond()) + (withMillis ? "[c]:[pc]" +
                                                                                                                                                 ((d.getNano()) + "").substring(0, (d.getNano() + "").length() > 1 ? 1 : (d.getNano() + "").length() > 2 ? 2 : 0) : "");
        return StringUtils.translate(ret);
    }
    
    public static String formatDate(Date d) {
        int monthI = d.getMonth()+1;
        String month = Month.of(monthI).name().charAt(0) + Month.of(monthI).name().substring(1).toLowerCase();
        String AM_PM = d.getHours() >= 12 ? "PM" : "AM";
        String hour = ((d.getHours() > 12 ? d.getHours() + -12 : d.getHours()) + "").replace("-", "") + AM_PM;
        d.setYear(d.getYear()+1900);
        String ret = "[pc]" + (d.getYear()) + "[c]-[pc]" + month + "[c]-[pc]" + d.getDate() +
                     "[c]-[pc]" + hour + "[c]:[pc]" + d.getMinutes() + "[c]:[pc]" + (d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds());
        return StringUtils.translate(ret);
    }
    
    public static String formatDate(Date d, boolean showYearMonth) {
        int monthI = d.getMonth() == 0 ? 12 : d.getMonth();
        String month = Month.of(monthI).name().charAt(0) + Month.of(monthI).name().substring(1).toLowerCase();
        String AM_PM = d.getHours() >= 12 ? "PM" : "AM";
        String hour = ((d.getHours() > 12 ? d.getHours() + -12 : d.getHours()) + "").replace("-", "") + AM_PM;
        ZoneId zz = ZoneId.of("America/Montreal");
        ZonedDateTime zdt = ZonedDateTime.now(zz);
        if (d.getYear() < zdt.getYear()) {
            d.setYear(zdt.getYear());
        }
        String ret = (showYearMonth?"[pc]" + d.getYear() + "[c]-[pc]" + month + "[c]-":"")+"[pc]" + d.getDate() +
                     "[c]-[pc]" + hour + "[c]:[pc]" + d.getMinutes() + "[c]:[pc]" + (d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds());
        return StringUtils.translate(ret);
    }
    
    public static Date getDate() {
        ZoneId zz = ZoneId.of("America/Montreal");
        ZonedDateTime zdt = ZonedDateTime.now(zz);
        return new Date(new Date().getYear(), zdt.getMonthValue()-1, zdt.getDayOfMonth(), zdt.getHour(), zdt.getMinute(), zdt.getSecond());
    }
    
    public static String getFormatedTimeR(int seconds, boolean showDHMS) {
        return getFormatedTime(seconds, showDHMS);
    }
    
    public static String getFormatedTimeMillis(long millis, boolean showDHMS) {
        return getFormatedTimeMillis(millis, false, showDHMS);
    }
    
    public static String getFormatedTimeMillis(long millis, boolean concat, boolean showDHMS) {
        long seconds = millis / 1000L;
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
        long weeks = days / 7L;
        long months = days / 30L;
        long years = months / 12L;
        
        millis -= seconds * 1000L;
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        days -= weeks * 7L;
        days -= months * 30L;
        
        String min = minutes > 1L ? " Minutes" : " Minute";
        String se = seconds > 1L ? " Seconds" : " Second";
        String hr = hours > 1L ? " Hours" : " Hour";
        String day = days > 1L ? " Days" : " Day";
        
        String secondsStr = seconds + "";
        
        long m = minutes;long h = hours;long d = days;
        
        StringBuilder sb = new StringBuilder();
        if (years > 0) {
            sb.append(years + (showDHMS ? (years > 1 ? " Years" : " Year") : ""));
            if ((months > 0) || (weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (months > 0) {
            sb.append(months + (showDHMS ? (months > 1 ? " Months" : " Month") : ""));
            if ((weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (weeks > 0) {
            sb.append(weeks + (showDHMS ? (weeks > 1 ? " Weeks" : " Week") : ""));
            if ((days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (days > 0) {
            sb.append(days + (showDHMS ? (days > 1 ? " Days" : " Day") : ""));
            if ((hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (hours > 0) {
            sb.append(hours + (showDHMS ? (hours > 1 ? " Hours" : " Hour") : ""));
            if ((minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (minutes > 0) {
            sb.append(minutes + (showDHMS ? (minutes > 1 ? " Minutes" : " Minute") : ""));
            if (seconds > 0) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (seconds > 0) {
            sb.append(secondsStr);
            if (millis > 0) {
                sb.append(".");
            }else {
                sb.append((showDHMS ? (seconds > 1 ? " Seconds" : " Second") : ""));
            }
        }
        if (millis > 0) {
            DecimalFormat f = new DecimalFormat("##");
            if (seconds <= 0L) {
                sb.append("0.");
            }
            sb.append(f.format(millis) + (showDHMS ? " Seconds" : ""));
        }
        
        return sb.toString();
    }
    
    public static String getFormatedTime(long seconds, boolean showDHMS) {
        long minutes = seconds / 60L;
        long hours = minutes / 60L;
        long days = hours / 24L;
        long weeks = days / 7L;
        long months = days / 30L;
        long years = months / 12L;
        
        seconds -= minutes * 60L;
        minutes -= hours * 60L;
        hours -= days * 24L;
        days -= weeks * 7L;
        days -= months * 30L;
        
        String min = minutes > 1 ? " Minutes" : " Minute";
        String se = seconds > 1 ? " Seconds" : " Second";
        String hr = hours > 1 ? " Hours" : " Hour";
        String day = days > 1 ? " Days" : " Day";
        
        String secondsStr = seconds < 10 ? "0"+seconds + "" : seconds+"";
        
        StringBuilder sb = new StringBuilder();
        if (years > 0) {
            sb.append(years + (showDHMS ? (years > 1 ? " Years" : " Year") : ""));
            if ((months > 0) || (weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (months > 0) {
            sb.append(months + (showDHMS ? (months > 1 ? " Months" : " Month") : ""));
            if ((weeks > 0) || (days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (weeks > 0) {
            sb.append(weeks + (showDHMS ? (weeks > 1 ? " Weeks" : " Week") : ""));
            if ((days > 0) || (hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (days > 0) {
            sb.append(days + (showDHMS ? (days > 1 ? " Days" : " Day") : ""));
            if ((hours > 0) || (minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (hours > 0) {
            sb.append(hours + (showDHMS ? (hours > 1 ? " Hours" : " Hour") : ""));
            if ((minutes > 0) || (seconds > 0)) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (minutes > 0) {
            sb.append(minutes + (showDHMS ? (minutes > 1 ? " Minutes" : " Minute") : ""));
            if (seconds > 0) {
                sb.append(showDHMS ? " " : ":");
            }
        }
        if (seconds > 0) {
            sb.append(secondsStr + (showDHMS ? " Second" : seconds > 1L ? " Seconds" : ""));
        }
        
        return sb.toString();
    }
    
    public static String format(int i, boolean showDHMS) {
        return getFormatedTime(i, showDHMS);
    }
    
    public static double getSecondsUntil(Date expires) {
        long time = expires.getTime()-getDate().getTime();
        return time/1000;
    }
    public static long getSecondsUntilFrom(Date start, Date expires) {
        long time1 = expires.getTime(),
                time2 = start.getTime();
        if(start.getTime()>expires.getTime()) {
            time1 = start.getTime();
            time2 = expires.getTime();
        }
        
        long time = time1-time2;
        return time;
    }
}
