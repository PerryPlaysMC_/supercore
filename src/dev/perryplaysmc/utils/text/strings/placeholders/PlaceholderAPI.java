package dev.perryplaysmc.utils.text.strings.placeholders;

import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import io.netty.util.internal.StringUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.text.strings.placeholders
 * Class: Placeholder
 * <p>
 * Path: dev.perryplaysmc.utils.text.strings.placeholders.Placeholder
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class PlaceholderAPI {

    static Set<Placeholder> placeholders = new HashSet<>();

    public static String parseUserPlaceHolders(String text, Object... possibles) {
        String ret = text;
        for(Placeholder placeholder : placeholders) {
            String f = "";
            boolean start = false;
            F:for(String identifier : text.split("%")) {
                A:for(Object s : possibles) {
                if(!identifier.contains("_")) continue F;
                String name = "";
                    B:for(String id : placeholder.getIdentifiers()) {
                        if(identifier.startsWith(id)) {
                            name = id;
                            break B;
                        }
                    }
                    if(StringUtils.isEmpty(name))continue F;

                    String replacement = placeholder.parse(identifier.split(name + "_")[1], s);
                    if(replacement == null || StringUtils.isEmpty(replacement) || replacement.equalsIgnoreCase("unknown identifier '" + identifier + "'"))continue A;
                    if(ret.contains("%" + identifier + "%"))
                        ret = ret.replace("%" + identifier + "%", replacement);
                }
            }
            /*for(char c : ret.toLowerCase().toCharArray()) {
                String n = "";
                F:for(String name : placeholder.getIdentifiers()) {
                    if(f.startsWith("%" + name.toLowerCase() + "_")) {
                        n = name.toLowerCase();
                        break F;
                    }
                }
                if(f.startsWith("%" + n + "_") && f.endsWith("%")) {
                    String identifier = f.split("%" + n + "_")[1].toLowerCase();
                    if(!StringUtils.isEmpty(identifier))
                        for(Object s : possibles)
                            ret = ret.replace(f, placeholder.parse(identifier.substring(0, identifier.length()-1), s));
                }
                if(c=='%') {
                    start = !start;
                    if(!start)
                        f+=c;
                }
                if(start)
                    f+=c;
                if(c == ' ') {
                    f = "";
                }
            }*/
        }
        return ret;
    }

}
