package dev.perryplaysmc.utils.text.strings.placeholders.holders;

import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.Utils;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import dev.perryplaysmc.utils.text.strings.TimeUtil;
import dev.perryplaysmc.utils.text.strings.placeholders.Placeholder;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.text
 * Class: UserPlaceholder
 * <p>
 * Path: dev.perryplaysmc.utils.text.UserPlaceholder
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class UserPlaceholder extends Placeholder {


    @Override
    public String[] getIdentifiers() {
        return new String[] {"user", "player"};
    }

    @Override
    public String parse(String identifier, Object user1) {
        OfflineUser user = null;
        if(user1 instanceof OfflineUser) {
            user = (OfflineUser)user1;
        }
        if(user == null) return "";
        if(!user.getSource().isOnline()) return "User is not online";
        User u = SuperAPI.getUser(user.getUniqueId());
        if(identifier.equalsIgnoreCase("money"))
            return u.getFormattedBalance();
        if(identifier.equalsIgnoreCase("balance"))
            return u.getFormattedBalance();
        if(identifier.equalsIgnoreCase("name"))
            return u.getName();
        if(identifier.equalsIgnoreCase("currentinventory"))
            return u.getCurrentInventory();
        if(identifier.equalsIgnoreCase("gamemode"))
            return StringUtils.getNameFromEnum(u.getGameMode());
        if(identifier.equalsIgnoreCase("health"))
            return u.getHealth()+"";
        if(identifier.equalsIgnoreCase("food")||identifier.equalsIgnoreCase("hunger"))
            return u.getHunger()+"";
        if(identifier.equalsIgnoreCase("saturation"))
            return u.getSaturation()+"";
        if(identifier.equalsIgnoreCase("time_played"))
            return TimeUtil.formatDateDiffReverse(u.getJoinTime());

        if(identifier.equalsIgnoreCase("location"))
            return Utils.convertLocation(u.getLocation());
        //Overworld
        if(identifier.equalsIgnoreCase("x"))
            return u.getLocation().getBlockX()+"";
        if(identifier.equalsIgnoreCase("y"))
            return u.getLocation().getBlockY()+"";
        if(identifier.equalsIgnoreCase("z"))
            return u.getLocation().getBlockZ()+"";
        //Nether
        if(identifier.equalsIgnoreCase("nether_x"))
            return (u.getLocation().getBlockX()/8)+"";
        if(identifier.equalsIgnoreCase("nether_y"))
            return u.getLocation().getBlockY()+"";
        if(identifier.equalsIgnoreCase("nether_z"))
            return (u.getLocation().getBlockZ()/8)+"";

        return "unknown identifier '" + identifier + "'";
    }
}
