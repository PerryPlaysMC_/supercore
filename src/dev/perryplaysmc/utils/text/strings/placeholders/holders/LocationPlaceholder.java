package dev.perryplaysmc.utils.text.strings.placeholders.holders;

import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.utils.text.strings.placeholders.Placeholder;
import org.bukkit.Location;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.text.strings.placeholders.holders
 * Class: LocationPlaceholder
 * <p>
 * Path: dev.perryplaysmc.utils.text.strings.placeholders.holders.LocationPlaceholder
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class LocationPlaceholder extends Placeholder {


    @Override
    public String[] getIdentifiers() {
        return new String[] {"location", "loc"};
    }

    @Override
    public String parse(String identifier, Object object) {
        Location loc = null;
        if(object instanceof Location) {
            loc = (Location)object;
        }
        if(loc == null) return "";
        if(identifier.equalsIgnoreCase("x"))
            return loc.getX()+"";
        if(identifier.equalsIgnoreCase("y"))
            return loc.getY()+"";
        if(identifier.equalsIgnoreCase("z"))
            return loc.getZ()+"";

        if(identifier.equalsIgnoreCase("block_x"))
            return loc.getBlockX()+"";
        if(identifier.equalsIgnoreCase("block_y"))
            return loc.getBlockY()+"";
        if(identifier.equalsIgnoreCase("block_z"))
            return loc.getBlockZ()+"";

        if(identifier.equalsIgnoreCase("chunk_x"))
            return loc.getChunk().getX()+"";
        if(identifier.equalsIgnoreCase("chunk_z"))
            return loc.getChunk().getZ()+"";
        if(identifier.equalsIgnoreCase("chunk_isSlime"))
            return loc.getChunk().isSlimeChunk()+"";

        return "unknown identifier '" + identifier + "'";
    }
}
