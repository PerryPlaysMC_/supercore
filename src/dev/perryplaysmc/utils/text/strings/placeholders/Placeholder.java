package dev.perryplaysmc.utils.text.strings.placeholders;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.text.strings.placeholders
 * Class: Placeholder
 * <p>
 * Path: dev.perryplaysmc.utils.text.strings.placeholders.Placeholder
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public abstract class Placeholder {

    public Placeholder() {
        PlaceholderAPI.placeholders.add(this);
    }

    public abstract String[] getIdentifiers();

    public abstract String parse(String identifier, Object object);

}
