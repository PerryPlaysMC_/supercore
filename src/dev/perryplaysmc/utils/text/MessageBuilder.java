package dev.perryplaysmc.utils.text;

import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.nmsUtils.NMSMain;
import dev.perryplaysmc.utils.text.json.FancyMessage;
import dev.perryplaysmc.utils.text.json.JsonString;
import dev.perryplaysmc.utils.text.json.command.JsonHandler;
import dev.perryplaysmc.utils.text.json.command.Run;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public class MessageBuilder {
    
    private FancyMessage base;
    public String last = "";
    public String original = "";
    private int latestSize;
    private CommandSource s;
    private ChatColor[] styles;
    private List<FancyMessage> toSend;

    private MessageBuilder(FancyMessage msg) {
        this.base = msg;
    }

    private MessageBuilder() {
        this("");
    }

    private MessageBuilder(String text) {
        this(null, text);
    }

    private MessageBuilder(CommandSource u, String text) {
        this.toSend = new ArrayList<>();
        base = new FancyMessage("");
        then(u, StringUtils.translate(text));
        this.s = u;
    }

    public static MessageBuilder create(FancyMessage msg) {
        return new MessageBuilder(msg);
    }

    public static MessageBuilder create(CommandSource u, String text) {
        return new MessageBuilder(u, text);
    }

    public static MessageBuilder create(String text) {
        return new MessageBuilder(text);
    }

    public static MessageBuilder create() {
        return new MessageBuilder();
    }
    
    public MessageBuilder clone() {
        return new MessageBuilder(base);
    }
    
    public MessageBuilder then(String txtt) {
        return then(null, txtt, TextOptions.KEEP_COLORS);
    }
    
    public MessageBuilder then(String txtt, TextOptions options) {
        return then(null, txtt, options);
    }
    
    public MessageBuilder then(String perm, String txtt) {
        if(s!=null) {
            if(s.hasPermission(perm)) return then(txtt);
            else {
                return then("");
            }
        }
        return then(txtt);
    }
    
    public int size() {
        return base.getParts().size();
    }
    
    public MessageBuilder then(boolean doLink, String txtt) {
        return then(doLink, null, txtt, TextOptions.KEEP_COLORS);
    }
    
    public MessageBuilder breakLine() {
        return then(null, "\n", TextOptions.KEEP_COLORS);
    }
    
    public MessageBuilder br() {
        return breakLine();
    }
    
    
    public MessageBuilder then(boolean doLink, CommandSource u, String txtt) {
        return then(doLink, u, txtt, TextOptions.KEEP_COLORS);
    }
    
    public MessageBuilder then(CommandSource u, String txtt) {
        return then(u, txtt, TextOptions.KEEP_COLORS);
    }
    public MessageBuilder then() {
        return then(null,"", TextOptions.KEEP_COLORS);
    }
    
    public MessageBuilder then(CommandSource u, String txtt, TextOptions options) {
        return then(false, u, txtt, options);
    }
    public MessageBuilder then(boolean doLink, CommandSource u, String txtt, TextOptions options) {
        return thenRaw(doLink, u != null && u.isPlayer() ? StringUtils.translate(txtt, original, u.getUser())
                : StringUtils.translate(txtt), options);
    }
    
    private FancyMessage thenX(String text) {
        if(!base.latest().hasText())
            return base.text(text);
        else
            return base.then(text);
    }
    
    private MessageBuilder thenRaw(boolean doLink, String txtt, TextOptions options) {
        String[] split = txtt.split(" ");
        latestSize = 0;
        if(split.length == 0){
            if(!txtt.isEmpty())
                thenX(txtt);
            return this;
        }
        for(int i = 0; i < split.length; i++) {
            String msg = split[i];
            String m = StringUtils.ColorUtil.convertColorCodes('&', msg).replace(original.replace("§", "&"), original.replace("&", "§"));
            String og = StringUtils.ColorUtil.removeColor(m);
            String x1 = og.startsWith("https://") | og.startsWith("http://") ? og : "https://" + og;
            if(x1.startsWith("https://") || x1.startsWith("http://")) {
                og = x1;
            } else if(!x1.startsWith("https://") && !x1.startsWith("http://")) {
                og = "https://" + x1;
            }
            setupText(last+msg, options);
            if(StringUtils.checkForDomain(og)&&doLink) {
                hover("[c]Click to go to: ", "[pc]" + og).link(og);
            }
            if(styles !=null&&styles.length>0)
                base.style(styles);
            if(i != (split.length+-1)) {
                thenX(" ");
                latestSize++;
            }
            latestSize++;
        }
        if(txtt.endsWith(" ")) {
            thenX(" ");
        }
        return this;
    }
    
    private void setupText(String message, TextOptions options) {
        char[] chars = message.toCharArray();
        String m = "";
        for(int i = 0; i < chars.length; i++) {
            char current = chars[i];
            char prev = i > 0 ? chars[i+-1] : '`';
            char next = i < chars.length+-1 ? chars[i+1] : '`';
            char next3 = i < chars.length+-3 ? chars[i+3] : '`';
            m+=current;
            if(current == '§') {
                if(!last.endsWith((""+current)+(""+next))) {
                    if(next=='r') {
                        last+=original;
                    }else {
                        last += ("" + current) + ("" + next);
                        if(StringUtils.ColorUtil.isColor(("" + current) + ("" + next)) || next == 'r') {
                            last = ("" + current) + ("" + next);
                        }
                    }
                }
            }
            if(i == chars.length+-1) {
                String nm=m;//reverse(m);
                if(last.isEmpty()) {
                    last=original;
                }
                ChatColor color = StringUtils.ColorUtil.locateChatColor(last);
                thenX(nm);
                if(color!=null)
                    if(color.isColor()) {
                        base.color(color);
                    }else if(color.isFormat())
                        base.style(color);
                m = "";
            }
        }
    }
    
    private String reverse(String x) {
        String ret = "";
        char[] chars = x.toCharArray();
        for(int i = chars.length+-1; i>-1;i--) {
            ret+=chars[i];
        }
        return ret;
    }
    
    private void set(String last, boolean addBold, boolean addStrikeThrough, boolean addUnderLine, boolean addItalic, boolean addMagic) {
        if(last.toLowerCase().contains("l"))
            addBold = true;
        if(last.toLowerCase().contains("m"))
            addStrikeThrough = true;
        if(last.toLowerCase().contains("n"))
            addUnderLine = true;
        if(last.toLowerCase().contains("o"))
            addItalic = true;
        if(last.toLowerCase().contains("k"))
            addMagic = true;
    }
    
    private void set(ChatColor c, boolean addBold, boolean addStrikeThrough, boolean addUnderLine, boolean addItalic, boolean addMagic) {
        if(c != null) base.color(c);
        if(addBold)base.style(ChatColor.BOLD);
        if(c != null) base.color(c);
        if(addStrikeThrough)base.style(ChatColor.STRIKETHROUGH);
        if(c != null) base.color(c);
        if(addUnderLine)base.style(ChatColor.UNDERLINE);
        if(c != null) base.color(c);
        if(addItalic)base.style(ChatColor.ITALIC);
        if(c != null) base.color(c);
        if(addMagic)base.style(ChatColor.MAGIC);
        if(c != null) base.color(c);
    }
    
    public MessageBuilder command(String text) {
        if(!text.startsWith("/")) text = "/" + text;
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = StringUtils.ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }

    public MessageBuilder command(Run r) {
        String text = UUID.randomUUID().toString();
        while(JsonHandler.getCommand(text) != null) {
            text = UUID.randomUUID().toString();
        }
        JsonHandler.addCommand(text, r);
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = StringUtils.ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }
    
    public MessageBuilder command(Run r, String... args) {
        String text = UUID.randomUUID().toString();
        while(JsonHandler.getCommand(text) != null) {
            text = UUID.randomUUID().toString();
        }
        JsonHandler.addCommand(text, r);
        String arguments = "";
        if(args.length > 0) {
            arguments = " ";
            for(int i = 0; i < args.length; i++) {
                arguments+=args[i]+" ";
            }
            arguments.substring(0, arguments.length()+-1);
        }
        text += arguments;
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = StringUtils.ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }
    
    public MessageBuilder forceChat(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = StringUtils.ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }
    
    public MessageBuilder suggest(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = StringUtils.ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "suggest_command";
        }
        return this;
    }
    
    public MessageBuilder insert(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).insertionData = StringUtils.ColorUtil.removeColor(text);
        }
        return this;
    }
    
    public MessageBuilder link(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = StringUtils.ColorUtil.removeColor(text);
            base.getParts().get(i).clickActionName = "open_url";
        }
        return this;
    }
    
    public MessageBuilder tooltip(ItemStack item) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).hoverActionData = new JsonString(NMSMain.getItemStackJSONConverter().convertItemStack(item));
            base.getParts().get(i).hoverActionName = "show_item";
        }
        return this;
    }
    public MessageBuilder hover(ItemStack item) {
        return tooltip(item);
    }
    
    public MessageBuilder hover(String... text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            StringBuilder builder = new StringBuilder();
            for (int ii = 0; ii < text.length; ii++) {
                builder.append(text[ii]);
                if (ii != text.length - 1) {
                    builder.append('\n');
                }
            }
            base.getParts().get(i).hoverActionData = new JsonString(StringUtils.translate(builder.toString()));
            base.getParts().get(i).hoverActionName = "show_text";
        }
        return this;
    }
    
    public MessageBuilder removeLinkData() {
        for(int i = 0; i < base.getParts().size(); i++) {
            if(base.getParts().get(i)!=null&&!StringUtils.isEmpty(base.getParts().get(i).clickActionName)&&base.getParts().get(i).clickActionName.equalsIgnoreCase("open_url")) {
                base.getParts().get(i).hoverActionData = null;
                base.getParts().get(i).hoverActionName = null;
                base.getParts().get(i).clickActionData = null;
                base.getParts().get(i).clickActionName = null;
            }
        }
        return this;
    }
    
    public String toString() {
        return base.toJSONString().replace("},", "},\n");
    }
    
    public MessageBuilder tooltip(String... text) {
        return hover(text);
    }
    
    public MessageBuilder tooltip(List<String> text) {
        return hover(text.toArray(new String[text.size()]));
    }
    
    public void send(User u){
        base.send(u);
    }
    
    public void send(OfflineUser u){
        base.send(u);
    }
    
    
    public void send(CommandSource u){
        base.send(u.getSource());
    }
    
    public void send(Player u) {
        base.send(u);
    }


    public void clear() {
        base.getParts().clear();
    }
}
