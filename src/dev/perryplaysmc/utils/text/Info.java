package dev.perryplaysmc.utils.text;

import dev.perryplaysmc.commands.Command;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.utils.text.strings.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class Info {
    
    
    Command cmd;
    String name;
    MessageBuilder fb;
    CommandSource s;
    int amount = 0;
    List<MessageBuilder> msgs = new ArrayList<>();
    
    public Info(Command cmd) {
        this.cmd = cmd;
        this.name = cmd.getName();
        fb = MessageBuilder.create("[c]" + ((cmd.getName().charAt(0) + "").toUpperCase() + cmd.getName().substring(1)) + " Commands: ");
        fb.then("\n\n[c]Hover for info!\n" +
                "\n[c] [o][argument][c] &7= Optional " +
                "\n [r]<argument>[c] &7= Required" +
                "\n [a]argument[c] &7= Object\n");
    }
    
    public Info(Command cmd, CommandSource s) {
        this(cmd);
        this.s = s;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Info setSource(CommandSource s) {
        this.s = s;
        return this;
    }
    
    List<String> perms = new ArrayList<>();
    public Info addSub(String sub) {
        amount++;
        msgs.add(fb);
        fb = MessageBuilder.create(" [c]-/[pc]" + name + " [r]" + sub);
        return this;
    }
    
    public Info addSubPerm(String sub) {
        return addSub(cmd.getDefaultPermissions(), sub);
    }
    
    public Info addSub(String[] permissions, String sub) {
        perms.clear();
        for(String s : permissions) {
            if(!perms.contains(s.toLowerCase()))
                perms.add(s.toLowerCase());
        }
        for(String s : cmd.getDefaultPermissions()) {
            if(!perms.contains(s.toLowerCase()))
                perms.add(s.toLowerCase());
        }
        if(s != null) {
            if(s.hasPermission(StringUtils.convertToArray(perms)))
                return addSub(sub);
            else return this;
        }
        perms.clear();
        return addSub(sub);
    }
    
    public Info tooltip(String... messages) {
        if(s != null) {
            if(s.hasPermission(StringUtils.convertToArray(perms)) || perms.size() == 0) {
                fb.tooltip(StringUtils.Formatter.format(messages, cmd.getLabel().getName()));
            }
        }else {
            fb.tooltip(StringUtils.Formatter.format(messages, cmd.getLabel().getName()));
        }
        return this;
    }
    
    public Info command(String command) {
        command = command.startsWith("/" + cmd.getLabel().getName() + " ") ? command : "/" + cmd.getLabel().getName() + " " + command;
        if(s != null) {
            if(s.hasPermission(StringUtils.convertToArray(perms)) || perms.size() == 0) {
                fb.command(command);
            }
        }else {
            fb.command(command);
        }
        return this;
    }
    
    public Info suggest(String command) {
        command = command.startsWith("/" + cmd.getLabel().getName() + " ") ? command : "/" + cmd.getLabel().getName() + " " + command;
        if(s != null) {
            if(s.hasPermission(StringUtils.convertToArray(perms)) || perms.size() == 0) {
                fb.suggest(command);
            }
        }else {
            fb.suggest(command);
        }
        return this;
    }
    
    public Info insert(String command) {
        command = command.startsWith("/" + cmd.getLabel().getName() + " ") ? command : "/" + cmd.getLabel().getName() + " " + command;
        if(s != null) {
            if(s.hasPermission(StringUtils.convertToArray(perms)) || perms.size() == 0) {
                fb.insert(command);
            }
        }else {
            fb.insert(command);
        }
        return this;
    }
    
    public void send(CommandSource s) {
        if(!msgs.contains(fb))
            msgs.add(fb);
        for(MessageBuilder msg : msgs)
            msg.send(s);
    }
    public void send(OfflineUser s) {
        if(!msgs.contains(fb))
            msgs.add(fb);
        for(MessageBuilder msg : msgs)
            msg.send(s);
    }
    
    
    
}
