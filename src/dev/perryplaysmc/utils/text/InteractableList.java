package dev.perryplaysmc.utils.text;

import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.text
 * Class: InteractableList
 * <p>
 * Path: dev.perryplaysmc.utils.text.InteractableList
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class InteractableList<T> {

    String header, footer;
    List<T> toParse;
    MessageBuilder msg;
    String pageCommand;
    int page = 0;
    int index = 0;
    Show<T> textToDisplay;
    Hover<T> hoverText;
    Click<T> clickText;
    boolean fillNullSpace = false;

    public InteractableList(String pageCommand, int page,String header, Set<T> toParse, String footer) {
        this(pageCommand, page, header, new ArrayList<>(toParse), footer);
    }


    public InteractableList(String pageCommand, int page, String header, List<T> toParse, String footer) {
        this.page = page;
        this.header = header;
        this.toParse = toParse;
        this.footer = footer;
        this.pageCommand = pageCommand;
        set();
    }

    void set() {
        msg = MessageBuilder.create(header+"\n");
        if(this.page < 1) this.page = 1;
        if(this.page > getTotalPages()) this.page = getTotalPages();
        if(page > 1)
            msg.then(StringUtils.centerText((header+" ").length(),"")).then("[c]<<").command(pageCommand + (page-1))
                    .hover("[c]Click to go to the previous page");
        else
            msg.then(StringUtils.centerText((header+" ").length(),"")).then("&8<<").hover("[c]You're at the first page already!");
        msg.then(" [c]([pc]" + page + "[c]/[pc]" + getTotalPages()+"[c]) ");
        if(page < getTotalPages())
            msg.then("[c]>>").command(pageCommand + " " + (page+1)).hover("[c]Click to go to the next page");
        else
            msg.then("&8>>").hover("[c]You're at the last page already!");
    }

    public int getTotalPages() {
        int x = toParse.size();
        return (x + (x % 10 > 0 ? (10 - (x % 10)) : 0)) / 10;
    }

    public int getPage() {
        return page;
    }

    public List<T> currentParse() {
        List<T> t = new ArrayList<>();
        index = (page*10)-10;
        for(int i = index; i < index+10; i++) {
            if(i >= toParse.size()) {
                if(!fillNullSpace) {
                    return t;
                }
                if(t.size() < 10) {
                    t.add(null);
                }else {
                    return t;
                }
                continue;
            }
            t.add(toParse.get(i));
        }
        return t;
    }
    public void click(Click<T> clickText) {
        this.clickText = clickText;
    }

    public void text(Show<T> textToDisplay) {
        this.textToDisplay = textToDisplay;
    }

    public void hover(Hover<T> hoverText) {
        this.hoverText = hoverText;
    }

    public void onCreate() {
        set();
        List<Hold> orginized = new ArrayList<>();
        if(currentParse().size()>0)
            for(T t : currentParse()) {
                if(t == null) {
                    orginized.add(new Hold("null", "", ""));
                    continue;
                }
                String text = "";
                String hover = "";
                String click = "";
                if(textToDisplay!=null)
                    text=(textToDisplay.getText(t));
                if(textToDisplay!=null&&hoverText!=null)
                    hover=(hoverText.getText(t));
                if(textToDisplay!=null&&clickText!=null)
                    click=(clickText.getText(t));
                if(!StringUtils.isEmpty(text)) {
                    orginized.add(new Hold(text,hover,click));
                }
            }
        List<String> texts = new ArrayList<>();
        for(Hold h : orginized) {
            texts.add(h.text);
        }
        Collections.sort(texts);
        if(orginized.size()>0) {
            for(String s : texts) {
                Hold h = null;
                A:for(Hold h1 : orginized) {
                    if(h1.text == s) {
                        h = h1;
                        break A;
                    }
                }
                if(!StringUtils.isEmpty(h.text)) {
                    if(h.text.equalsIgnoreCase("null")) {
                        msg.then("\n");
                        continue;
                    }
                    msg.then("\n" + h.text);
                    if(!StringUtils.isEmpty(h.hover))
                        msg.hover(h.hover);;
                    if(!StringUtils.isEmpty(h.click))
                        msg.suggest(h.click);
                }
            }
        }
    }

    @AllArgsConstructor
    class Hold {
        String text, hover, click;
    }

    public void send(CommandSource s) {
        onCreate();
        msg.then("\n"+footer);
        msg.send(s);
    }


    public interface Hover<T> {
        String getText(T t);
    }

    public interface Show<T> {
        String getText(T t);
    }

    public interface Click<T> {
        String getText(T t);
    }


}
