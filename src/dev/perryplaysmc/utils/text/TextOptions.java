package dev.perryplaysmc.utils.text;

public enum TextOptions {

    KEEP_COLORS, KEEP_HOVER, KEEP_CLICK, KEEP_EVENTS, ALL, NONE

}
