package dev.perryplaysmc.utils.text.json.command;

import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashMap;

public class JsonHandler implements Listener {

    private static HashMap<String, Run> commands = new HashMap<>();

    public static Run getCommand(String label) {
        return commands.get(label);
    }

    public static void addCommand(String label, Run r) {
        commands.put(label, r);
    }

    public static HashMap<String, Run> getCommands() {
        return commands;
    }

    @EventHandler
    void onCommand(AsyncPlayerChatEvent e) {
        String msg = StringUtils.ColorUtil.removeColor(e.getMessage());
        if(getCommand(msg.split(" ")[0]) != null) {
            e.setCancelled(true);
            getCommand(msg.split(" ")[0]).run(SuperAPI.getUser(e.getPlayer()),
                    e.getMessage().substring(msg.split(" ")[0].length()).split(" "));
            e.setMessage("");
        }
    }
}
