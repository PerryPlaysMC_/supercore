package dev.perryplaysmc.utils.nmsUtils;

import dev.perryplaysmc.core.Log;
import dev.perryplaysmc.utils.nmsUtils.classes.*;
import dev.perryplaysmc.utils.nmsUtils.classes.versionUtil.Version;
import lombok.Getter;
import org.bukkit.Bukkit;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.nmsUtils
 * Class: NMSMain
 * <p>
 * Path: dev.perryplaysmc.utils.nmsUtils.NMSMain
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class NMSMain {

    @Getter
    private static PacketUtil packetUtil;
    @Getter
    private static JsonMessager messager;
    @Getter
    private static EntityHider entityHider;
    @Getter
    private static InventoryCreate inventoryCreate;
    @Getter
    private static PlayerDataLoader playerDataLoader;
    @Getter
    private static InventoryConverter inventoryConverter;
    @Getter
    private static ItemDataUtilHelper itemDataUtilHelper;
    @Getter
    private static ItemStackJSONConverter itemStackJSONConverter;

    public static void load() {
        String pack = Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.') + 1).replaceFirst("v", "");
        boolean isSupported = true;
        long start = System.currentTimeMillis();
        System.out.println("Loading PacketData for version " + version);
        if (true) {
            try {
                Class handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.jsonMessager.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".JsonMessager_v" + version);
                if (handler != null)
                    messager = (JsonMessager) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.packet.PacketUtil.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".PacketUtil_v" + version);
                if (handler != null)
                    packetUtil = (PacketUtil) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.entityHider.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".EntityHider_v" + version);
                if (handler != null)
                    entityHider = (EntityHider) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.inventoryCreator.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".Inventory_v" + version);
                if (handler != null)
                    inventoryCreate = (InventoryCreate) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.playerDataLoader.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".PlayerData_v" + version);
                if (handler != null)
                    playerDataLoader = (PlayerDataLoader) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.itemUtil.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".ItemUtil_v" + version);
                if (handler != null)
                    itemDataUtilHelper = (ItemDataUtilHelper) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.inventoryConverter.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".InventoryConverter_v" + version);
                if (handler != null)
                    inventoryConverter = (InventoryConverter) handler.newInstance();
                handler = Class.forName("dev.perryplaysmc.utils.nmsUtils.classes.handlers.itemStackConverter.v" +
                        version.substring(0, version.lastIndexOf('_'))
                        + ".ItemStackConverter_v" + version);
                if (handler != null)
                    itemStackJSONConverter = (ItemStackJSONConverter) handler.newInstance();
                Log.complete("Loaded PacketHelpers for " + version);
            } catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
                e.printStackTrace();
                Log.error("Could not load PacketHelpers for version " + version);
            }
            return;
        }
    }

}
