package dev.perryplaysmc.utils.nmsUtils.classes;

import org.bukkit.inventory.ItemStack;

public interface ItemStackJSONConverter {


    String convertItemStack(ItemStack item);


}
