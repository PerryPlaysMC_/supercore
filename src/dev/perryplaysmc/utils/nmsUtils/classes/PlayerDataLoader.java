package dev.perryplaysmc.utils.nmsUtils.classes;

import com.sun.istack.internal.NotNull;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public interface PlayerDataLoader {


    Player loadPlayer(@NotNull final OfflinePlayer offline);

}
