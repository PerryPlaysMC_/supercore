package dev.perryplaysmc.utils.nmsUtils.classes;

import dev.perryplaysmc.utils.inventory.CustomInventory;
import org.bukkit.inventory.Inventory;

public interface InventoryConverter {
    
    
    
    
    CustomInventory convertInventory(Inventory inv);
    
    
    
    
    
}
