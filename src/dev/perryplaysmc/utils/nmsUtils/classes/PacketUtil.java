package dev.perryplaysmc.utils.nmsUtils.classes;

import net.minecraft.server.v1_8_R1.EnumPlayerInfoAction;
import org.bukkit.entity.Player;

import java.util.List;

public interface PacketUtil {
    
    
    
    
    void PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo_Action action, List<Player> sendTo, Player... playersToUpdate);
    
    
    
    
    enum PacketPlayOutPlayerInfo_Action {
        UPDATE_DISPLAY_NAME,
        UPDATE_LATENCY,
        ADD_PLAYER,
        REMOVE_PLAYER,
        UPDATE_GAME_MODE;
    }
    
}
