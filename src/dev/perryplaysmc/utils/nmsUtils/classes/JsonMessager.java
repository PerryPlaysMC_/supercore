package dev.perryplaysmc.utils.nmsUtils.classes;

import org.bukkit.entity.Player;

public interface JsonMessager {

    void sendMessage(Player player, ChatType type, String json);
    
    enum ChatType {
        ACTION_BAR(2), NORMAL(1);
    
        byte type;
        ChatType(int i) {
            type=(byte)i;
        }
        
        public byte getType() {
            return type;
        }
    }

}
