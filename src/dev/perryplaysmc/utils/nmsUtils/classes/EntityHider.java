package dev.perryplaysmc.utils.nmsUtils.classes;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/5/19-2023
 * Package: me.perryplaysmc.base.user.handlers
 * Path: me.perryplaysmc.base.user.handlers.EntityHider
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/
public interface EntityHider {
    
    void hideEntity(Player p, Entity entity);
    
    void showEntity(Player p, Entity entity);
    
}
