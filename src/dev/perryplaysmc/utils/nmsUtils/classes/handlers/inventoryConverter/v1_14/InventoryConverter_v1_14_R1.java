package dev.perryplaysmc.utils.nmsUtils.classes.handlers.inventoryConverter.v1_14;

import dev.perryplaysmc.utils.nmsUtils.classes.InventoryConverter;
import dev.perryplaysmc.utils.inventory.CustomInventory;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftInventory;
import org.bukkit.inventory.Inventory;

public class InventoryConverter_v1_14_R1 implements InventoryConverter {
    @Override
    public CustomInventory convertInventory(Inventory inv) {
        CraftInventory inv2 = (CraftInventory) inv;
        String name = inv2.getViewers().stream().findFirst().map(e -> e.getOpenInventory().getTitle()).orElse("");
        return new CustomInventory(inv2.getHolder(), inv2.getSize(), name);
    }
}
