package dev.perryplaysmc.utils.nmsUtils.classes.handlers.itemStackConverter.v1_11;

import dev.perryplaysmc.utils.nmsUtils.classes.ItemStackJSONConverter;
import net.minecraft.server.v1_11_R1.NBTTagCompound;
import org.bukkit.craftbukkit.v1_11_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

public class ItemStackConverter_v1_11_R1 implements ItemStackJSONConverter {

    @Override
    public String convertItemStack(ItemStack item) {
        net.minecraft.server.v1_11_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = new NBTTagCompound();
        compound = nmsItemStack.save(compound);
        return compound.toString();
    }
}
