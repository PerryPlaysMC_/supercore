package dev.perryplaysmc.utils.nmsUtils.classes.handlers.playerDataLoader.v1_9;

import com.mojang.authlib.GameProfile;
import dev.perryplaysmc.utils.nmsUtils.classes.PlayerDataLoader;
import net.minecraft.server.v1_9_R2.*;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_9_R2.CraftServer;
import org.bukkit.entity.Player;

public class PlayerData_v1_9_R2 implements PlayerDataLoader {
    @Override
    public Player loadPlayer(OfflinePlayer offline)  {
        if (!offline.hasPlayedBefore()) {
            return null;
        }
        final GameProfile profile = new GameProfile(offline.getUniqueId(), offline.getName());
        final MinecraftServer server = ((CraftServer)Bukkit.getServer()).getServer();
        final EntityPlayer entity = new EntityPlayer(server,  server.getWorldServer(DimensionManager.OVERWORLD.getDimensionID()), profile, new PlayerInteractManager((World)server.getWorldServer(0)));
        final Player target = (Player)entity.getBukkitEntity();
        if (target != null) {
            target.loadData();
        }
        return target;
    }
}
