package dev.perryplaysmc.utils.nmsUtils.classes.handlers.playerDataLoader.v1_15;

import com.mojang.authlib.GameProfile;
import dev.perryplaysmc.utils.nmsUtils.classes.PlayerDataLoader;
import net.minecraft.server.v1_15_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_15_R1.CraftServer;
import org.bukkit.entity.Player;

public class PlayerData_v1_15_R1 implements PlayerDataLoader {
    @Override
    public Player loadPlayer(OfflinePlayer offline) {
        if (!offline.hasPlayedBefore()) {
            return null;
        }
        final GameProfile profile = new GameProfile(offline.getUniqueId(), offline.getName());
        final MinecraftServer server = (MinecraftServer)((CraftServer)Bukkit.getServer()).getServer();
        final EntityPlayer entity = new EntityPlayer(server, server.getWorldServer(DimensionManager.OVERWORLD), profile, new PlayerInteractManager(server.getWorldServer(DimensionManager.OVERWORLD)));
        final Player target = (Player)entity.getBukkitEntity();
        if (target != null) {
            target.loadData();
        }
        return target;
    }
}