package dev.perryplaysmc.utils.nmsUtils.classes.handlers.jsonMessager.v1_9;

import net.minecraft.server.v1_9_R2.IChatBaseComponent;
import net.minecraft.server.v1_9_R2.PacketPlayOutChat;
import dev.perryplaysmc.utils.nmsUtils.classes.JsonMessager;
import org.bukkit.craftbukkit.v1_9_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_v1_9_R2 implements JsonMessager {

    @Override
    public void sendMessage(Player player, ChatType type, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, type.getType());
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
