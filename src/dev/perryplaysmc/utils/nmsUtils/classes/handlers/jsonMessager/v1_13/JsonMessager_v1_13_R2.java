package dev.perryplaysmc.utils.nmsUtils.classes.handlers.jsonMessager.v1_13;

import net.minecraft.server.v1_13_R2.ChatMessageType;
import net.minecraft.server.v1_13_R2.IChatBaseComponent;
import net.minecraft.server.v1_13_R2.PacketPlayOutChat;
import dev.perryplaysmc.utils.nmsUtils.classes.JsonMessager;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_v1_13_R2 implements JsonMessager {

    @Override
    public void sendMessage(Player player, ChatType type, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, ChatMessageType.valueOf(type.name().replace("ACTION_BAR", "GAME_INFO").replace("NORMAL", "CHAT")));
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
