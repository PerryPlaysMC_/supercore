package dev.perryplaysmc.utils.nmsUtils.classes.handlers.jsonMessager.v1_11;

import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.PacketPlayOutChat;
import dev.perryplaysmc.utils.nmsUtils.classes.JsonMessager;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_v1_11_R1 implements JsonMessager {

    @Override
    public void sendMessage(Player player, ChatType type, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, type.getType());
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
