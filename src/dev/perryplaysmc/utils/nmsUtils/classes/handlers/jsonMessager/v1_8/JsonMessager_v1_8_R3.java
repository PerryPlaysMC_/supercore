package dev.perryplaysmc.utils.nmsUtils.classes.handlers.jsonMessager.v1_8;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import dev.perryplaysmc.utils.nmsUtils.classes.JsonMessager;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_v1_8_R3 implements JsonMessager {
    
    @Override
    public void sendMessage(Player player, ChatType type, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, type.getType());
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
