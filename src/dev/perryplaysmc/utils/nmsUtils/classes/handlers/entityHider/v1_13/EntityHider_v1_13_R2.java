package dev.perryplaysmc.utils.nmsUtils.classes.handlers.entityHider.v1_13;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.utils.nmsUtils.classes.EntityHider;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntity;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityDestroy;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 10/5/19-2023
 * Package: dev.perryplaysmc.utils.nmsUtils.classes.handlers.entityHider.v1_8
 * Path: dev.perryplaysmc.utils.nmsUtils.classes.handlers.entityHider.v1_8.EntityHider_v1_8_R1
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class EntityHider_v1_13_R2 implements EntityHider {
    
    @Override
    public void hideEntity(Player p, Entity entity) {
        (new BukkitRunnable() {
            @Override
            public void run() {
                PacketPlayOutEntityDestroy ppoed = new PacketPlayOutEntityDestroy(entity.getEntityId());
                ((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoed);
            }
        }).runTaskLater(Core.getInstance(), 50);
    }
    
    @Override
    public void showEntity(Player p, Entity entity) {
        (new BukkitRunnable() {
            @Override
            public void run() {
                PacketPlayOutEntity ppoed = new PacketPlayOutEntity(entity.getEntityId());
                ((CraftPlayer)p).getHandle().playerConnection.sendPacket(ppoed);
            }
        }).runTaskLater(Core.getInstance(), 50);
    }
}
