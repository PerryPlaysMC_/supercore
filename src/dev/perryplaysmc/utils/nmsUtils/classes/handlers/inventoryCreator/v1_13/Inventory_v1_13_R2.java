package dev.perryplaysmc.utils.nmsUtils.classes.handlers.inventoryCreator.v1_13;

import dev.perryplaysmc.utils.nmsUtils.classes.InventoryCreate;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftInventoryPlayer;
import org.bukkit.entity.HumanEntity;
import org.bukkit.inventory.PlayerInventory;

public class Inventory_v1_13_R2 implements InventoryCreate {
    @Override
    public PlayerInventory createInventory(HumanEntity base, String name) {
        return new CraftInventoryPlayer(new net.minecraft.server.v1_13_R2.PlayerInventory(null));
    }
}
