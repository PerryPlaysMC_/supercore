package dev.perryplaysmc.utils.nmsUtils.classes.handlers.itemUtil.v1_15;


import dev.perryplaysmc.utils.inventory.NBTTag;
import dev.perryplaysmc.utils.inventory.TagType;
import dev.perryplaysmc.utils.nmsUtils.classes.ItemDataUtilHelper;
import net.minecraft.server.v1_15_R1.*;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("all")
public class ItemUtil_v1_15_R1 implements ItemDataUtilHelper {
    
    HashMap<String, NBTTag> data, prev;
    
    @Override
    public HashMap<String, NBTTag> getData(ItemStack stack) {
        data = new HashMap<>();
        prev = new HashMap<>();
        net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
        NBTTagCompound d = nmsStack.hasTag() ? nmsStack.getTag() : new NBTTagCompound();
        if(d.getKeys()!=null && d.getKeys().size()>0)
            for(String key : d.getKeys()) {
                NBTBase a = d.get(key);
                if(a instanceof NBTTagByte) data.put(key, new NBTTag(TagType.BYTE, d.getByte(key)));
                if(a instanceof NBTTagString) data.put(key, new NBTTag(TagType.STRING, d.getString(key)));
                if(a instanceof NBTTagByteArray) data.put(key, new NBTTag(TagType.BYTE_ARRAY, d.getByteArray(key)));
                if(a instanceof NBTTagShort) data.put(key, new NBTTag(TagType.SHORT, d.getShort(key)));
                if(a instanceof NBTTagDouble) data.put(key, new NBTTag(TagType.DOUBLE, d.getDouble(key)));
                if(a instanceof NBTTagIntArray) data.put(key, new NBTTag(TagType.INT_ARRAY, d.getIntArray(key)));
                if(a instanceof NBTTagInt) data.put(key, new NBTTag(TagType.INT, d.getInt(key)));
                if(a instanceof NBTTagLong) data.put(key, new NBTTag(TagType.LONG, d.getLong(key)));
                if(a instanceof NBTTagFloat) data.put(key, new NBTTag(TagType.FLOAT, d.getFloat(key)));
            }
        return data;
    }
    @Override
    public ItemStack damage(Player player, ItemStack itemStack, int amount) {
        org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer cp =
                (org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer) player;
        net.minecraft.server.v1_15_R1.ItemStack item = CraftItemStack.asNMSCopy(itemStack);
        item.damage(amount, cp.getHandle(), null);
        return CraftItemStack.asBukkitCopy(item);
    }
    @Override
    public void setData(HashMap<String, NBTTag> newData) {
        prev = data;
        this.data = newData;
    }
    
    @Override
    public ItemStack clearData(ItemStack stack) {
        net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(stack);
        NBTTagCompound d = new NBTTagCompound();
        nmsStack.setTag(d);
        stack = CraftItemStack.asBukkitCopy(nmsStack);
        return stack;
    }
    
    @Override
    public ItemStack finish(ItemStack item) {
        net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound d = nmsStack.getOrCreateTag();
        if(data.size() > 0) {
            for (Map.Entry<String, NBTTag> key : data.entrySet()) {
                switch (key.getValue().getType()) {
                    case INT:
                        d.set(key.getKey(), NBTTagInt.a((Integer) key.getValue().getValue()));
                        break;
                    case DOUBLE:
                        d.set(key.getKey(), NBTTagDouble.a((double) key.getValue().getValue()));
                        break;
                    case STRING:
                        d.set(key.getKey(), NBTTagString.a((String) key.getValue().getValue()));
                        break;
                    case BOOLEAN:
                        d.set(key.getKey(), NBTTagByte.a((byte) ((boolean) key.getValue().getValue() ? 1 : 0)));
                        break;
                    case BYTE:
                        d.set(key.getKey(), NBTTagByte.a((Byte) key.getValue().getValue()));
                        break;
                    case BYTE_ARRAY:
                        d.set(key.getKey(), new NBTTagByteArray((byte[]) key.getValue().getValue()));
                        break;
                    case FLOAT:
                        d.set(key.getKey(), NBTTagFloat.a((Float) key.getValue().getValue()));
                        break;
                    case INT_ARRAY:
                        d.set(key.getKey(), new NBTTagIntArray((int[]) key.getValue().getValue()));
                        break;
                    case LONG:
                        d.set(key.getKey(), NBTTagLong.a((Long) key.getValue().getValue()));
                        break;
                    case SHORT:
                        d.set(key.getKey(), NBTTagShort.a((Short) key.getValue().getValue()));
                        break;
                }
            }
            nmsStack.setTag(d);
            item = CraftItemStack.asBukkitCopy(nmsStack);
        }
        return item;
    }
}
