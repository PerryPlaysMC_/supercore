package dev.perryplaysmc.utils.nmsUtils.classes.handlers.packet.PacketUtil.v1_9;

import dev.perryplaysmc.utils.nmsUtils.classes.PacketUtil;
import net.minecraft.server.v1_9_R1.EntityPlayer;
import net.minecraft.server.v1_9_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_9_R1.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.List;

@SuppressWarnings("all")
public class PacketUtil_v1_9_R1 implements PacketUtil {
    
    
    @Override
    public void PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo_Action action, List<Player> sendTo, Player... playersToUpdate) {
        EnumPlayerInfoAction a = EnumPlayerInfoAction.valueOf(action.name());
    
        EntityPlayer[] players = new EntityPlayer[playersToUpdate.length];
        
        for(int i = 0; i < players.length; i++) {
            CraftPlayer p = (CraftPlayer)playersToUpdate[i];
            players[i] =  p.getHandle();
        }
        
        PacketPlayOutPlayerInfo info = new PacketPlayOutPlayerInfo(a, players);
        
        for(Player op : sendTo) {
            CraftPlayer p = (CraftPlayer)op;
            p.getHandle().playerConnection.sendPacket(info);
        }
    }
}
