package dev.perryplaysmc.utils.inventory;

import dev.perryplaysmc.enchantment.EnchantHandler;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

@SuppressWarnings("all")
public class EnchantUtil {
    
    public static ItemMeta addEnchantment(ItemStack stack, Enchantment ench, int lvl) {
        ItemEnchant i = new ItemEnchant(stack.clone());
        i.addEnchant(ench, lvl);
        return i.finish().getItemMeta();
    }
    
    public static ItemMeta reloadEnchants(ItemStack item) {
        ItemMeta im = item.getItemMeta();
        List<String> enchants = new ArrayList<>();
        List<String> defaultLore = new ArrayList<>();
        if(im!=null&&im.hasEnchants())
            for(Enchantment e : im.getEnchants().keySet()) {
                String toAdd = "";
                if(im.getEnchantLevel(e)>1||e.getMaxLevel()>1)
                    toAdd=("§7"+fixName(e) + " " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
                else toAdd=("§7"+fixName(e));
                if(!enchants.contains(toAdd))enchants.add(toAdd);
            }
        if(enchants.size()>0)
            enchants.add(" ");
        
        if(im.hasLore())
            for(String s : im.getLore()) {
                if(getEnchantFromLore(im, s) > -1) continue;
                defaultLore.add(s);
            }
        defaultLore.removeAll(enchants);
        enchants.addAll(defaultLore);
        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        im.setLore(enchants);
        return im;
    }
    
    public static ItemMeta fixEnchantments(ItemStack item) {
        HashMap<Enchantment, Integer> enchs = new HashMap<>();
        for(Enchantment c : item.getEnchantments().keySet()) {
            enchs.put(c, item.getEnchantmentLevel(c));
            item.removeEnchantment(c);
        }
        for(Map.Entry<Enchantment, Integer> i : enchs.entrySet())  {
            item.setItemMeta(addEnchantment(item, i.getKey(), i.getValue()));
        }
        for(Enchantment g : getEnchants(item.getItemMeta()).keySet()) {
            item.setItemMeta(removeEnchant(item, g));
        }
        ItemMeta itemMeta = item.getItemMeta();
        
        
        return itemMeta;
    }
    
    
    public static ItemStack removeEnchants(ItemStack item) {
        ItemEnchant i = new ItemEnchant(item.clone());
        for(Enchantment c : i.getEnchants().keySet()) {
            i.removeEnchant(c);
        }
        return i.finish();
    }
    
    public static ItemMeta removeAllEnchants(ItemStack item) {
        HashMap<Enchantment, Integer> enchs = new HashMap<>();
        for(Enchantment c : getEnchants(item.getItemMeta()).keySet()) {
            item.removeEnchantment(c);
            item.setItemMeta(removeEnchant(item, c));
        }
        ItemMeta itemMeta = item.getItemMeta();
        
        
        return itemMeta;
    }
    
    public static ItemMeta removeAllEnchantments(ItemStack item) {
        if(item==null) return null;
        List<Enchantment> ec = new ArrayList<>();
        ec.addAll(EnchantHandler.getEnchantments());
        ec.addAll(Arrays.asList(Enchantment.values()));
        List<String> newLore = new ArrayList<>();
        if(item.hasItemMeta()) {
            if(item.getItemMeta().hasLore()) {
                for(String x : item.getItemMeta().getLore()) {
                    for(Enchantment ench : ec) {
                        String enchantmentName = fixName(ench);
                        if(StringUtils.translate(x).toLowerCase().contains(StringUtils.translate(enchantmentName.toLowerCase()))) {
                            String p2 = x.toLowerCase().split(StringUtils.translate(enchantmentName))[1],
                                    p1 = x.toLowerCase().split(p2)[0] + (ench.getMaxLevel() == 1 & p2 == "" ? "" : " ");
                            if(x.equalsIgnoreCase(p1 + p2)) {
                                continue;
                            }
                        }
                        newLore.add(StringUtils.translate(x));
                    }
                }
            }
        }
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setLore(newLore);
        return itemMeta;
    }
    public static ItemMeta removeEnchant(ItemStack item, Enchantment ench) {
        ItemMeta im = item.getItemMeta();
        List<String> enchants = new ArrayList<>();
        List<String> defaultLore = new ArrayList<>();
        int lvl = im.getEnchantLevel(ench);
        System.out.println("Removed Enchant");
        for(Enchantment e : im.getEnchants().keySet()) {
            String toAdd = "";
            if(im.getEnchantLevel(e)>1||e.getMaxLevel()>1)
                toAdd=("§7"+fixName(e)+" " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
            else toAdd=("§7"+fixName(e));
            if(!enchants.contains(toAdd)) {
                enchants.add(toAdd);
            }
        }
        String toAdd;
        Enchantment e = ench;
        if(im.getEnchantLevel(e)>1||e.getMaxLevel()>1)
            toAdd=("§7"+fixName(e)+" " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
        else toAdd=("§7"+fixName(e));
        if(enchants.contains(toAdd))enchants.remove(toAdd);
        im.removeEnchant(ench);
        System.out.println("Removed Enchant");
        if(im.hasLore())
            for(String s : im.getLore()) {
                defaultLore.add(s);
            }
        defaultLore.removeAll(enchants);
        enchants.addAll(defaultLore);
        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        im.setLore(enchants);
        return im;
    }
    
    
    public static boolean hasEnchant(ItemStack item, Enchantment ench) {
        if(item == null) return false;
        return getEnchant(item, ench) > -1 || item.getItemMeta().hasEnchant(ench);
    }
    
    public static int getEnchant(ItemStack i, Enchantment ench) {
        ItemMeta im = i.getItemMeta();
        if(im.hasEnchant(ench)) {
            return im.getEnchantLevel(ench);
        }
        String f = StringUtils.ColorUtil.removeColor(fixName(ench)).toLowerCase();
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    if(StringUtils.ColorUtil.removeColor(x).toLowerCase().contains(f)) {
                        String p2 = x.toLowerCase().split(StringUtils.ColorUtil.removeColor(f))[1],
                                p1 = x.toLowerCase().split(p2)[0];
                        if(x.equalsIgnoreCase(p1 + p2)) {
                            if(p2.replace(" ", "") == "") {
                                im.addEnchant(ench, 1, true);
                                return 1;
                            }
                            im.addEnchant(ench, NumberUtil.intToRoman(p2.replace(" ", "")), true);
                            i.setItemMeta(im);
                            return NumberUtil.intToRoman(p2.replace(" ", ""));
                        }
                    }
                }
            }
        }catch(Exception e) {
            
        }
        return -1;
    }
    
    
    
    public static int getEnchantFromLore(ItemMeta im, String ench) {
        String f = StringUtils.ColorUtil.removeColor(ench).split(" ")[0];
        if(ench.contains(" ")) {
            for(String s : StringUtils.ColorUtil.removeColor(ench).split(" ")) {
                if(NumberUtil.intToRoman2(s)==-1&&!StringUtils.ColorUtil.removeColor(s).equalsIgnoreCase(StringUtils.ColorUtil.removeColor(ench).split(" ")[0]))f+=" "+s;
            }
        }
        f=StringUtils.ColorUtil.removeColor(f);
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    if(StringUtils.ColorUtil.removeColor(x).toLowerCase().contains(f.toLowerCase())) {
                        x=StringUtils.ColorUtil.removeColor(x);
                        String p2 = x.toLowerCase().split(StringUtils.ColorUtil.removeColor(f.toLowerCase()))[1],
                                p1 = x.toLowerCase().split(p2)[0]+" ";
                        if(x.equalsIgnoreCase(p1 + p2.replace(" ", ""))) {
                            return NumberUtil.intToRoman2(p2.replace(" ", ""));
                        }
                    }
                }
            }
        }catch(Exception e) {
        
        }
        return -1;
    }
    
    public static Map<Enchantment, Integer> getEnchants(ItemMeta im) {
        HashMap<Enchantment, Integer> enchants = new HashMap<>();
        List<Enchantment> ec = new ArrayList<>();
        ec.addAll(EnchantHandler.getEnchantments());
        ec.addAll(Arrays.asList(Enchantment.values()));
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    for(Enchantment ench : ec)
                        if(StringUtils.translate(x).toLowerCase().contains(StringUtils.translate(ench.getName().toLowerCase()))) {
                            String p2 = x.toLowerCase().split(StringUtils.translate(fixName(ench).toLowerCase()))[1],
                                    p1 = x.toLowerCase().split(p2)[0];
                            if(x.equalsIgnoreCase(p1 + p2)) {
                                enchants.put(ench, NumberUtil.intToRoman(StringUtils.ColorUtil.removeColor(p2.replace(" ", ""))));
                            }
                        }
                }
            }
        }catch(Exception e) {
            
        }
        return enchants.isEmpty() & im.hasEnchants() ? im.getEnchants() : enchants;
    }
    
    public static String fixName(boolean color, Enchantment ench) {
        String name = ench.getName();
        if(EnchantHandler.getEnchantment(name)!=null) return name;
        if(name.toLowerCase().contains("binding_curse")) {
            name = "Curse of Binding";
        }
        if(name.toLowerCase().contains("vanishing_curse")) {
            name = "Curse of Vanishing";
        }
        if(name.toLowerCase().contains("dig_speed")) {
            name = "Efficiency";
        }
        if(name.toLowerCase().contains("arrow_damage")) {
            name = "Power";
        }
        if(name.toLowerCase().contains("arrow_knockback")) {
            name = "Punch";
        }
        if(name.toLowerCase().contains("arrow_fire")) {
            name = "Flame";
        }
        if(name.toLowerCase().contains("arrow_infinite")) {
            name = "Infinity";
        }
        if(name.toLowerCase().contains(Enchantment.DAMAGE_UNDEAD.getName().toLowerCase())) {
            name = "Smite";
        }
        if(name.toLowerCase().contains(Enchantment.DAMAGE_ALL.getName().toLowerCase())) {
            name = "Sharpness";
        }
        if(name.toLowerCase().contains(Enchantment.DAMAGE_ARTHROPODS.getName().toLowerCase())) {
            name = "Bane of Arthropods";
        }
        if(name.toLowerCase().contains(Enchantment.DURABILITY.getName().toLowerCase())) {
            name = "Unbreaking";
        }
        if(name.toLowerCase().contains(Enchantment.LOOT_BONUS_BLOCKS.getName().toLowerCase())) {
            name = "Fortune";
        }
        if(name.toLowerCase().contains(Enchantment.LOOT_BONUS_MOBS.getName().toLowerCase())) {
            name = "Looting";
        }
        if(name.toLowerCase().contains(Enchantment.OXYGEN.getName().toLowerCase())) {
            name = "Respiration";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_ENVIRONMENTAL.getName().toLowerCase())) {
            name = "Protection";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_EXPLOSIONS.getName().toLowerCase())) {
            name = "Blast Protection";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_FALL.getName().toLowerCase())) {
            name = "Feather Falling";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_FIRE.getName().toLowerCase())) {
            name = "Fire Protection";
        }
        if(name.toLowerCase().contains(Enchantment.PROTECTION_PROJECTILE.getName().toLowerCase())) {
            name = "Projectile Protection";
        }
        if(name.toLowerCase().contains(Enchantment.WATER_WORKER.getName().toLowerCase())) {
            name = "Aqua Affinity";
        }
        if(name.toLowerCase().contains(Enchantment.LUCK.getName().toLowerCase())) {
            name = "Luck of the Sea";
           return color?"§7"+name:name;
        }
        String n = StringUtils.getNameFromCaps(name);
        return color?"§7"+n:n;
    }
    
    public static String fixName(Enchantment ench) {
        return fixName(false, ench);
    }
    
    
}
