package dev.perryplaysmc.utils.inventory;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

public class CustomInventory {
    
    private org.bukkit.inventory.Inventory inv;
    private String name;
    
    public CustomInventory(InventoryHolder owner, int size, String name) {
        this.inv = Bukkit.createInventory(owner, size, name);
        this.name = name;
    }
    
    public CustomInventory(int size, String name) {
        this.inv = Bukkit.createInventory(null, size, name);
        this.name = name;
    }
    
    public CustomInventory(InventoryType type, String name) {
        this.inv = Bukkit.createInventory(null, type, name);
        this.name = name;
    }
    
    public CustomInventory(InventoryView v) {
        this.inv = v.getTopInventory() == null ? v.getBottomInventory() : v.getTopInventory();
        this.name = v.getTitle();
    }
    
    public String getName() {
        return name;
    }
    
    public org.bukkit.inventory.Inventory getInventory() {
        return inv;
    }
    
    public void setItem(int index, ItemStack item) {
        inv.setItem(index, item);
    }
    
    public ListIterator<ItemStack> iterator() {
        return inv.iterator();
    }
    
    public ListIterator<ItemStack> iterator(int i) {
        return inv.iterator(i);
    }
    
    public void setContents(ItemStack[] items) {
        inv.setContents(items);
    }
    
    public void setStorageContents(ItemStack[] items) {
        inv.setStorageContents(items);
    }
    
    public int getMaxStackSize() {
        return inv.getMaxStackSize();
    }
    
    public void setMaxStackSize(int max) {
        inv.setMaxStackSize(max);
    }
    
    public void clear() {
        inv.clear();
    }
    
    public void clear(int i) {
        inv.clear(i);
    }
    
    public HashMap<Integer, ? extends ItemStack> all(Material m) {
        return inv.all(m);
    }
    
    public HashMap<Integer, ? extends ItemStack> all(ItemStack m) {
        return inv.all(m);
    }
    
    public List<Player> getViewersAsPlayers() {
        List<Player> pl = new ArrayList<>();
        for(HumanEntity h : getViewers()) pl.add((Player)h);
        return pl;
    }
    
    public List<HumanEntity> getViewers() {
        return inv.getViewers();
    }
    
    public void remove(Material mat) {
        inv.remove(mat);
    }
    
    public void remove(ItemStack item) {
        inv.remove(item);
    }
    
    public int first(Material mat) {
        return inv.first(mat);
    }
    
    public int first(ItemStack i) {
        return inv.first(i);
    }
    
    public int firstEmpty() {
        return inv.firstEmpty();
    }
    
    public boolean containsAtLeast(ItemStack i, int amount) {
        return inv.containsAtLeast(i, amount);
    }
    
    public boolean contains(ItemStack i) {
        return inv.contains(i);
    }
    
    public boolean contains(Material i) {
        return inv.contains(i);
    }
    
    public boolean contains(ItemStack i, int amount) {
        return inv.contains(i, amount);
    }
    
    public boolean contains(Material i, int amount) {
        return inv.contains(i, amount);
    }
    
    public ItemStack getItem(int index) {
        return inv.getItem(index);
    }
    
    public int getSize() {
        return inv.getSize();
    }
    
    public ItemStack[] getContents() {
        return inv.getContents();
    }
    
    public ItemStack[] getStorageContents() {
        return inv.getStorageContents();
    }
    
    public HashMap<Integer, ItemStack> removeItem(ItemStack... items) {
        return inv.removeItem(items);
    }
    
    public HashMap<Integer, ItemStack> addItem(ItemStack... items) {
        return inv.addItem(items);
    }
    
    
    
}
