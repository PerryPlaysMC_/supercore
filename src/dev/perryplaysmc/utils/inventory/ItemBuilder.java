package dev.perryplaysmc.utils.inventory;

import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.configuration.ConfigManager;
import dev.perryplaysmc.enchantment.EnchantHandler;
import dev.perryplaysmc.utils.nmsUtils.NMSMain;
import dev.perryplaysmc.utils.nmsUtils.classes.ItemDataUtilHelper;
import dev.perryplaysmc.utils.nmsUtils.classes.versionUtil.Version;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.inventory
 * Class: ItemUtil
 * <p>
 * Path: dev.perryplaysmc.utils.inventory.ItemUtil
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ItemBuilder {

    List<String> enchants, defaultLore, doNOTADD;
    HashMap<Enchantment, Integer> enchs;
    HashMap<String, NBTTag> data;
    ItemDataUtilHelper h;
    ItemStack item;
    ItemMeta im;
    static ItemUtility util;
    static List<Material> woods, leaves;
    static HashMap<String, ItemStack> materials = new HashMap<>();
    static List<String> str = new ArrayList<>();


    public ItemBuilder() {
        util = new ItemUtility();
        woods = new ArrayList<>();
        leaves = new ArrayList<>();
        for(Material m : Material.values()) {
            if(!str.contains(m.name()))
                str.add(m.name());
        }
        if(Version.isCurrentHigher(Version.v1_12)) {
            woods.add(Material.ACACIA_LOG);
            woods.add(Material.BIRCH_LOG);
            woods.add(Material.DARK_OAK_LOG);
            woods.add(Material.BIRCH_LOG);
            woods.add(Material.JUNGLE_LOG);
            woods.add(Material.OAK_LOG);
            woods.add(Material.SPRUCE_LOG);

            woods.add(Material.STRIPPED_ACACIA_LOG);
            woods.add(Material.STRIPPED_BIRCH_LOG);
            woods.add(Material.STRIPPED_DARK_OAK_LOG);
            woods.add(Material.STRIPPED_BIRCH_LOG);
            woods.add(Material.STRIPPED_JUNGLE_LOG);
            woods.add(Material.STRIPPED_OAK_LOG);
            woods.add(Material.STRIPPED_SPRUCE_LOG);

            leaves.add(Material.ACACIA_LEAVES);
            leaves.add(Material.BIRCH_LEAVES);
            leaves.add(Material.DARK_OAK_LEAVES);
            leaves.add(Material.JUNGLE_LEAVES);
            leaves.add(Material.OAK_LEAVES);
            leaves.add(Material.SPRUCE_LEAVES);
        }else {
            woods.add(convertMaterial("LOG"));
            woods.add(convertMaterial("LOG_2"));

            leaves.add(convertMaterial("LEAVES"));
            leaves.add(convertMaterial("LEAVES_2"));
        }
        File f = new File(Config.defaultDirectory, "items.cvs");
        for(String s : ConfigManager.readFile(f)) {
            //#item,id,metadata
            if(s.startsWith("#")) continue;
            String[] sp = s.split(",");
            if(sp.length == 3) {
                Material id = byId(NumberUtil.getInt(sp[1]));
                //public ItemStack(@NotNull Material type, int amount, short damage) {
                if(id!=null&&!materials.containsKey(sp[0].toLowerCase())) {
                    int i = NumberUtil.getInt(sp[2]);
                    materials.put(sp[0].toLowerCase(), new ItemStack(id, 1, (short)i));
                }
            }
        }
    }

    public static Material byId(int id) {
        for(Material c : Material.values()) {
            if(Version.isCurrentHigher(Version.v1_12)) {
                if(Bukkit.getUnsafe().toLegacy(c).getId() == id) return c;
            }else {
                if(c.getId() == id) return c;
            }
        }
        return null;
    }

    public String getDisplayName() {
        return im.getDisplayName();
    }

    public enum ColorType {
        WHITE(0),
        ORANGE(1),
        MAGENTA(2),
        LIGHT_BLUE(3),
        YELLOW(4),
        LIME(5),
        PINK(6),
        GRAY(7),
        LIGHT_GRAY(8),
        CYAN(9),
        PURPLE(10),
        BLUE(11),
        BROWN(12),
        GREEN(13),
        RED(14),
        BLACK(15);

        short data;
        ColorType(int data) {
            this.data = (short)data;
        }

        public short getData() {
            return data;
        }

        public static ColorType random() {
            return values()[new Random().nextInt(values().length)];
        }
        public static ColorType random(ColorType... types) {
            return types[new Random().nextInt(types.length)];
        }

    }

    private ItemBuilder(String material) {
        this(findMaterial(material, 1));
    }

    private ItemBuilder(Material material) {
        this(new ItemStack(convertMaterial(material)));
    }

    private ItemBuilder(ColorType color, boolean isPane) {
        this(Version.isCurrentHigher(Version.v1_12) ?
                new ItemStack(Material.getMaterial(color.name() + "_STAINED_GLASS" + (isPane ? "_PANE" : ""))) :
                new ItemStack(Material.getMaterial("STAINED_GLASS" + (isPane ? "_PANE" : "")), 1, color.getData()));
    }
    private ItemBuilder(ColorType color) {
        this(Version.isCurrentHigher(Version.v1_12) ?
                new ItemStack(Material.getMaterial(color.name() + "_WOOL")) :
                new ItemStack(Material.getMaterial("WOOL"), 1, color.getData()));
    }

    private ItemBuilder(ItemStack item) {
        this.item = item;
        im = item.getItemMeta();
        if(im==null)im= Bukkit.getItemFactory().getItemMeta(item.getType());
        h = NMSMain.getItemDataUtilHelper();
        enchants = new ArrayList<>();
        defaultLore = new ArrayList<>();
        enchs = new HashMap<>();
        doNOTADD = new ArrayList<>();
        if(im!=null&&im.hasEnchants())
            for (Enchantment e : im.getEnchants().keySet()) {
                String toAdd = "";
                if(im.getEnchantLevel(e) > 1 || e.getMaxLevel() > 1)
                    toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
                else toAdd = (EnchantUtil.fixName(true, e));
                if(!enchants.contains(toAdd))
                    enchants.add(toAdd);
                enchs.put(e, im.getEnchantLevel(e));
            }
    }

    public static ItemBuilder createItem(String material) {
        return new ItemBuilder(material);
    }

    public static ItemBuilder createWool(ColorType color) {
        return new ItemBuilder(color);
    }


    public static ItemBuilder createItem(Material material) {
        return new ItemBuilder(material);
    }

    public static ItemBuilder createGlass(ColorType color, boolean isPane) {
        return new ItemBuilder(color, isPane);
    }

    public static ItemBuilder createItem(ItemStack item) {
        return new ItemBuilder(item);
    }

    public ItemBuilder setAmount(int amount) {
        item.setAmount(amount);
        return this;
    }

    public ItemBuilder setName(String name) {
        if(name.isEmpty())name=" ";
        im.setDisplayName(StringUtils.translate(name));
        return this;
    }

    public ItemBuilder setLore(int line, String lore) {
        List<String> newLore = defaultLore;
        newLore.set(line, StringUtils.translate(lore));
        defaultLore = newLore;
        im.setLore(newLore);
        return this;
    }

    public ItemBuilder addLore(String... lore) {
        List<String> newLore = defaultLore;
        for(String s : lore)
            newLore.add(StringUtils.translate(s));
        defaultLore = newLore;
        im.setLore(newLore);
        return this;
    }

    public ItemBuilder setLore(String... lore) {
        List<String> newLore = enchants;
        for(String s : lore) {
            newLore.add(StringUtils.translate(s));
        }
        defaultLore = newLore;
        im.setLore(newLore);
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        List<String> newLore = enchants;
        for(String s : lore) {
            newLore.add(StringUtils.translate(s));
        }
        defaultLore = newLore;
        im.setLore(newLore);
        return this;
    }

    public ItemBuilder setType(Material newType) {
        item.setType(convertMaterial(newType));
        return this;
    }

    public ItemBuilder setType(String newType) {
        item.setType(convertMaterial(newType));
        return this;
    }
    //Data
    public String getNoColorDisplayName() {
        return StringUtils.ColorUtil.removeColor(im.getDisplayName());
    }

    public boolean hasDisplayName() {
        return im.hasDisplayName();
    }

    public NBTTag get(String key) {
        if(getData().containsKey(key)) return getData().get(key);
        return null;
    }
    public String getString(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.STRING) return (String)getData().get(key).getValue();
        return "";
    }
    public double getDouble(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.DOUBLE)return (double) getData().get(key).getValue();
        return 0.0;
    }
    public int getInt(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.INT) return (int)getData().get(key).getValue();
        return 0;
    }
    public boolean getBoolean(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (boolean)getData().get(key).getValue();
        return false;
    }
    public long getLong(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (long)getData().get(key).getValue();
        return 0L;
    }
    public short getShort(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (short) getData().get(key).getValue();
        return 0;
    }
    public byte getByte(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (byte)getData().get(key).getValue();
        return 0;
    }
    public float getFloat(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (float) getData().get(key).getValue();
        return 0;
    }
    public byte[] getByteArray(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (byte[])getData().get(key).getValue();
        return new byte[] {0};
    }
    public int[] getIntArray(String key) {
        if(getData().containsKey(key) && getData().get(key).getType() == TagType.BOOLEAN) return (int[])getData().get(key).getValue();
        return new int[]{0};
    }

    public boolean hasKey(String key) {
        return getData().containsKey(key);
    }

    public ItemBuilder setString(String key, String value) {
        getData().put(key, new NBTTag(TagType.STRING, value));
        return this;
    }
    public ItemBuilder setDouble(String key, double value) {
        getData().put(key, new NBTTag(TagType.DOUBLE, value));
        return this;
    }
    public ItemBuilder setInt(String key, int value) {
        getData().put(key, new NBTTag(TagType.INT, value));
        return this;
    }
    public ItemBuilder setBoolean(String key, boolean value) {
        getData().put(key, new NBTTag(TagType.BOOLEAN, value));
        return this;
    }
    public ItemBuilder setLong(String key, long value) {
        getData().put(key, new NBTTag(TagType.LONG, value));
        return this;
    }
    public ItemBuilder setShort(String key, short value) {
        getData().put(key, new NBTTag(TagType.SHORT, value));
        return this;
    }
    public ItemBuilder setFloat(String key, float value) {
        getData().put(key, new NBTTag(TagType.FLOAT, value));
        return this;
    }

    public ItemBuilder setByteArray(String key, Byte[] value) {
        getData().put(key, new NBTTag(TagType.BYTE_ARRAY, value));
        return this;
    }

    public ItemBuilder setIntArray(String key, Integer[] value) {
        getData().put(key, new NBTTag(TagType.INT_ARRAY, value));
        return this;
    }

    public ItemBuilder removeKey(String key) {
        if(hasKey(key))
            getData().remove(key);
        return this;
    }

    public ItemBuilder addEnchant(Enchantment e, int lvl) {
        if(hasEnchant(e))
            removeEnchant(e);
        im.addEnchant(e, lvl, true);
        String toAdd = "";
        if(lvl > 1 || e.getMaxLevel() > 1)
            toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(lvl));
        else toAdd = (EnchantUtil.fixName(true, e));
        if(!enchants.contains(toAdd))enchants.add(toAdd);
        doNOTADD.add(toAdd);
        enchs.put(e, lvl);
        return this;
    }

    public ItemBuilder removeEnchant(Enchantment e) {
        if(hasEnchant(e)) {
            String toAdd = "";
            if(getEnchant(e) > 1 || e.getMaxLevel() > 1)
                toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(getEnchant(e)));
            else toAdd = (EnchantUtil.fixName(true, e));
            if(enchants.contains(toAdd))enchants.remove(toAdd);
            doNOTADD.add(toAdd);
            im.removeEnchant(e);
            enchs.remove(e);
        }
        return this;
    }

    public ItemBuilder removeAllEnchants() {
        for(Enchantment ench : getEnchants().keySet())
            removeEnchant(ench);
        return this;
    }

    public int getEnchant(Enchantment ench) {
        if(im.hasEnchant(ench)) {
            return im.getEnchantLevel(ench);
        }
        String f = StringUtils.ColorUtil.removeColor(EnchantUtil.fixName(ench)).toLowerCase();
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    if(StringUtils.ColorUtil.removeColor(x).toLowerCase().contains(f)) {
                        String p2 = x.toLowerCase().split(StringUtils.ColorUtil.removeColor(f))[1],
                                p1 = x.toLowerCase().split(p2)[0];
                        if(x.equalsIgnoreCase(p1 + p2)) {
                            if(p2.replace(" ", "") == "") {
                                im.addEnchant(ench, 1, true);
                                return 1;
                            }
                            im.addEnchant(ench, NumberUtil.intToRoman(p2.replace(" ", "")), true);
                            return NumberUtil.intToRoman(p2.replace(" ", ""));
                        }
                    }
                }
            }
        }catch(Exception e) {

        }
        return -1;
    }

    public Map<Enchantment, Integer> getEnchants() {

        HashMap<Enchantment, Integer> enchants = new HashMap<>();
        List<Enchantment> ec = EnchantHandler.getAllEnchantments();
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    for(Enchantment ench : ec)
                        if(getEnchant(ench) > -1) {
                            enchants.put(ench, getEnchant(ench));
                            enchs.put(ench, getEnchant(ench));
                        }
                }
            }
        }catch(Exception e) {

        }
        return enchants.isEmpty() & im.hasEnchants() ? im.getEnchants() : enchants;
    }


    public boolean hasEnchant(Enchantment ench) {
        return im.hasEnchant(ench) || enchs.containsKey(ench);
    }

    public ItemBuilder reload() {
        enchants.clear();
        enchs.clear();
        if(im!=null&&im.hasEnchants())
            for (Enchantment e : im.getEnchants().keySet()) {
                String toAdd = "";
                if(im.getEnchantLevel(e) > 1 || e.getMaxLevel() > 1)
                    toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
                else toAdd = (EnchantUtil.fixName(true, e));
                if(!enchants.contains(toAdd))
                    enchants.add(toAdd);
                doNOTADD.add(toAdd);
                enchs.put(e, im.getEnchantLevel(e));
            }

        return this;
    }

    public ItemBuilder addItemFlags(ItemFlag... flags) {
        im.addItemFlags(flags);
        return this;
    }

    public ItemBuilder removeItemFlags(ItemFlag... flags) {
        im.removeItemFlags(flags);
        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return this;
    }

    public ItemBuilder addAttribute(Attribute attribute, double amount) {
        if(Version.isCurrentHigher(Version.v1_13_R1))
            im.addAttributeModifier(attribute,
                    new AttributeModifier(getAttributeName(attribute), amount,
                            AttributeModifier.Operation.ADD_NUMBER));
        return this;
    }

    public ItemBuilder removeAttribute(Attribute attribute) {
        if(Version.isCurrentHigher(Version.v1_12))
            im.removeAttributeModifier(attribute);
        return this;
    }


    private String getAttributeName(Attribute attribute) {
        String ret = StringUtils.getNameFromCaps(attribute.name()).replace("GENERIC_","");
        ret = (ret.charAt(0)+"").toLowerCase() + ret.substring(1);
        return "generic."+ret;
    }

    public ItemStack buildItem() {
        List<String> newLore = new ArrayList<>(), lore2 = new ArrayList<>();
        for(String s : this.enchants) {
            if(!newLore.contains(s))
                newLore.add(s);
        }
        if(defaultLore!=null)
            for (String l : defaultLore) {
                if(!doNOTADD.contains(l))
                    if(!lore2.contains(l)&&!newLore.contains(l)) {
                        if(StringUtils.hasChars(l))
                            if(EnchantUtil.getEnchantFromLore(im, l)==-1)
                                lore2.add(l);
                    }
            }

        if(lore2.size()>0){
            if(newLore.size()>0)
                newLore.add(" ");
            newLore.addAll(lore2);
        }

        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        im.setLore(newLore);
        item.setItemMeta(im);
        if(data!=null) {
            h.setData(data);
            item = h.finish(item);
        }
        return item;
    }

    public ItemMeta getItemMeta() {
        if(im!=null)return im;
        im = item.getItemMeta();
        if(im==null)im= Bukkit.getItemFactory().getItemMeta(item.getType());
        return im;
    }
    public HashMap<String, NBTTag> getData() {
        if(h==null)
            h = NMSMain.getItemDataUtilHelper();
        if(data == null)
            data = h.getData(item);
        return data;
    }


    public static Material convertMaterial(Material m) {
        if(Version.isCurrentHigher(Version.v1_12)) {
            if(Material.valueOf("LEGACY_" + m.name())!=null)
                return Material.valueOf("LEGACY_" + m.name().replace("SPADE", "SHOVEL"));
            return m;
        }
        if(m.name().startsWith("LEGACY_"))
            return Material.valueOf(m.name().replace("LEGACY_", "").replace("SHOVEL", "SPADE"));
        return m;
    }

    static Material getMaterial(String name) {
        if(name.startsWith("LEGACY_"))
            return Material.matchMaterial(name, true);
        else
            return Material.matchMaterial(name);
    }

    public static ItemStack findMaterial(String line, int amount) {
        if(materials.containsKey(line.toLowerCase())) {
            ItemStack im = materials.get(line.toLowerCase());
            im.setAmount(amount);
            return im;
        }
        for(Material m : Material.values()) {
            if(line.equalsIgnoreCase(m.name())||line.equalsIgnoreCase(m.name().replace("_","")))
                return new ItemStack(m, amount);
        }
        return util.get(line);
    }

    public static Material convertMaterial(String m) {
        if(Version.isCurrentHigher(Version.v1_12)) {
            if(m == null || m.isEmpty()) return null;
            if(getMaterial("LEGACY_"+ m.toUpperCase())!=null)
                return getMaterial("LEGACY_"+m.toUpperCase());
            if(getMaterial(m.toUpperCase())!=null)
                return getMaterial(m.toUpperCase());
            return getMaterial(m.toUpperCase());
        }
        if(m.toUpperCase().startsWith("LEGACY_"))
            return Material.valueOf(m.toUpperCase().replace("LEGACY_", "").replace("SHOVEL", "SPADE"));
        return Material.valueOf(m.toUpperCase());
    }

    public static boolean isAir(Material m) {
        return m.name().endsWith("AIR") && !m.name().endsWith("AIRS");
    }


    public static ItemStack toStack(String material) {
        return new ItemStack(convertMaterial(material));
    }

    public static List<Material> getLeaves() {
        return leaves;
    }

    public static List<Material> getWoods() {
        return woods;
    }

    public static boolean isWood(Material m) {
        return woods.contains(m);
    }

}