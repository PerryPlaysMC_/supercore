package dev.perryplaysmc.utils.inventory.menus;

import dev.perryplaysmc.utils.inventory.ItemBuilder;
import dev.perryplaysmc.utils.inventory.menus.GuiHandler;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GuiUtil {

    private Inventory result;
    private InventoryClick click;
    private InventoryOpen open;
    private InventoryClose close;
    private InventoryDrag drag;
    private String name;
    private int size;

    public GuiUtil(String name, int size) {
        this.name = StringUtils.translate(name);
        this.size = size;
        if(size == 5) {
            result = Bukkit.createInventory(null, InventoryType.HOPPER, StringUtils.translate(name));
        }else
        if(size == 1) {
            result = Bukkit.createInventory(null, InventoryType.DROPPER, StringUtils.translate(name));
        }else
        if(size == 2) {
            result = Bukkit.createInventory(null, InventoryType.DISPENSER, StringUtils.translate(name));
        }else
        result = Bukkit.createInventory(null, size, StringUtils.translate(name));
        GuiHandler.add(this);
    }

    public void remove() {
        GuiHandler.remove(this);
    }

    public void onClick(InventoryClick click) {
        this.click = click;
    }

    public void onOpen(InventoryOpen open) {
        this.open = open;
    }

    public void onClose(InventoryClose close) {
        this.close = close;
    }

    public void onDrag(InventoryDrag drag) {
        this.drag = drag;
    }

    public GuiUtil setItem(int slot, ItemStack stack) {
        result.setItem(slot, stack);
        return this;
    }

    public GuiUtil clear() {
        result.clear();
        return this;
    }

    public GuiUtil setItem(int slot, ItemBuilder stack) {
        result.
                setItem(slot,

                stack.
                        buildItem());
        return this;
    }

    public String getName() {
        return name;
    }

    public InventoryDrag getDrag() {
        return drag;
    }

    public InventoryClose getClose() {
        return close;
    }

    public InventoryOpen getOpen() {
        return open;
    }

    public InventoryClick getClick() {
        return click;
    }

    public Inventory getResult() {
        return result;
    }

    public int size() {
        return size;
    }

    public ItemStack getItem(int i) {
        return result.getItem(i);
    }


    public interface InventoryClick {
        void event(InventoryClickEvent e);
    }

    public interface InventoryOpen {
        void event(InventoryOpenEvent e);
    }

    public interface InventoryClose {
        void event(InventoryCloseEvent e);
    }

    public interface InventoryDrag {
        void event(InventoryDragEvent e);
    }

}
