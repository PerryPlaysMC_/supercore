package dev.perryplaysmc.utils.inventory;

import org.bukkit.Material;

import java.util.HashMap;

public class MaterialHelper {

    static HashMap<Material, Integer> byMaterial = new HashMap<>();
    static HashMap<Integer, Material> byId = new HashMap<>();
    static {
        int current = 0;
        for(Material m : Material.values()) {
            byMaterial.put(m, current);
            byId.put(current, m);
            current++;
        }
    }

    public static Material byId(int id) {
        return byId.get(id);
    }

    public static int getId(Material id) {
        return byMaterial.get(id);
    }

}
