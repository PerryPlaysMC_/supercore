package dev.perryplaysmc.utils.inventory;

import dev.perryplaysmc.enchantment.EnchantHandler;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

/**
 * Copy Right ©
 * This code is private
 * Project: MiniverseRemake
 * Owner: PerryPlaysMC
 * From: 8/8/19-2023
 * <p>
 * 
 Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ItemEnchant {
    
    ItemStack data;
    ItemMeta im;
    List<String> enchants;
    List<String> defaultLore;
    List<String> doNOTADD;
    HashMap<Enchantment, Integer> enchs;
    
    public ItemEnchant(ItemStack data) {
        this.data = data;
        im = data.getItemMeta();
        if(im==null)im= Bukkit.getItemFactory().getItemMeta(data.getType());
        enchants = new ArrayList<>();
        defaultLore = new ArrayList<>();
        enchs = new HashMap<>();
        doNOTADD = new ArrayList<>();
        if(im!=null&&im.hasEnchants())
            for (Enchantment e : im.getEnchants().keySet()) {
                String toAdd = "";
                if(im.getEnchantLevel(e) > 1 || e.getMaxLevel() > 1)
                    toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
                else toAdd = (EnchantUtil.fixName(true, e));
                if(!enchants.contains(toAdd))
                    enchants.add(toAdd);
                enchs.put(e, im.getEnchantLevel(e));
            }
    }
    
    
    public ItemEnchant addEnchant(Enchantment e, int lvl) {
        if(hasEnchant(e))
            removeEnchant(e);
        im.addEnchant(e, lvl, true);
        String toAdd = "";
        if(lvl > 1 || e.getMaxLevel() > 1)
            toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(lvl));
        else toAdd = (EnchantUtil.fixName(true, e));
        if(!enchants.contains(toAdd))enchants.add(toAdd);
        doNOTADD.add(toAdd);
        enchs.put(e, lvl);
        return this;
    }
    
    public ItemEnchant removeEnchant(Enchantment e) {
        if(hasEnchant(e)) {
            String toAdd = "";
            if(getEnchant(e) > 1 || e.getMaxLevel() > 1)
                toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(getEnchant(e)));
            else toAdd = (EnchantUtil.fixName(true, e));
            if(enchants.contains(toAdd))enchants.remove(toAdd);
            doNOTADD.add(toAdd);
            im.removeEnchant(e);
            enchs.remove(e);
        }
        return this;
    }
    
    public int getEnchant(Enchantment ench) {
        if(im.hasEnchant(ench)) {
            return im.getEnchantLevel(ench);
        }
        String f = StringUtils.ColorUtil.removeColor(EnchantUtil.fixName(ench)).toLowerCase();
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    if(StringUtils.ColorUtil.removeColor(x).toLowerCase().contains(f)) {
                        String p2 = x.toLowerCase().split(StringUtils.ColorUtil.removeColor(f))[1],
                                p1 = x.toLowerCase().split(p2)[0];
                        if(x.equalsIgnoreCase(p1 + p2)) {
                            if(p2.replace(" ", "") == "") {
                                im.addEnchant(ench, 1, true);
                                return 1;
                            }
                            im.addEnchant(ench, NumberUtil.intToRoman(p2.replace(" ", "")), true);
                            return NumberUtil.intToRoman(p2.replace(" ", ""));
                        }
                    }
                }
            }
        }catch(Exception e) {
        
        }
        return -1;
    }
    
    public Map<Enchantment, Integer> getEnchants() {
        
        HashMap<Enchantment, Integer> enchants = new HashMap<>();
        List<Enchantment> ec = EnchantHandler.getAllEnchantments();
        try {
            if(im.hasLore()) {
                for(String x : im.getLore()) {
                    for(Enchantment ench : ec)
                        if(getEnchant(ench) > -1) {
                            enchants.put(ench, getEnchant(ench));
                            enchs.put(ench, getEnchant(ench));
                        }
                }
            }
        }catch(Exception e) {
        
        }
        return enchants.isEmpty() & im.hasEnchants() ? im.getEnchants() : enchants;
    }
    
    
    public boolean hasEnchant(Enchantment ench) {
        return im.hasEnchant(ench) || enchs.containsKey(ench);
    }
    
    public ItemEnchant reload() {
        enchants.clear();
        enchs.clear();
        if(im!=null&&im.hasEnchants())
            for (Enchantment e : im.getEnchants().keySet()) {
                String toAdd = "";
                if(im.getEnchantLevel(e) > 1 || e.getMaxLevel() > 1)
                    toAdd = (EnchantUtil.fixName(true, e) + " " + NumberUtil.convertToRoman(im.getEnchantLevel(e)));
                else toAdd = (EnchantUtil.fixName(true, e));
                if(!enchants.contains(toAdd))
                    enchants.add(toAdd);
                doNOTADD.add(toAdd);
                enchs.put(e, im.getEnchantLevel(e));
            }
        
        return this;
    }
    
    public ItemStack finish() {
        List<String> newLore = new ArrayList<>(), lore2 = new ArrayList<>();
        
        for(String s : this.enchants) {
            if(!newLore.contains(s))
                newLore.add(s);
        }
        if(im!=null&&im.hasLore())
            for (String l : im.getLore()) {
                if(!doNOTADD.contains(l))
                    if(!lore2.contains(l)&&!newLore.contains(l)) {
                        if(StringUtils.hasChars(l))
                            if(EnchantUtil.getEnchantFromLore(im, l)==-1)
                                lore2.add(l);
                    }
            }
        
        if(lore2.size()>0){
            if(newLore.size()>0)
                newLore.add(" ");
            newLore.addAll(lore2);
        }
        
        im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        im.setLore(newLore);
        data.setItemMeta(im);
        return data;
    }
    
    
    
}
