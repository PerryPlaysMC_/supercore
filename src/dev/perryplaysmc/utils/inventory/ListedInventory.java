package dev.perryplaysmc.utils.inventory;

import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils.inventory
 * Class: ListedInventory
 * <p>
 * Path: dev.perryplaysmc.utils.inventory.ListedInventory
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ListedInventory {
/*
    private Inventory inv;
    private BoarderType type;
    private List<ItemData> items;

    private ListedInventory(String title, int size) {
        size = (size + (9-(size%9)));
        items = new ArrayList<>();
        inv = Bukkit.createInventory(null, size/9 == 1 ? size*3 : size == 2 ? size+9 : size, StringUtils.translate(title));
    }

    public ListedInventory fillBoarder(BoarderType type) {
        this.type = type;
        switch(type) {
            case GLASS_BLOCK:
                for(int i = 0; i < 9; i++) {
                    inv.setItem(i, new ItemStack(Material.GLASS));
                }
                for(int i = 9; i < inv.getSize(); i+=9) {
                    inv.setItem(i, new ItemStack(Material.GLASS));
                }
                for(int i = 8; i < inv.getSize()-9; i+=9) {
                    inv.setItem(i, new ItemStack(Material.GLASS));
                }
                for(int i = inv.getSize()-9; i < inv.getSize(); i++) {
                    inv.setItem(i, new ItemStack(Material.GLASS));
                }
                break;
            case GLASS_PANE:
                for(int i = 0; i < 9; i++) {
                    inv.setItem(i, new ItemStack(Material.GLASS_PANE));
                }
                for(int i = 9; i < inv.getSize(); i+=9) {
                    inv.setItem(i, new ItemStack(Material.GLASS_PANE));
                }
                for(int i = 8; i < inv.getSize()-9; i+=9) {
                    inv.setItem(i, new ItemStack(Material.GLASS_PANE));
                }
                for(int i = inv.getSize()-9; i < inv.getSize(); i++) {
                    inv.setItem(i, new ItemStack(Material.GLASS_PANE));
                }
                break;
            case STAINED_GLASS_BLOCK:
                for(int i = 0; i < 9; i++) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], false).buildItem());
                }
                for(int i = 9; i < inv.getSize(); i+=9) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], false).buildItem());
                }
                for(int i = 8; i < inv.getSize()-9; i+=9) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], false).buildItem());
                }
                for(int i = inv.getSize()-9; i < inv.getSize(); i++) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], false).buildItem());
                }
                break;
            case STAINED_GLASS_PANE:
                for(int i = 0; i < 9; i++) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], true).buildItem());
                }
                for(int i = 9; i < inv.getSize(); i+=9) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], true).buildItem());
                }
                for(int i = 8; i < inv.getSize()-9; i+=9) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], true).buildItem());
                }
                for(int i = inv.getSize()-9; i < inv.getSize(); i++) {
                    inv.setItem(i, ItemBuilder.createGlass(ItemBuilder.ColorType.values()
                            [new Random().nextInt(ItemBuilder.ColorType.values().length)], true).buildItem());
                }
                break;
        }
        return this;
    }

    private boolean isSet(int slot) {
        for(ItemData item : items) {
            if(item.slot != null && item.slot == slot) return true;
        }
        return false;
    }

    public ListedInventory addItem(ItemStack... items) {
        for(ItemStack item : items) {
            this.items.add(new ItemData(null, item));
        }
        return this;
    }

    public ListedInventory setItem(int slot, ItemStack item) {
        while(isBoarder(slot)) {
            slot++;
            if(!isBoarder(slot)) break;
        }
        items.add(new ItemData(slot, item));
        return this;
    }

    public ListedInventory show(Player viewer) {
        int minSlot = 0;
        int maxSlot = inv.getSize()-1;
        if(this.type != null) {
            minSlot = 10;
            maxSlot = inv.getSize()-10;
        }
        for(int i = minSlot; i < maxSlot; i++) {
            inv.setItem(i, items.get(i));
        }
        return this;
    }

    ItemData getItemData(int slot) {
        for(ItemData item : items) {
            if(item.slot == null) return item;
            if(item.slot == slot) return item;
        }
        return null;
    }

    boolean isBoarder(int slot) {
        if(slot < 9) return true;
        if(slot > inv.getSize()-9) return true;
        if(slot % 9 == 0) return true;
        if(slot % 8 == 0) return true;
        return false;
    }


    enum BoarderType {
        GLASS_BLOCK, GLASS_PANE, STAINED_GLASS_BLOCK, STAINED_GLASS_PANE;
    }

    @AllArgsConstructor
    private class ItemData {
        Integer slot;
        ItemStack stack;
    }

 */

}
