package dev.perryplaysmc.utils;

import dev.perryplaysmc.core.Core;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: Timer
 * <p>
 * Path: dev.perryplaysmc.utils.Timer
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class TimerUtility {

    boolean isCancelled = false;

    public TimerUtility(Runnable run, double delay, double period) {
        double resetDelay = delay;
        double resetPeriod = period;
        (new BukkitRunnable() {
            double del = delay;
            @Override
            public void run() {
                if(del > 0.1) {
                    del -= 0.1;
                }else {
                    if(period > 0)
                        (new BukkitRunnable() {
                            double per = period;
                            @Override
                            public void run() {
                                if(isCancelled) {
                                    cancel();
                                    return;
                                }
                                if(per > 0.1) {
                                    per -= 0.1;
                                }else {
                                    per = resetPeriod;
                                    run.run();
                                }
                            }
                        }).runTaskTimer(Core.getInstance(),0, 2);
                    else run.run();
                    cancel();
                }
            }
        }).runTaskTimer(Core.getInstance(), 0, 2);
    }

    void cancel() {
        isCancelled = true;
    }

}
