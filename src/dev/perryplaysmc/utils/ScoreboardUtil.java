package dev.perryplaysmc.utils;

import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.*;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.utils
 * Class: ScoreboardUtil
 * <p>
 * Path: dev.perryplaysmc.utils.ScoreboardUtil
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class ScoreboardUtil {

    String name, displayName;
    DisplaySlot slot;
    Scoreboard sb;
    Objective obj;

    public ScoreboardUtil(String name) {
        this(name, name);
    }
    public ScoreboardUtil(String name, String displayName) {
        this(name, displayName, DisplaySlot.SIDEBAR);
    }

    public ScoreboardUtil(String name, String displayName, DisplaySlot slot) {
        sb = Bukkit.getScoreboardManager().getNewScoreboard();
        obj = sb.getObjective("display") == null ?
                sb.registerNewObjective(name, "dummy", StringUtils.translate(displayName)) : sb.getObjective("display");
        obj.setDisplaySlot(slot);
        this.name = name;
        this.displayName = displayName;
        this.slot = slot;
    }

    private void registerBoards() {
        sb = Bukkit.getScoreboardManager().getNewScoreboard();
        obj = sb.getObjective("display") == null ?
                sb.registerNewObjective(name, "dummy") : sb.getObjective("display");
        obj.setDisplaySlot(slot);
        obj.setDisplayName(StringUtils.translate(displayName));
        obj.setRenderType(RenderType.INTEGER);
    }


    public void setText(int id, String prefix, String suffix) {
        boolean isNull = sb.getTeam("§" + id + "§r") == null;
        Team t = sb.getTeam("§" + id + "§r") == null ? sb.registerNewTeam("§" + id + "§r") : sb.getTeam("§" + id + "§r");
        if(!t.hasEntry("§" + id + "§r"))
            t.addEntry("§" + id + "§r");
        if(!StringUtils.isEmpty(prefix))
            t.setPrefix(StringUtils.translate(prefix));
        if(!StringUtils.isEmpty(suffix))
            t.setSuffix(StringUtils.translate(suffix));
        if(isNull) {
            Score score = obj.getScore("§" + id + "§r");
            score.setScore(id);
        }
    }

    public void clearScore(int id) {
        Team t = sb.getTeam("§" + id + "§r");
        if(t == null) return;
        t.removeEntry("§" + id + "§r");
        t.unregister();
    }

    public Scoreboard generateScoreboard() {
        return sb;
    }

}
