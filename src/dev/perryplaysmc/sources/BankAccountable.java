package dev.perryplaysmc.sources;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.sources
 * Class: BankAccountable
 * <p>
 * Path: dev.perryplaysmc.sources.BankAccountable
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public interface BankAccountable {

    /**
     * Returns raw balance
     * @return this
     */
    double getBalance();

    /**
     * Returns the formatted balance
     * @return String
     */
    String getFormattedBalance();

    /**
     * True/False play has more money than provided money
     * @param money
     * @return boolean
     */
    boolean hasEnough(double money);

    /**
     * Takes money from the user
     * @param money
     * @return this
     */
    BankAccountable withdraw(double money);

    /**
     * Sets the users balance
     * @param money
     * @return this
     */
    BankAccountable setBalance(double money);

    /**
     * Gives money to the user
     * @param money
     * @return this
     */
    BankAccountable deposit(double money);

    boolean exceedsMin(boolean isSet, double money);

    boolean exceedsMax(boolean isSet, double money);




}
