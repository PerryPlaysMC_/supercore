package dev.perryplaysmc.sources;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@SuppressWarnings("all")
public interface User extends CommandSource, OfflineUser {

    Player getSource();

    default Boolean isPlayer() {
        return true;
    }

    default User getUser() {
        return this;
    }

    Location getLocation();

    void teleport(Location loc);

    void teleport(Entity entity);

    List<String> getTPARequests();

    void requestTPA(User u);

    void acceptTPA(User u);

    void denyTPA(User u);

    void launchTo(Location nextLocation);

    void addInventory(String name, Inventory inv);

    boolean hasInventory(String name);

    void removeInventory(String name);

    void swapInventory(String name);

    HashMap<String, Inventory> getInventories();

    String getCurrentInventory();

    void setHealth(double health);

    double getHealth();

    double getMaxHealth();

    void setHunger(int hunger);

    int getHunger();

    int getMaxHunger();

    void setSaturation(double saturation);

    double getSaturation();

    double getMaxSaturation();

}
