package dev.perryplaysmc.sources;

import dev.perryplaysmc.configuration.Config;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.PlayerInventory;

import java.util.UUID;

@SuppressWarnings("all")
public interface OfflineUser extends PermissionsHolder, BankAccountable{


    Config getConfig();

    String getName();

    UUID getUniqueId();

    PlayerInventory getInventory();

    void updateInventory();

    OfflinePlayer getSource();

    void sendMessage(String... messages);

    void sendMessage(String path, Object... objects);

    void setGameMode(GameMode mode);

    GameMode getGameMode();

    long getJoinTime();

    long getQuitTime();


}
