package dev.perryplaysmc.sources;

import org.bukkit.command.CommandSender;

import java.util.UUID;

@SuppressWarnings("all")
public interface CommandSource extends PermissionsHolder {


    String getName();

    UUID getUniqueId();

    Boolean isPlayer();

    CommandSender getSource();

    User getUser();

    void sendMessage(String... messages);

    void sendMessage(String path, Object... objects);

}
