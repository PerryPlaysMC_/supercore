package dev.perryplaysmc.sources.inheritors;

import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.core.Log;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.BankAccountable;
import dev.perryplaysmc.sources.PermissionsHolder;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.sources.data.Permission;
import dev.perryplaysmc.utils.ScoreboardUtil;
import dev.perryplaysmc.utils.TimerUtility;
import dev.perryplaysmc.utils.nmsUtils.classes.versionUtil.Version;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import dev.perryplaysmc.utils.text.strings.placeholders.Placeholder;
import dev.perryplaysmc.utils.text.strings.placeholders.PlaceholderAPI;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("all")
public class BaseUser implements User {

    Player source;
    PermissionAttachment attachment;
    HashMap<String, Inventory> inventories;
    List<String> tpaRequests;
    String current;
    Config cfg;

    public BaseUser(Player player) {
        source = player;
        inventories = new HashMap<>();
        attachment = source.addAttachment(Core.getInstance());
        cfg = new Config("plugins/SuperCore/users", source.getUniqueId().toString() + ".yml");
        attachment.getPermissions().forEach((key, value) -> {
            attachment.unsetPermission(key);
        });
        getActivePermissions().forEach(p -> attachment.setPermission(p.getName(), true));
        if(cfg.isSet("Inventories"))
            for(String s : cfg.getSection("Inventories").getKeys(false))
                if(!s.equalsIgnoreCase("current"))
                    inventories.put(s, cfg.getInventory("Inventories." + s));
        current = cfg.isSet("Inventories.current") ? cfg.getString("Inventories.current") : "default";
        tpaRequests = new ArrayList<>();
        ScoreboardUtil util = new ScoreboardUtil("test", SuperAPI.getSettings().getString("Scoreboard.User.title"));
        source.setScoreboard(util.generateScoreboard());
        new TimerUtility(() -> {
            for(String s : SuperAPI.getSettings().getSection("Scoreboard.User.lines").getKeys(false)) {
                int index = SuperAPI.getSettings().getInt("Scoreboard.User.lines." + s + ".index");

                String text = PlaceholderAPI.parseUserPlaceHolders(
                        SuperAPI.getSettings().getString("Scoreboard.User.lines." + s + ".text"),
                        this, this.getLocation());
                util.setText(index, text, "");
            }
        },0, SuperAPI.getSettings().getDouble("Scoreboard.User.interval"));
    }

    @Override
    public Config getConfig() {
        return cfg;
    }

    @Override
    public String getName() {
        return source.getName();
    }

    @Override
    public UUID getUniqueId() {
        return source.getUniqueId();
    }

    @Override
    public PlayerInventory getInventory() {
        return source.getInventory();
    }

    @Override
    public void updateInventory() {
        source.updateInventory();
    }

    @Override
    public Player getSource() {
        return source;
    }

    @Override
    public Location getLocation() {
        return source.getLocation();
    }

    @Override
    public void teleport(Location loc) {
        source.teleport(loc);
    }

    @Override
    public void teleport(Entity entity) {
        source.teleport(entity);
    }

    @Override
    public List<String> getTPARequests() {
        return tpaRequests;
    }

    @Override
    public void requestTPA(User u) {
        u.getTPARequests().add(getName());
        if(SuperAPI.getSettings().getBoolean("TPA.canTimeout"))
            new TimerUtility(() -> {
                u.getTPARequests().remove(getName());
            }, SuperAPI.getSettings().getDouble("TPA.timeout"), 0);
    }

    @Override
    public void acceptTPA(User u) {
        if(tpaRequests.contains(u.getName())) {
            new TimerUtility(() -> {
                tpaRequests.remove(u.getName());
                teleport(u.getLocation());
            }, SuperAPI.getSettings().getBoolean("TPA.isDelayed") ?
                    SuperAPI.getSettings().getDouble("TPA.delay") : 0, 0);
        }
    }

    @Override
    public void denyTPA(User u) {
        if(tpaRequests.contains(u.getName()))
            tpaRequests.remove(u.getName());
    }

    @Override
    public void launchTo(Location nextLocation) {
        Vector vec1 = getLocation().toVector();
        Vector vec2 = nextLocation.toVector();
        Vector pre_velocity = vec2.subtract(vec1);
        Vector velocity = pre_velocity.multiply(0.3);
        source.setVelocity(velocity);
    }

    @Override
    public void addInventory(String name, Inventory inv) {
        //inventories.put(name, inv);
        cfg.set("Inventories." + name, inv);
    }

    @Override
    public boolean hasInventory(String name) {
        if(name.equalsIgnoreCase("current"))return true;
        return cfg.isSet("Inventories." + name);
    }

    @Override
    public void removeInventory(String name) {
        cfg.set("Inventories." + name, null);
    }

    @Override
    public void swapInventory(String name) {
        current = name;
        cfg.set("Inventories.current", current);
        Inventory inv = cfg.getInventory("Inventories." + name);
        if(inv instanceof PlayerInventory) {
            getInventory().setArmorContents  (((PlayerInventory) inv).getArmorContents());
            getInventory().setExtraContents  (((PlayerInventory) inv).getExtraContents());
            getInventory().setStorageContents(((PlayerInventory) inv).getStorageContents());
        }else
            getInventory().setContents(inv.getContents());
        updateInventory();
    }
    public boolean isInvEmpty(Inventory inv) {
        for (ItemStack item : inv.getContents()) {
            if(item != null)
                return false;
        }
        for (ItemStack item : inv.getContents()) {
            if(item != null)
                return false;
        }
        return true;
    }

    @Override
    public HashMap<String, Inventory> getInventories() {
        return inventories;
    }

    @Override
    public String getCurrentInventory() {
        return current;
    }

    //Health Start

    @Override
    public void setHealth(double health) {
        source.setHealth(health);
    }

    @Override
    public double getHealth() {
        return source.getHealth();
    }

    @Override
    public double getMaxHealth() {
        return source.getMaxHealth();
    }

    @Override
    public void setHunger(int hunger) {
        source.setFoodLevel(hunger > 20 ? 20 : (int)hunger);
    }

    @Override
    public int getHunger() {
        return source.getFoodLevel();
    }

    @Override
    public int getMaxHunger() {
        return 20;
    }

    @Override
    public void setSaturation(double saturation) {
        source.setSaturation((float)saturation);
    }

    @Override
    public double getSaturation() {
        return source.getSaturation();
    }

    @Override
    public double getMaxSaturation() {
        return 20;
    }

    //Health End

    @Override
    public void sendMessage(String... messages) {
        Arrays.stream(messages).forEach(m -> source.sendMessage(StringUtils.translate(m)));
    }

    @Override
    public void sendMessage(String path, Object... objects) {
        sendMessage(StringUtils.Formatter.format(SuperAPI.getMessages(), path, objects));
    }

    @Override
    public void setGameMode(GameMode mode) {
        source.setGameMode(mode);
    }

    @Override
    public GameMode getGameMode() {
        return source.getGameMode();
    }

    @Override
    public long getJoinTime() {
        cfg.reload();
        return cfg.getLong("Data.joinTime");
    }

    @Override
    public long getQuitTime() {
        cfg.reload();
        return cfg.getLong("Data.quitTime");
    }

    //Permissions Start

    @Override
    public Boolean isOpped() {
        return source.isOp();
    }

    @Override
    public PermissionsHolder setOp(Boolean op) {
        source.setOp(op);
        return this;
    }

    @Override
    public Boolean hasPermission(Permission... permissions) {
        String prefix = SuperAPI.getSettings().getString("Data.Command.permissionPrefix");
        String feature = prefix + "feature.";
        String command = prefix + "command.";
        if(source.isOp()) return true;
        for(Permission perm : permissions)
            for(Permission p : getActivePermissions()) {
                if(perm.getName().equalsIgnoreCase(p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(prefix + p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(prefix + p.getName())) return true;

                if((feature + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(feature + p.getName())) return true;
                if((feature + perm.getName()).equalsIgnoreCase(feature + p.getName())) return true;

                if((command + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(command + p.getName())) return true;
                if((command + perm.getName()).equalsIgnoreCase(command + p.getName())) return true;
            }
        return false;
    }

    @Override
    public Boolean isPermissionSet(Permission... permissions) {
        String prefix = SuperAPI.getSettings().getString("Data.Command.permissionPrefix");
        String feature = prefix + "feature.";
        String command = prefix + "command.";
        for(Permission perm : permissions)
            for(Permission p : getPermissions()) {
                if(perm.getName().equalsIgnoreCase(p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(prefix + p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(prefix + p.getName())) return true;

                if((feature + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(feature + p.getName())) return true;
                if((feature + perm.getName()).equalsIgnoreCase(feature + p.getName())) return true;

                if((command + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(command + p.getName())) return true;
                if((command + perm.getName()).equalsIgnoreCase(command + p.getName())) return true;
            }
        return false;
    }

    @Override
    public PermissionsHolder givePermission(Permission permission) {
        List<String> currentPermissions = cfg.getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(s.startsWith("--") && permission.getName().equalsIgnoreCase(s.substring(2))) {
                newPermissions.add(s.substring(2));
                continue;
            } newPermissions.add(s);
        }
        if(!newPermissions.contains(permission.getName())) newPermissions.add(permission.getName());
        cfg.set("Data.permissions", newPermissions);
        attachment.setPermission(permission.getName(), true);
        return this;
    }

    @Override
    public PermissionsHolder takePermission(Permission permission) {
        List<String> currentPermissions = cfg.getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(!(s.startsWith("--") && permission.getName().equalsIgnoreCase(s.substring(2))))
                newPermissions.add(s);
        }
        if(newPermissions.contains(permission.getName())) newPermissions.remove(permission.getName());
        cfg.set("Data.permissions", newPermissions);
        attachment.unsetPermission(permission.getName());
        return this;
    }

    @Override
    public Boolean hasPermission(String... permissions) {
        Permission[] perms = new Permission[permissions.length];
        for(int i = 0; i < permissions.length; i++) {
            perms[i] = new Permission(permissions[i], true);
        }
        return hasPermission(perms);
    }

    @Override
    public Boolean isPermissionSet(String... permissions) {
        Permission[] perms = new Permission[permissions.length];
        for(int i = 0; i < permissions.length; i++) {
            perms[i] = new Permission(permissions[i], true);
        }
        return isPermissionSet(perms);
    }

    @Override
    public PermissionsHolder givePermission(String permission) {
        List<String> currentPermissions = cfg.getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(s.startsWith("--") && permission.equalsIgnoreCase(s.substring(2))) {
                newPermissions.add(s.substring(2));
                continue;
            } newPermissions.add(s);
        }
        if(!newPermissions.contains(permission)) newPermissions.add(permission);
        cfg.set("Data.permissions", newPermissions);
        attachment.setPermission(permission, true);
        return null;
    }

    @Override
    public PermissionsHolder takePermission(String permission) {
        List<String> currentPermissions = cfg.getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(!(s.startsWith("--") && permission.equalsIgnoreCase(s.substring(2))))
                newPermissions.add(s);
        }
        if(newPermissions.contains(permission)) newPermissions.remove(permission);
        cfg.set("Data.permissions", newPermissions);
        attachment.unsetPermission(permission);
        return this;
    }

    @Override
    public Set<Permission> getPermissions() {
        Set<Permission> perms = new HashSet<>();
        cfg.getStringList("Data.permissions").forEach(s -> {
            perms.add(new Permission(s.startsWith("--") ? s.substring(2) : s, s.startsWith("--") ? false : true));
        });
        return perms;
    }

    @Override
    public Set<Permission> getActivePermissions() {
        return getPermissions().stream().filter(Permission::isActive).collect(Collectors.toSet());
    }

    //Permissions End

    //Economy Start

    @Override
    public double getBalance() {
        return cfg.getDouble("Balance");
    }

    @Override
    public String getFormattedBalance() {
        return NumberUtil.format(getBalance());
    }

    @Override
    public boolean hasEnough(double money) {
        return getBalance() >= money || (!exceedsMin(false, money));
    }

    @Override
    public BankAccountable withdraw(double money) {
        setBalance(getBalance()-money);
        return this;
    }

    @Override
    public BankAccountable setBalance(double money) {
        cfg.set("Balance", money);
        return this;
    }

    @Override
    public BankAccountable deposit(double money) {
        setBalance(getBalance() + money);
        return this;
    }

    @Override
    public boolean exceedsMax(boolean isSet, double amt) {
        String maxBalance = SuperAPI.getSettings().getString("Economy.maxBalance");
        if(maxBalance.equalsIgnoreCase("off") || maxBalance.equalsIgnoreCase("false")) return false;
        if(NumberUtil.isDouble(maxBalance)) {
            double d = NumberUtil.getMoney(maxBalance);
            if(isSet)
                if(amt>d)
                    return true;
                else
                    return false;
            if((getBalance()+amt)>d) {
                return true;
            }else return false;
        }
        Log.error("Settings.yml >> 'Economy.maxBalance' is not a valid number or is not [off, false] " + maxBalance);
        return false;
    }

    @Override
    public boolean exceedsMin(boolean isSet, double amt) {
        String minBalance = SuperAPI.getSettings().getString("Economy.minBalance");
        if(minBalance.equalsIgnoreCase("off") || minBalance.equalsIgnoreCase("false")) return false;
        if(NumberUtil.isDouble(minBalance)) {
            double d = NumberUtil.getMoney(minBalance);
            if(isSet)
                if(amt<d)
                    return true;
                else
                    return false;
            if((getBalance()-amt)<d) {
                return true;
            }else return false;
        }
        Log.error("Settings.yml >> 'Economy.minBalance' is not a valid number or is not [off, false] " + minBalance);
        return false;
    }

    //Economy End
}
