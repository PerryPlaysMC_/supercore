package dev.perryplaysmc.sources.inheritors;

import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.core.Log;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.BankAccountable;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.PermissionsHolder;
import dev.perryplaysmc.sources.data.Permission;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.PermissionAttachment;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("all")
public class BaseOfflineUser implements OfflineUser {

    OfflinePlayer source;
    PermissionAttachment attachment;
    Config cfg;

    public BaseOfflineUser(OfflinePlayer player) {
        source = player;
        if(Core.getInstance().loadAsPlayer(source)!=null)
            attachment = Core.getInstance().loadAsPlayer(source).addAttachment(Core.getInstance());
        cfg = new Config("plugins/SuperCore/users", source.getUniqueId().toString()+".yml");
        cfg = !source.isOnline() ? cfg : SuperAPI.getUser(source.getUniqueId()).getConfig();
        attachment.getPermissions().forEach((key, value)->{
            attachment.unsetPermission(key);
        });
    }

    @Override
    public Config getConfig() {
        return !source.isOnline() ? cfg : SuperAPI.getUser(source.getUniqueId()).getConfig();
    }

    @Override
    public String getName() {
        return source.getName();
    }

    @Override
    public UUID getUniqueId() {
        return source.getUniqueId();
    }

    @Override
    public PlayerInventory getInventory() {
        return null;
    }

    @Override
    public void updateInventory() {

    }

    @Override
    public OfflinePlayer getSource() {
        return source;
    }

    @Override
    public void sendMessage(String... messages) {
        if(source.isOnline())
            Arrays.stream(messages).forEach(m -> Bukkit.getPlayer(source.getName()).sendMessage(StringUtils.translate(m)));
    }

    @Override
    public void sendMessage(String path, Object... objects) {
        sendMessage(StringUtils.Formatter.format(SuperAPI.getMessages(), path, objects));
    }

    @Override
    public void setGameMode(GameMode mode) {
        if(source.isOnline())
            source.getPlayer().setGameMode(mode);
        try{
            Player player = SuperAPI.getCore().loadAsPlayer(source);
            player.setGameMode(mode);
            player.saveData();
        }catch (Exception e) {
        }
    }

    @Override
    public GameMode getGameMode() {
        if(source.isOnline())
            return source.getPlayer().getGameMode();
        return SuperAPI.getCore().loadAsPlayer(source).getGameMode();
    }

    @Override
    public long getJoinTime() {
        getConfig().reload();
        return getConfig().getLong("Data.joinTime");
    }

    @Override
    public long getQuitTime() {
        getConfig().reload();
        return getConfig().getLong("Data.quitTime");
    }

    //Permissions Start

    @Override
    public Boolean isOpped() {
        return source.isOp();
    }

    @Override
    public PermissionsHolder setOp(Boolean op) {
        source.setOp(op);
        return this;
    }

    @Override
    public Boolean hasPermission(Permission... permissions) {
        String prefix = SuperAPI.getSettings().getString("Data.Command.permissionPrefix");
        String feature = prefix+"feature.";
        String command = prefix + "command.";
        if(source.isOp()) return true;
        for(Permission perm : permissions)
            for(Permission p : getActivePermissions()) {
                if(perm.getName().equalsIgnoreCase(p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(prefix + p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(prefix + p.getName())) return true;

                if((feature + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(feature + p.getName())) return true;
                if((feature + perm.getName()).equalsIgnoreCase(feature + p.getName())) return true;

                if((command + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(command + p.getName())) return true;
                if((command + perm.getName()).equalsIgnoreCase(command + p.getName())) return true;
            }
        return false;
    }

    @Override
    public Boolean isPermissionSet(Permission... permissions) {
        String prefix = SuperAPI.getSettings().getString("Data.Command.permissionPrefix");
        String feature = prefix+"feature.";
        String command = prefix + "command.";
        for(Permission perm : permissions)
            for(Permission p : getPermissions()) {
                if(perm.getName().equalsIgnoreCase(p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(prefix + p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(prefix + p.getName())) return true;

                if((feature + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(feature + p.getName())) return true;
                if((feature + perm.getName()).equalsIgnoreCase(feature + p.getName())) return true;

                if((command + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(command + p.getName())) return true;
                if((command + perm.getName()).equalsIgnoreCase(command + p.getName())) return true;
            }
        return false;
    }

    @Override
    public PermissionsHolder givePermission(Permission permission) {
        List<String> currentPermissions = getConfig().getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(s.startsWith("--")&&permission.getName().equalsIgnoreCase(s.substring(2))) {
                newPermissions.add(s.substring(2));
            }else newPermissions.add(s);
        }
        if(!newPermissions.contains(permission.getName()))newPermissions.add(permission.getName());
        getConfig().set("Data.permissions", newPermissions);
        attachment.setPermission(permission.getName(), true);
        return this;
    }

    @Override
    public PermissionsHolder takePermission(Permission permission) {
        List<String> currentPermissions = getConfig().getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(!(s.startsWith("--")&&permission.getName().equalsIgnoreCase(s.substring(2))))
                newPermissions.add(s);
        }
        if(newPermissions.contains(permission.getName()))newPermissions.remove(permission.getName());
        getConfig().set("Data.permissions", newPermissions);
        attachment.unsetPermission(permission.getName());
        return this;
    }

    @Override
    public Boolean hasPermission(String... permissions) {
        String prefix = SuperAPI.getSettings().getString("Data.Command.permissionPrefix");
        String feature = prefix+"feature.";
        String command = prefix + "command.";
        Permission[] perms = new Permission[permissions.length];
        for(int i = 0; i < permissions.length; i++) {
            perms[i] = new Permission(permissions[i], true);
        }
        if(source.isOp()) return true;
        for(Permission perm : perms)
            for(Permission p : getActivePermissions()) {
                if(perm.getName().equalsIgnoreCase(p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(prefix + p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(prefix + p.getName())) return true;

                if((feature + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(feature + p.getName())) return true;
                if((feature + perm.getName()).equalsIgnoreCase(feature + p.getName())) return true;

                if((command + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(command + p.getName())) return true;
                if((command + perm.getName()).equalsIgnoreCase(command + p.getName())) return true;
            }
        return false;
    }

    @Override
    public Boolean isPermissionSet(String... permissions) {
        String prefix = SuperAPI.getSettings().getString("Data.Command.permissionPrefix");
        String feature = prefix+"feature.";
        String command = prefix + "command.";
        Permission[] perms = new Permission[permissions.length];
        for(int i = 0; i < permissions.length; i++) {
            perms[i] = new Permission(permissions[i], true);
        }
        for(Permission perm : perms)
            for(Permission p : getPermissions()) {
                if(perm.getName().equalsIgnoreCase(p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(prefix + p.getName())) return true;
                if((prefix + perm.getName()).equalsIgnoreCase(prefix + p.getName())) return true;

                if((feature + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(feature + p.getName())) return true;
                if((feature + perm.getName()).equalsIgnoreCase(feature + p.getName())) return true;

                if((command + perm.getName()).equalsIgnoreCase(p.getName())) return true;
                if(perm.getName().equalsIgnoreCase(command + p.getName())) return true;
                if((command + perm.getName()).equalsIgnoreCase(command + p.getName())) return true;
            }
        return false;
    }

    @Override
    public PermissionsHolder givePermission(String permission) {
        List<String> currentPermissions = getConfig().getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(s.startsWith("--")&&permission.equalsIgnoreCase(s.substring(2))) {
                newPermissions.add(s.substring(2));
            }else newPermissions.add(s);
        }
        if(!newPermissions.contains(permission))newPermissions.add(permission);
        getConfig().set("Data.permissions", newPermissions);
        attachment.setPermission(permission, true);
        return null;
    }

    @Override
    public PermissionsHolder takePermission(String permission) {
        List<String> currentPermissions = getConfig().getStringList("Data.permissions");
        List<String> newPermissions = new ArrayList<>();
        for(String s : currentPermissions) {
            if(!(s.startsWith("--")&&permission.equalsIgnoreCase(s.substring(2))))
                newPermissions.add(s);
        }
        if(newPermissions.contains(permission))newPermissions.remove(permission);
        getConfig().set("Data.permissions", newPermissions);
        attachment.unsetPermission(permission);
        return this;
    }

    @Override
    public Set<Permission> getPermissions() {
        Set<Permission> perms = new HashSet<>();
        getConfig().getStringList("permissions").forEach(s -> {
            perms.add(new Permission(s.startsWith("--") ? s.substring(2) : s, s.startsWith("--") ? false : true));
        });
        return perms;
    }

    @Override
    public Set<Permission> getActivePermissions() {
        return getPermissions().stream().filter(Permission::isActive).collect(Collectors.toSet());
    }

    //Permissions End

    //Economy Start

    @Override
    public double getBalance() {
        return getConfig().getDouble("Balance");
    }

    @Override
    public String getFormattedBalance() {
        return NumberUtil.format(getBalance());
    }

    @Override
    public boolean hasEnough(double money) {
        return getBalance() >= money || (!exceedsMin(false, money));
    }

    @Override
    public BankAccountable withdraw(double money) {
        setBalance(getBalance()-money);
        return this;
    }

    @Override
    public BankAccountable setBalance(double money) {
        getConfig().set("Balance", money);
        return this;
    }

    @Override
    public BankAccountable deposit(double money) {
        setBalance(getBalance() + money);
        return this;
    }

    @Override
    public boolean exceedsMax(boolean isSet, double amt) {
        String maxBalance = SuperAPI.getSettings().getString("Economy.maxBalance");
        if(maxBalance.equalsIgnoreCase("off") || maxBalance.equalsIgnoreCase("false")) return false;
        if(NumberUtil.isDouble(maxBalance)) {
            double d = NumberUtil.getMoney(maxBalance);
            if(isSet)
                if(amt>d)
                    return true;
                else
                    return false;
            if((getBalance()+amt)>d) {
                return true;
            }else return false;
        }
        Log.error("Settings.yml >> 'Economy.maxBalance' is not a valid number or is not [off, false] " + maxBalance);
        return false;
    }

    @Override
    public boolean exceedsMin(boolean isSet, double amt) {
        String minBalance = SuperAPI.getSettings().getString("Economy.minBalance");
        if(minBalance.equalsIgnoreCase("off") || minBalance.equalsIgnoreCase("false")) return false;
        if(NumberUtil.isDouble(minBalance)) {
            double d = NumberUtil.getMoney(minBalance);
            if(isSet)
                if(amt<d)
                    return true;
                else
                    return false;
            if((getBalance()-amt)<d) {
                return true;
            }else return false;
        }
        Log.error("Settings.yml >> 'Economy.minBalance' is not a valid number or is not [off, false] " + minBalance);
        return false;
    }

    //Economy End
}
