package dev.perryplaysmc.sources.inheritors;

import dev.perryplaysmc.core.Core;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.PermissionsHolder;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.sources.data.Permission;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversation;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.sources.inheritors
 * Class: BaseCommandSource
 * <p>
 * Path: dev.perryplaysmc.sources.inheritors.BaseCommandSource
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class BaseCommandSource implements CommandSource {

    CommandSender source;

    public BaseCommandSource(CommandSender sender) {
        source = sender;
    }


    @Override
    public String getName() {
        return isPlayer() ? source.getName() : "Console";
    }

    @Override
    public UUID getUniqueId() {
        return isPlayer() ? getUser().getUniqueId() : UUID.randomUUID();
    }

    @Override
    public Boolean isPlayer() {
        return source instanceof Player;
    }

    @Override
    public CommandSender getSource() {
        return source;
    }

    @Override
    public User getUser() {
        return SuperAPI.getUser(source.getName());
    }

    @Override
    public void sendMessage(String... messages) {
        Arrays.stream(messages).forEach(m -> source.sendMessage(StringUtils.translate(m)));
    }

    @Override
    public void sendMessage(String path, Object... objects) {
        sendMessage(StringUtils.Formatter.format(SuperAPI.getMessages(), path, objects));
    }


    //Permissions Start


    @Override
    public Boolean isOpped() {
        return source.isOp();
    }

    @Override
    public PermissionsHolder setOp(Boolean op) {
        source.setOp(op);
        return this;
    }

    @Override
    public Boolean hasPermission(Permission... permissions) {
        return isPlayer() ? getUser().hasPermission(permissions) : true;
    }

    @Override
    public Boolean isPermissionSet(Permission... permissions) {
        return isPlayer() ? getUser().isPermissionSet(permissions) : true;
    }

    @Override
    public PermissionsHolder givePermission(Permission permission) {
        return isPlayer() ? getUser().givePermission(permission) : this;
    }

    @Override
    public PermissionsHolder takePermission(Permission permission) {
        return isPlayer() ? getUser().takePermission(permission) : this;
    }

    @Override
    public Boolean hasPermission(String... permissions) {
        return isPlayer() ? getUser().hasPermission(permissions) : true;
    }

    @Override
    public Boolean isPermissionSet(String... permissions) {
        return isPlayer() ? getUser().isPermissionSet(permissions) : true;
    }

    @Override
    public PermissionsHolder givePermission(String permission) {
        return isPlayer() ? getUser().givePermission(permission) : this;
    }

    @Override
    public PermissionsHolder takePermission(String permission) {
        return isPlayer() ? getUser().takePermission(permission) : this;
    }

    @Override
    public Set<Permission> getPermissions() {
        if(isPlayer()) return getUser().getPermissions();
        Set<Permission> perms = new HashSet<>();
        for(org.bukkit.permissions.Permission perm : Bukkit.getPluginManager().getPermissions())
            perms.add(new Permission(perm.getName(), true));
        return perms;
    }

    @Override
    public Set<Permission> getActivePermissions() {
        if(isPlayer()) return getUser().getActivePermissions();
        return getPermissions();
    }

    //Permissions End

}
