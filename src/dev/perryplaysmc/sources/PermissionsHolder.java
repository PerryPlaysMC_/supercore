package dev.perryplaysmc.sources;

import dev.perryplaysmc.sources.data.Permission;

import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.sources
 * Class: PermissionsHolder
 * <p>
 * Path: dev.perryplaysmc.sources.PermissionsHolder
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public interface PermissionsHolder {

    Boolean isOpped();

    PermissionsHolder setOp(Boolean op);

    Boolean hasPermission(Permission... permissions);

    Boolean isPermissionSet(Permission... permissions);

    PermissionsHolder givePermission(Permission permission);

    PermissionsHolder takePermission(Permission permission);

    Boolean hasPermission(String... permissions);

    Boolean isPermissionSet(String... permissions);

    PermissionsHolder givePermission(String permission);

    PermissionsHolder takePermission(String permission);

    Set<Permission> getPermissions();

    Set<Permission> getActivePermissions();



}
