package dev.perryplaysmc.sources.data;

import com.google.common.base.Preconditions;
import org.apache.commons.lang.Validate;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_14_R1.util.CraftLegacy;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.sources.data
 * Class: PlayerInventory
 * <p>
 * Path: dev.perryplaysmc.sources.data.PlayerInventory
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class PlayerInventoryImpl implements PlayerInventory {

    int maxStack = 41;
    ItemStack[] contents, storage, armor, extra;
    ItemStack main;

    public PlayerInventoryImpl(ItemStack main, ItemStack[] contents, ItemStack[] storage, ItemStack[] armor, ItemStack[] extra) {
        this.main = main;
        this.contents = contents;
        this.storage = storage;
        this.armor = armor;
        this.extra = extra;
    }

    @Override
    public @NotNull ItemStack[] getArmorContents() {
        return armor;
    }

    @Override
    public @NotNull ItemStack[] getExtraContents() {
        return extra;
    }

    @Override
    public ItemStack getHelmet() {
        return this.getItem(this.getSize() - 2);
    }

    @Override
    public ItemStack getChestplate() {
        return this.getItem(this.getSize() - 3);
    }
    @Override
    public ItemStack getLeggings() {
        return this.getItem(this.getSize() - 4);
    }

    @Override
    public ItemStack getBoots() {
        return this.getItem(this.getSize() - 5);
    }

    @Override
    public int getSize() {
        return 41;
    }

    @Override
    public int getMaxStackSize() {
        return maxStack;
    }

    @Override
    public void setMaxStackSize(int i) {
        maxStack = i;
    }

    @Override
    public @Nullable ItemStack getItem(int i) {
        return null;
    }

    @Override
    public void setItem(int i, @Nullable ItemStack itemStack) {
        contents[i] = itemStack;
    }

    @Override
    public @NotNull ItemStack[] getContents() {
        return contents;
    }

    @Override
    public void setContents(@NotNull ItemStack[] itemStacks) throws IllegalArgumentException {
        contents = itemStacks;
    }

    @Override
    public @NotNull ItemStack[] getStorageContents() {
        return storage;
    }

    public boolean contains(Material material) {
        Validate.notNull(material, "Material cannot be null");
        material = CraftLegacy.fromLegacy(material);
        ItemStack[] var5;
        int var4 = (var5 = this.getStorageContents()).length;

        for(int var3 = 0; var3 < var4; ++var3) {
            ItemStack item = var5[var3];
            if (item != null && item.getType() == material) {
                return true;
            }
        }

        return false;
    }

    public boolean contains(ItemStack item) {
        if (item == null) {
            return false;
        } else {
            ItemStack[] var5;
            int var4 = (var5 = this.getStorageContents()).length;

            for(int var3 = 0; var3 < var4; ++var3) {
                ItemStack i = var5[var3];
                if (item.equals(i)) {
                    return true;
                }
            }

            return false;
        }
    }

    public boolean contains(Material material, int amount) {
        Validate.notNull(material, "Material cannot be null");
        material = CraftLegacy.fromLegacy(material);
        if (amount <= 0) {
            return true;
        } else {
            ItemStack[] var6;
            int var5 = (var6 = this.getStorageContents()).length;

            for(int var4 = 0; var4 < var5; ++var4) {
                ItemStack item = var6[var4];
                if (item != null && item.getType() == material && (amount -= item.getAmount()) <= 0) {
                    return true;
                }
            }

            return false;
        }
    }

    public boolean contains(ItemStack item, int amount) {
        if (item == null) {
            return false;
        } else if (amount <= 0) {
            return true;
        } else {
            ItemStack[] var6;
            int var5 = (var6 = this.getStorageContents()).length;

            for(int var4 = 0; var4 < var5; ++var4) {
                ItemStack i = var6[var4];
                if (item.equals(i)) {
                    --amount;
                    if (amount <= 0) {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public boolean containsAtLeast(ItemStack item, int amount) {
        if (item == null) {
            return false;
        } else if (amount <= 0) {
            return true;
        } else {
            ItemStack[] var6;
            int var5 = (var6 = this.getStorageContents()).length;

            for(int var4 = 0; var4 < var5; ++var4) {
                ItemStack i = var6[var4];
                if (item.isSimilar(i) && (amount -= i.getAmount()) <= 0) {
                    return true;
                }
            }

            return false;
        }
    }


    public HashMap<Integer, ItemStack> all(Material material) {
        Validate.notNull(material, "Material cannot be null");
        material = CraftLegacy.fromLegacy(material);
        HashMap<Integer, ItemStack> slots = new HashMap();
        ItemStack[] inventory = this.getStorageContents();

        for(int i = 0; i < inventory.length; ++i) {
            ItemStack item = inventory[i];
            if (item != null && item.getType() == material) {
                slots.put(i, item);
            }
        }

        return slots;
    }

    public HashMap<Integer, ItemStack> all(ItemStack item) {
        HashMap<Integer, ItemStack> slots = new HashMap();
        if (item != null) {
            ItemStack[] inventory = this.getStorageContents();

            for(int i = 0; i < inventory.length; ++i) {
                if (item.equals(inventory[i])) {
                    slots.put(i, inventory[i]);
                }
            }
        }

        return slots;
    }

    private int first(ItemStack item, boolean withAmount) {
        if (item == null) {
            return -1;
        } else {
            ItemStack[] inventory = this.getStorageContents();
            int i = 0;

            while(true) {
                if (i >= inventory.length) {
                    return -1;
                }

                if (inventory[i] != null) {
                    if (withAmount) {
                        if (item.equals(inventory[i])) {
                            break;
                        }
                    } else if (item.isSimilar(inventory[i])) {
                        break;
                    }
                }

                ++i;
            }

            return i;
        }
    }

    public int firstPartial(Material material) {
        Validate.notNull(material, "Material cannot be null");
        material = CraftLegacy.fromLegacy(material);
        ItemStack[] inventory = this.getStorageContents();

        for(int i = 0; i < inventory.length; ++i) {
            ItemStack item = inventory[i];
            if (item != null && item.getType() == material && item.getAmount() < item.getMaxStackSize()) {
                return i;
            }
        }

        return -1;
    }

    private int firstPartial(ItemStack item) {
        ItemStack[] inventory = this.getStorageContents();
        ItemStack filteredItem = CraftItemStack.asCraftCopy(item);
        if (item == null) {
            return -1;
        } else {
            for(int i = 0; i < inventory.length; ++i) {
                ItemStack cItem = inventory[i];
                if (cItem != null && cItem.getAmount() < cItem.getMaxStackSize() && cItem.isSimilar(filteredItem)) {
                    return i;
                }
            }

            return -1;
        }
    }

    public HashMap<Integer, ItemStack> addItem(ItemStack... items) {
        Validate.noNullElements(items, "Item cannot be null");
        HashMap<Integer, ItemStack> leftover = new HashMap();

        label35:
        for(int i = 0; i < items.length; ++i) {
            ItemStack item = items[i];

            while(true) {
                while(true) {
                    int firstPartial = this.firstPartial(item);
                    if (firstPartial == -1) {
                        int firstFree = this.firstEmpty();
                        if (firstFree == -1) {
                            leftover.put(i, item);
                            continue label35;
                        }

                        if (item.getAmount() <= this.getMaxItemStack()) {
                            this.setItem(firstFree, item);
                            continue label35;
                        }

                        CraftItemStack stack = CraftItemStack.asCraftCopy(item);
                        stack.setAmount(this.getMaxItemStack());
                        this.setItem(firstFree, stack);
                        item.setAmount(item.getAmount() - this.getMaxItemStack());
                    } else {
                        ItemStack partialItem = this.getItem(firstPartial);
                        int amount = item.getAmount();
                        int partialAmount = partialItem.getAmount();
                        int maxAmount = partialItem.getMaxStackSize();
                        if (amount + partialAmount <= maxAmount) {
                            partialItem.setAmount(amount + partialAmount);
                            this.setItem(firstPartial, partialItem);
                            continue label35;
                        }

                        partialItem.setAmount(maxAmount);
                        this.setItem(firstPartial, partialItem);
                        item.setAmount(amount + partialAmount - maxAmount);
                    }
                }
            }
        }

        return leftover;
    }

    public HashMap<Integer, ItemStack> removeItem(ItemStack... items) {
        Validate.notNull(items, "Items cannot be null");
        HashMap<Integer, ItemStack> leftover = new HashMap();

        for(int i = 0; i < items.length; ++i) {
            ItemStack item = items[i];
            int toDelete = item.getAmount();

            while(true) {
                int first = first(item, false);
                if (first == -1) {
                    item.setAmount(toDelete);
                    leftover.put(i, item);
                    break;
                }

                ItemStack itemStack = this.getItem(first);
                int amount = itemStack.getAmount();
                if (amount <= toDelete) {
                    toDelete -= amount;
                    this.clear(first);
                } else {
                    itemStack.setAmount(amount - toDelete);
                    this.setItem(first, itemStack);
                    toDelete = 0;
                }

                if (toDelete <= 0) {
                    break;
                }
            }
        }

        return leftover;
    }

    private int getMaxItemStack() {
        return getMaxStackSize();
    }

    public void remove(Material material) {
        Validate.notNull(material, "Material cannot be null");
        material = CraftLegacy.fromLegacy(material);
        ItemStack[] items = this.getStorageContents();

        for(int i = 0; i < items.length; ++i) {
            if (items[i] != null && items[i].getType() == material) {
                this.clear(i);
            }
        }

    }

    public void remove(ItemStack item) {
        ItemStack[] items = this.getStorageContents();

        for(int i = 0; i < items.length; ++i) {
            if (items[i] != null && items[i].equals(item)) {
                this.clear(i);
            }
        }

    }


    @Override
    public int first(Material material) {
        Validate.notNull(material, "Material cannot be null");
        material = CraftLegacy.fromLegacy(material);
        ItemStack[] inventory = this.getStorageContents();

        for(int i = 0; i < inventory.length; ++i) {
            ItemStack item = inventory[i];
            if (item != null && item.getType() == material) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public int first(ItemStack item) {
        return this.first(item, true);
    }

    @Override
    public int firstEmpty() {
        ItemStack[] inventory = this.getStorageContents();

        for(int i = 0; i < inventory.length; ++i) {
            if (inventory[i] == null) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public void clear(int index) {
        this.setItem(index, (ItemStack)null);
    }

    public void clear() {
        for(int i = 0; i < this.getSize(); ++i) {
            this.clear(i);
        }

    }

    @Override
    public @NotNull List<HumanEntity> getViewers() {
        return new ArrayList<>();
    }

    @Override
    public @NotNull InventoryType getType() {
        return InventoryType.PLAYER;
    }

    private void setSlots(ItemStack[] items, int baseSlot, int length) {
        if (items == null) {
            items = new ItemStack[length];
        }

        Preconditions.checkArgument(items.length <= length, "items.length must be < %s", length);

        for(int i = 0; i < length; ++i) {
            if (i >= items.length) {
                this.setItem(baseSlot + i, (ItemStack)null);
            } else {
                this.setItem(baseSlot + i, items[i]);
            }
        }

    }
    @Override
    public void setStorageContents(ItemStack[] items) throws IllegalArgumentException {
        storage = items;
    }
    @Override
    public void setArmorContents(ItemStack[] items) {
        armor = items;
    }

    @Override
    public void setExtraContents(ItemStack[] items) {
        extra = items;
    }

    @Override
    public void setHelmet(ItemStack helmet) {
        this.setItem(this.getSize() - 2, helmet);
    }

    @Override
    public void setChestplate(ItemStack chestplate) {
        this.setItem(this.getSize() - 3, chestplate);
    }

    @Override
    public void setLeggings(ItemStack leggings) {
        this.setItem(this.getSize() - 4, leggings);
    }

    @Override
    public void setBoots(ItemStack boots) {
        this.setItem(this.getSize() - 5, boots);
    }

    @Override
    public @NotNull ItemStack getItemInMainHand() {
        return main;
    }

    @Override
    public void setItemInMainHand(@Nullable ItemStack itemStack) {
        main = itemStack;
    }

    @Override
    public @NotNull ItemStack getItemInOffHand() {
        return main;
    }

    @Override
    public void setItemInOffHand(@Nullable ItemStack itemStack) {

    }

    @Override
    public @NotNull ItemStack getItemInHand() {
        return null;
    }

    @Override
    public void setItemInHand(@Nullable ItemStack itemStack) {

    }

    @Override
    public int getHeldItemSlot() {
        return 0;
    }

    @Override
    public void setHeldItemSlot(int i) {

    }

    @Override
    public @Nullable HumanEntity getHolder() {
        return null;
    }

    @Override
    public @NotNull ListIterator<ItemStack> iterator() {
        return null;
    }

    @Override
    public @NotNull ListIterator<ItemStack> iterator(int i) {
        return null;
    }

    @Override
    public @Nullable Location getLocation() {
        return null;
    }
}
