package dev.perryplaysmc.sources.data;

import lombok.Getter;
import org.bukkit.Bukkit;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.sources.data
 * Class: Permission
 * <p>
 * Path: dev.perryplaysmc.sources.data.Permission
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Permission {

    @Getter
    String name;

    Boolean isActive;

    public Permission(String name, boolean isActive) {
        this.name = name;
        this.isActive = isActive;
    }

    public Boolean isActive() {
        return isActive;
    }

    public static Permission createPerm(String name, boolean isActive) {
        if(Bukkit.getPluginManager().getPermission(name)==null) {
            Bukkit.getPluginManager().addPermission(new org.bukkit.permissions.Permission(name));
        }
        return new Permission(name, isActive);
    }

}
