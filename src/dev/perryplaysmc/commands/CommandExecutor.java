package dev.perryplaysmc.commands;

import com.google.common.collect.ImmutableList;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.AccessLevel;
import lombok.Getter;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.commands
 * Class: CommandExecutor
 * <p>
 * Path: dev.perryplaysmc.commands.CommandExecutor
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class CommandExecutor extends org.bukkit.command.Command {

    @Getter(AccessLevel.PRIVATE)
    private Command command;
    private Argument[] args;
    private int f = 0;

    @Getter
    private static Set<Command> commands = new HashSet<>();

    public static void addCommand(Command cmd) {
        commands.add(cmd);
    }

    public CommandExecutor(String name, Command cmd) {
        super(name);
        command = cmd;
    }

    @Override
    public boolean execute(@NotNull CommandSender sender, @NotNull String cl, @NotNull String[] strings) {
        args = new Argument[strings.length];
        for(int i = 0; i < strings.length; i++) {
            args[i] = new Argument(strings[i]);
        }
        getCommand().args = args;
        getCommand().sender = SuperAPI.getSource(sender.getName());
        getCommand().setLabel(new CommandLabel(cl));

        CommandSource s = SuperAPI.getSource(sender.getName());
        if(!testPermission(sender)) return true;
        try {
            getCommand().run(s, new CommandLabel(cl), args);
        } catch (CommandException e) {
            if(!StringUtils.isEmpty(e.getMessage()) && !e.isIgnore())
                s.sendMessage(e.getMessage());
        }

        return true;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, @NotNull String alias, String[] strings) throws IllegalArgumentException {
        Argument[] args = new Argument[strings.length];
        for(int i = 0; i < strings.length; i++) {
            args[i] = new Argument(strings[i]);
        }
        getCommand().tabArgs = args;
        getCommand().sender = SuperAPI.getSource(sender.getName());
        getCommand().setLabel(new CommandLabel(alias));
        if(!testPermissionSilent(sender)) return ImmutableList.of();
        try {
            if(args.length == 0 || (args.length>=(getCommand().getMaxTabIndex()+1)&&getCommand().allowMaxTabIndex())) {
                return ImmutableList.of();
            }
            return getCommand().onTab(SuperAPI.getSource(sender.getName()), new CommandLabel(alias), args);
        } catch (CommandException e) {
            if(!StringUtils.isEmpty(e.getMessage()) && !e.isIgnore())
                SuperAPI.getSource(sender.getName()).sendMessage(e.getMessage());
        }
        return ImmutableList.of();
    }


    @Override
    public boolean testPermission(@NotNull CommandSender target) {
        if(testPermissionSilent(target)) return true;
        String perm = "none";
        for(String s : getCommand().getDefaultPermissions())
            if(s.startsWith(getName() + ":"))
                perm = s.replace(getName() + ":", "");
        target.sendMessage(
                StringUtils.Formatter.formatDefault("Messages.Error.invalidPermission", perm)
        );
        return false;
    }

    @Override
    public boolean testPermissionSilent(CommandSender target) {
        if(SuperAPI.getSource(target) == null) {
            for(String s : getCommand().getDefaultPermissions())
                if(s.startsWith(getName() + ":")) {
                    if(target.hasPermission(s.replace(getName() + ":", "")) || target.isOp())
                        return true;

                }

            return getCommand().getDefaultPermissions().length == 0;
        }
        for(String s : getCommand().getDefaultPermissions())
            if(s.startsWith(getName() + ":")) {
                if(SuperAPI.getSource(target).hasPermission(s.replace(getName() + ":", "")) || target.isOp())

                    return true;
            }
        return getCommand().getDefaultPermissions().length == 0;
    }
}
