package dev.perryplaysmc.commands;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.commands
 * Class: TabCompletionExecutor
 * <p>
 * Path: dev.perryplaysmc.commands.TabCompletionExecutor
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public abstract class TabCompletionExecutor {

    protected Argument[] tabArgs;
    @Getter@Accessors(fluent = true)
    private boolean allowOfflineTab = true, allowMaxTabIndex = false;
    @Getter
    private int maxTabIndex = 1;

    protected List<String> newList() {
        return new ArrayList<>();
    }


    public List<String> onTab(CommandSource s, CommandLabel label, Argument[] args) throws CommandException {
        List<String> f = Lists.newArrayList();
        List<String> l = Lists.newArrayList();
        if(args.length == 0 || (args.length>=(maxTabIndex+1)&&allowMaxTabIndex)) {
            return ImmutableList.of();
        }
        String name = args[args.length+-1].getInput();
        for(User u : SuperAPI.getOnlineUsers()) {
            l.add(u.getName());
        }
        if(allowOfflineTab)
            for(OfflineUser u : SuperAPI.getOfflineUsers()) {
                if(!l.contains(u.getName()))
                    l.add(u.getName());
            }
        for(String u : l) {
            if(u.toLowerCase().startsWith(name.toLowerCase())) f.add(u);
        }
        return f;
    }

    protected List<String> list(String... args) {
        return Arrays.asList(args);
    }

    protected void setAllowOfflineTab(boolean allowOfflineTab) {
        this.allowOfflineTab = allowOfflineTab;
    }

    protected void setAllowMaxTabIndex(boolean allowMaxTabIndex) {
        this.allowMaxTabIndex = allowMaxTabIndex;
    }

    protected void setMaxTabIndex(int maxTabIndex) {
        this.maxTabIndex = maxTabIndex;
    }


    protected boolean isArg(String name) {
        for(Argument arg : tabArgs) {
            if(arg.getInput().equalsIgnoreCase(name)) return true;
        }
        return false;
    }

    protected List<String> loadTab(int length) {
        if(tabArgs.length == length) {
            List<String> f = Lists.newArrayList();
            List<String> l = Lists.newArrayList();
            String name = tabArgs[tabArgs.length+-1].getInput();
            for(User u : SuperAPI.getOnlineUsers()) {
                l.add(u.getName());
            }
            if(allowOfflineTab)
                for(OfflineUser u : SuperAPI.getOfflineUsers()) {
                    if(!l.contains(u.getName()))
                        l.add(u.getName());
                }
            for(String u : l) {
                if(u.toLowerCase().startsWith(name.toLowerCase())) f.add(u);
            }
            return f;
        }
        return ImmutableList.of();
    }


    protected List<String> loadTab(int length, Iterable<String> arguments) {
        return loadTab(length, true, arguments);
    }

    protected List<String> loadTab(int length, String prevArg, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        try {
            if(tabArgs.length == length && tabArgs[tabArgs.length+-2].equals(prevArg))
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getInput()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        try {
            if(tabArgs.length == length && isTrue)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getInput()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, String[] args, Iterable<String> arguments) {
        return loadTab(length, args, true, arguments);
    }


    protected List<String> loadTab(int length, Iterable<String> arguments, String replace) {
        List<String> returnList = new ArrayList<>();
        try {
            if(tabArgs.length == length)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getInput().substring(replace.length())))returnList.add(replace+s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, String[] args, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        try {
            boolean isTrue1 = false;
            for(int i = args.length; i > 0; i--) {
                if(tabArgs[tabArgs.length+-i].equals(args[args.length+-i]))
                    isTrue1=true;
                else
                    isTrue1=false;
            }
            if(tabArgs.length == length && isTrue && isTrue1)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getInput()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, ArgumentData data, Iterable<String> arguments) {
        return loadTab(length, data, true, arguments);
    }

    protected List<String> loadTab(int length, ArgumentData[] argsData, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        boolean check = false;
        try {
            for(ArgumentData data : argsData)
                if(data.index != -1) {
                    if(tabArgs[data.index].equals(data.name))
                        check = true;
                    //System.out.println(tabArgs[data.index].equals(data.name));
                }else
                    for(int c = 0; c < data.indexes.length; c++) {
                        int i = data.indexes[c];
                        if(tabArgs[i].equals(data.name[c]))
                            check = true;
                        else {
                            check = false;
                            break;
                        }
                    }
            if(tabArgs.length==length&&check&&isTrue)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getInput()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected List<String> loadTab(int length, ArgumentData data, boolean isTrue, Iterable<String> arguments) {
        List<String> returnList = new ArrayList<>();
        boolean check = false;
        try {
            if(data.index != -1) {
                if(tabArgs[data.index].equals(data.name))
                    check = true;
            }else
                for(int c = 0; c < data.indexes.length; c++) {
                    int i = data.indexes[c];
                    if(tabArgs[i].equals(data.name[c]))
                        check = true;
                    else {
                        check = false;
                        break;
                    }
                }
            if(tabArgs.length==length&&check&&isTrue)
                for(String s : arguments)
                    if(checkTab(s, tabArgs[length+-1].getInput()))returnList.add(s);
        }catch (Exception e) {
            return returnList;
        }
        return returnList;
    }

    protected class ArgumentData {
        int index = -1;
        int[] indexes;
        String[] name;
        public ArgumentData(int index, String... name) {
            this.index = index;
            this.name = name;
        }
        public ArgumentData(int[] indexes, String... name) {
            this.indexes = indexes;
            this.name = name;
        }
    }

    protected List<String> forInt(int min, int max) {
        List<String> str = new ArrayList<>();
        int rmin, rmax;
        if(min < max) {
            rmin=min;
            rmax=max;
        }else {
            rmin=max;
            rmax=min;
        }
        for(int i = rmin; i < rmax; i++) {
            if(!str.contains(""+i)) str.add(""+i);
        }
        return str;
    }

    private boolean checkTab(String s1, String arg) {
        return s1.toLowerCase().startsWith(arg.toLowerCase());
    }





}
