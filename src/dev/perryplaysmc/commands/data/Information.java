package dev.perryplaysmc.commands.data;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.commands.data
 * Class: Information
 * <p>
 * Path: dev.perryplaysmc.commands.data.Information
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@AllArgsConstructor
@SuppressWarnings("all")
public class Information {

    @Getter(AccessLevel.PUBLIC)@Setter(AccessLevel.PUBLIC)
    private String description, usage;

}
