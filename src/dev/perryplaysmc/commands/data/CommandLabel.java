package dev.perryplaysmc.commands.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.commands.data
 * Class: CommandLabel
 * <p>
 * Path: dev.perryplaysmc.commands.data.CommandLabel
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
@AllArgsConstructor
public class CommandLabel {

    @Getter
    String name;

    public boolean startsWith(String... possibilities) {
        for (String possibility : possibilities) if(name.toLowerCase().startsWith(possibility.toLowerCase())) return true;
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if(o instanceof String) return equals(new String[]{""+o});
        if (o == null || getClass() != o.getClass()) return false;
        CommandLabel that = (CommandLabel) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    public boolean equals(String... possibilities) {
        for (String possibility : possibilities) if(name.equalsIgnoreCase(possibility)) return true;
        return false;
    }

}
