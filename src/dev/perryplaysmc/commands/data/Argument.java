package dev.perryplaysmc.commands.data;

import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.NumberUtil;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.Getter;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.commands.data
 * Class: Argument
 * <p>
 * Path: dev.perryplaysmc.commands.data.Argument
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public class Argument {

    @Getter()
    String input;

    public Argument(String input) {
        this.input = input;
    }


    public User getUser() throws CommandException {
        User u =  SuperAPI.getUser(input);
        if(u == null) throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidUser", input));
        return u;
    }

    public OfflineUser getAnyUser() throws CommandException {
        User u =  SuperAPI.getUser(input);
        OfflineUser u2 =  SuperAPI.getOfflineUser(input);
        if(u == null && u2 == null) throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidUser", input));
        return u == null ? u2 : u;
    }

    public OfflineUser getOfflineUser() throws CommandException {
        OfflineUser u = SuperAPI.getOfflineUser(input);
        if(u == null) throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidUser", input));
        return u;
    }

    public int getInt() throws CommandException {
        try {
            return Integer.parseInt(input);
        }catch (Exception e) {
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidIntegerInput", input));
        }
    }


    public double getDouble() throws CommandException {
        try {
            return Double.parseDouble(input);
        }catch (Exception e) {
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidDoubleInput", input));
        }
    }

    public double getMoney() throws CommandException {
        try {
            return NumberUtil.getMoney(input);
        }catch (Exception e) {
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidMoneyInput", input));
        }
    }

    public boolean equals(String name) {
        return input.equalsIgnoreCase(name);
    }

    public boolean equals(String... names) {
        for(String s : names)
            if(input.equalsIgnoreCase(s.replace(" ", "")))return true;
        return false;
    }

}
