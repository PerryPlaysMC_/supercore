package dev.perryplaysmc.commands;

import dev.perryplaysmc.commands.data.Argument;
import dev.perryplaysmc.commands.data.CommandLabel;
import dev.perryplaysmc.commands.data.Information;
import dev.perryplaysmc.configuration.Config;
import dev.perryplaysmc.core.SuperAPI;
import dev.perryplaysmc.exceptions.CommandException;
import dev.perryplaysmc.sources.CommandSource;
import dev.perryplaysmc.sources.OfflineUser;
import dev.perryplaysmc.sources.User;
import dev.perryplaysmc.utils.text.strings.StringUtils;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copy Right ©
 * This code is private
 * Owner: PerryPlaysMC
 * From: 11/3/19-2200
 * Package: dev.perryplaysmc.commands
 * Class: Command
 * <p>
 * Path: dev.perryplaysmc.commands.Command
 * <p>
 * Any attempts to use these program(s) may result in a penalty of up to $1,000 USD
 **/

@SuppressWarnings("all")
public abstract class Command extends TabCompletionExecutor {

    @Getter(AccessLevel.PUBLIC)@Setter
    private String name;

    @Getter(AccessLevel.PUBLIC)
    private Information info;

    protected List<String> defaultPermissions = new ArrayList<>();

    @Getter(AccessLevel.PUBLIC)@Setter
    private String[] aliases = new String[]{};

    @Getter(AccessLevel.PUBLIC)@Setter(AccessLevel.PUBLIC)
    private CommandLabel label;

    public CommandSource sender;

    public Argument[] args = new Argument[]{};

    public Command(String name, String... aliases) {
        this(name, new Information("Description Not set", "/" + name.toLowerCase()), aliases);
    }

    public Command(String name, Information info, String... aliases) {
        this.name = name.toLowerCase();
        this.info = info;
        this.aliases = StringUtils.toLowerCase(aliases);
        CommandExecutor.addCommand(this);
    }

    protected void setDescription(String description) {
        info.setDescription(description);
    }

    protected void setUsage(String usage) {
        info.setUsage(usage);
    }

    protected void addPermissions(String... permissions) {
        String cmdPrefix=SuperAPI.getSettings().getString("Data.Chat.Prefixes.Command")
                .replace("<name>", name)
                .replace("<command>", name);
        if(!defaultPermissions.contains((cmdPrefix+".*").toLowerCase()))
            defaultPermissions.add((cmdPrefix+".*").toLowerCase());
        for(String s : permissions) {
            if(!defaultPermissions.contains(s.toLowerCase()))
                defaultPermissions.add(s.toLowerCase());
        }
    }

    public String[] getDefaultPermissions() {
        return StringUtils.convertToArray(defaultPermissions);
    }

    public abstract void run(CommandSource s, CommandLabel label, Argument[] args) throws CommandException;

    protected void args(int length, Runner run) throws CommandException {
        if(args.length == length) {
            run.run();
            throw new CommandException();
        }
    }

    protected void args(int length, Runner run, CommandException e) throws CommandException {
        if(args.length == length) {
            run.run();
            throw new CommandException();
        }
        throw e;
    }

    protected void allArgsAfter(int length, Runner run) throws CommandException {
        if(args.length > length) {
            run.run();
            throw new CommandException();
        }
    }

    protected void argsGreater(int length, Runner run) throws CommandException {
        if(args.length > length) {
            run.run();
            throw new CommandException();
        }
    }

    protected void argsBetween(int start, int end, Runner run) throws CommandException {
        if(args.length >= start && args.length <= end) {
            run.run();
            throw new CommandException();
        }
    }

    protected void argsLess(int length, Runner run) throws CommandException {
        if(args.length < length) {
            run.run();
            throw new CommandException();
        }
    }

    //Helpers Start


    protected void commandType(CommandType type)  throws CommandException {
        if(type==CommandType.CONSOLE_SPECIFY_USER && !sender.isPlayer())
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.consoleCommandSpecifyUser"));

        if(type==CommandType.CONSOLE && sender.isPlayer())
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.consoleCommand"));

        if(type==CommandType.PLAYER && !sender.isPlayer())
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.playerCommand"));

    }

    //Helpers End

    protected void sendMessage(String path, Object... objects) {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        sender.sendMessage(path, objects);
    }

    protected void sendMessageEnd(String path, Object... objects) throws CommandException {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        sender.sendMessage(path, objects);
        throw new CommandException();
    }

    protected void sendMessage(OfflineUser t, String checkSendMessage, String path, Object... objects) {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        if(!checkSendMessage.startsWith("Messages.")) checkSendMessage = "Messages."+checkSendMessage;
        if(SuperAPI.getMessages().getBoolean(checkSendMessage) && t.getName() != sender.getName())
            t.sendMessage(path, objects);
    }

    protected void sendMessageOther(OfflineUser t, boolean checkSendMessage, String path, Object... objects) {
        if(!path.startsWith("Messages.")) path = "Messages."+path;

        if((checkSendMessage ? SuperAPI.getMessages().getBoolean(path + ".other.sendTarget") : true) && t.getName() != sender.getName())
            t.sendMessage(path + ".other.target", objects);
    }

    protected void sendMessageEnd(OfflineUser t, String checkSendMessage, String path, Object... objects) throws CommandException {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        if(!checkSendMessage.startsWith("Messages.")) checkSendMessage = "Messages."+checkSendMessage;
        if(SuperAPI.getMessages().getBoolean(checkSendMessage) && t.getName() != sender.getName()) {
            t.sendMessage(path, objects);
            throw new CommandException();
        }
    }

    protected void sendMessage(OfflineUser t, String path, Object... objects) {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        t.sendMessage(path, objects);
    }

    protected void sendMessageEnd(OfflineUser t, String path, Object... objects) throws CommandException {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        t.sendMessage(path, objects);
        throw new CommandException();
    }

    protected void sendMessage(Config cfg, String path, Object... objects) {
        sender.sendMessage(StringUtils.Formatter.format(cfg, path, objects));
    }


    protected void sendArgs(boolean tooMany) {
        if(tooMany)
            sendMessage("Error.tooManyArgs");
        else
            sendMessage("Error.notEnoughArgs");
    }

    protected void broadcastPermission(String permission, String path, Object... objs) {
        if(!path.startsWith("Messages.")) path = "Messages."+path;
        SuperAPI.getConsole().sendMessage(path, objs);
        for(User u : SuperAPI.getOnlineUsers())
            if(StringUtils.isEmpty(permission))
                u.sendMessage(path, objs);
            else {
                if(u.hasPermission(permission))
                    u.sendMessage(path, objs);
            }
    }

    protected void broadcast(String path, Object... objs) {
        broadcastPermission("", path, objs);
    }

    protected void hasPermission(String... permissions) throws CommandException {
        String perm = "none";
        for(String s : getDefaultPermissions())
            if(s.startsWith(label.getName()+":"))
                perm = s.replace(label.getName()+":","");
        if(!sender.hasPermission(permissions))
            throw new CommandException(StringUtils.Formatter.formatDefault("Messages.Error.invalidPermission", perm));

    }

    protected void runNormal(User t, int i) throws CommandException {
        if(sender.getName().equalsIgnoreCase(t.getName())) {
            args = Arrays.copyOfRange(args, 0, i);
            run(sender, label, args);
            throw new CommandException();
        }
    }

   protected void runBack(User t) throws CommandException {
        if(sender.getName().equalsIgnoreCase(t.getName())) {
            args = Arrays.copyOfRange(args, 0, args.length-1);
            run(sender, label, args);
            throw new CommandException();
        }
    }


    protected enum CommandType {
        PLAYER, CONSOLE, CONSOLE_SPECIFY_USER;
    }

    protected interface Runner {
        void run() throws CommandException;
    }
}
